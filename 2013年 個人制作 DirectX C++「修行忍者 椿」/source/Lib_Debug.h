#ifdef _DEBUG

#ifndef __LIB_DEBUG_H__
#define __LIB_DEBUG_H__

//#include "Lib_Types.h"
#include "color.h"

//#include <string>
using namespace std;

namespace Debug
{
#define f32 float

#define NEW(obj) new obj;
//#define DEL(obj) if( obj ){ delete ( obj ); ( obj ) = NULL; }
#define DEL_A(obj) if( obj ){ delete [] ( obj ); ( obj ) = NULL; }

#define NORMAL_FONT_SIZE 16
#define MINI_FONT_SIZE 12

#define KEEP_SEPARATE	0x00
#define KEEP_PRESS		0x01
#define FIRST_SEPARATE	0x02
#define FIRST_PRESS		0x03 

//	ウインドウリソースパス
//#define DEBUG_RESOURCE_PATH "Resource\\Debug\\white.png"

//	テキストサイズ
#define DB_FONT_SIZE	MINI_FONT_SIZE
#define DB_FONT_SIZE_W	( DB_FONT_SIZE / 2 )
//	テキストカラー
#define DB_MANU_COLOR						RGBA(80,80,80,192)
#define DB_MENU_NONE_ACTIVE_COLOR			RGBA(80,80,80,128)

#define DB_SELECT_BACK_COLOR				GREEN
#define DB_SELECT_BACK_NONE_ACTIVE_COLOR	RGBA(0,255,0,128)

#define DB_NORMAL_TEXT_COLOR				WHITE
#define DB_NORMAL_TEXT_NONE_ACTIVE_COLOR	RGBA(255,255,255,128)

#define DB_SELECT_TEXT_COLOR				BLACK
#define DB_SELECT_TEXT_NONE_ACTIVE_COLOR	RGBA(0,0,0,128)

#define DB_TITLE_TEXT_COLOR					BLACK
#define DB_TITLE_TEXT_NONE_ACTIVE_COLOR		RGBA(0,0,0,128)

#define DB_ATRIBUTE_TEXT_COLRO				ORANGE
#define DB_ATRIBUTE_TEXT_NONE_ACTIVE_COLRO	RGBA(255,0,0,128)

#define DB_SEPARATOR_COLOR					WHITE
#define DB_SEPARATOR_NONE_ACTIVE_COLOR		RGBA(255,255,255,128)

//	キーの割り当て
#define DB_BUT_UP		0
#define DB_BUT_DOWN		1
#define DB_BUT_LEFT		2
#define DB_BUT_RIGHT	3
#define DB_BUT_CANCEL	5
#define DB_BUT_SELECT	4
#define DB_BUT_OPEN		4
	

#define DB_NUM_MENU 512	//メニュー最大登録数
#define DB_NUM_ITEM 256 //アイテム最大登録数

#define DB_STRING_LENGTH 128
#define DB_ATR_STRING_LEGTH 32

#define DB_PRESS_WAITTIME 40

//	アイテム属性文字列
#define DB_ATTRIBUTE_READONLY		" <R"
#define DB_ATTRIBUTE_PUSH_CALL		" <P!"
#define DB_ATTRIBUTE_VALUE_CALL		" <V!"
#define DB_ATTRIBUTE_BRACKET		" ⇒"

typedef void ( *CALL_BACK )( void* );

enum GROUP_ID
{
	INVALID_GROUP = -1,
	START_GROUP = 0,
};

enum ACTION
{ 
	ACT_NONE = 0,
	ACT_SELECT,
	ACT_NON_SELECT,
	ACT_LEFT,
	ACT_RIGHT,
};

struct SRect
{
	int x, y;
	int w, h;
	SRect(){ x = y = w = h = 0;}
	SRect(int posx, int posy, int width, int height)
	{
		x = posx; y = posy; w = width; h = height;
	}
};

class CMenu;

//================================================================================//
//@brief 抽象アイテムクラス
//--------------------------------------------------------------------------------//
//@sub 外部から宣言は基本的にはしない
//@sub アイテムのクラスを作るときはこのクラスを継承して使う
//================================================================================//
class CItem
{
protected:
	const string _String;
	string _Atr;
	u32 _GroupId;
	SRect _Rect;
	u32 _MaxWidth;

	bool _Select;
	bool _ReadOnly;
	bool _Separator;

	CALL_BACK _PushCallback;
	CALL_BACK _ValueCallback;
public:
	CItem( u32 GroupId, const string String, const string Atr, SRect* Rect, bool ReadOnly );
	virtual ~CItem(){}
	virtual void Action( ACTION Action, CMenu* Menu );
	virtual void Draw( u32 MaxWidth, bool Active );
	
	void UpdatePosition( int x, int y );

	u32 GetGroupId(){ return _GroupId; }

	SRect GetRect(){ return _Rect; }

	u32 GetMaxWidth(){ return _MaxWidth; }

	void AddAtributeString( string String ){ _Atr += String; }
	void SetSelect( bool Select ){ _Select = Select; }
	void SetSeparator( bool Separator ){ _Separator = Separator; }

	bool IsReadOnly(){ return _ReadOnly; }

	void SetPushCallback( CALL_BACK Callback ){ _PushCallback = Callback; }
	void SetValueCallback( CALL_BACK Callback ){ _ValueCallback = Callback; }
};

//================================================================================//
//@brief Titleアイテムクラス
//--------------------------------------------------------------------------------//
//@sub タイトル
//@sub 機能 : 強制でReadOnly 他の設定はしても意味がない
//================================================================================//
class CTitleItem : public CItem
{
public:
	CTitleItem( u32 GroupId, const string String, SRect* Rect, bool ReadOnly );

//	void Action( ACTION Action, CMenu* Menu );
	void Draw( u32 MaxWidth, bool Active );
};

//================================================================================//
//@brief Stringアイテムクラス
//--------------------------------------------------------------------------------//
//@sub タイトル
//@sub 機能 : ReadOnly, PushCallback, SelectCallback
//================================================================================//
class CStringItem : public CItem
{
	char* _Value;
public:
	CStringItem( u32 GroupId, const string String, char* Value, const string Atr, SRect* Rect, bool ReadOnly );

	//void Action( ACTION Action, CMenu* Menu );
	void Draw( u32 MaxWidth, bool Active );
};

//================================================================================//
//@brief Boolアイテムクラス
//--------------------------------------------------------------------------------//
//@sub true or false 用のアイテムクラス
//@sub 機能 : ReadOnly, PushCallback, SelectCallback
//================================================================================//
class CBoolItem : public CItem
{
	bool* _Value;
	const string _List;
public:
	CBoolItem( u32 GroupId, const string String, bool* Value, const string List, const string Atr, SRect* Rect, bool ReadOnly );

	void Action( ACTION Action, CMenu* Menu );
	void Draw( u32 MaxWidth, bool Active );
};

//================================================================================//
//@brief Bracketアイテムクラス
//--------------------------------------------------------------------------------//
//@sub 分岐
//@sub 機能 : ReadOnly, PushCallback, SelectCallback, Bracket
//================================================================================//
class CBracketItem : public CItem
{
	bool _PutEnable; //	派生もとのｙ位置にあわせる
	u32 _BracketGroupId;
public:
	CBracketItem( u32 GroupId, u32 BracketGroupId, const string String, const string Atr, SRect* Rect, bool ReadOnly, bool PutEnable );
	void Action( ACTION Action, CMenu* Menu );
};

//================================================================================//
//@brief Eventアイテムクラス
//--------------------------------------------------------------------------------//
//@sub 基本コールバック用
//================================================================================//
class CEventItem : public CItem
{
public:
	CEventItem( u32 GroupId, const string String, const string Atr, SRect* Rect, bool ReadOnly );

	void Action( ACTION Action, CMenu* Menu );
//	void Draw( u32 MaxWidth, bool Active );
};

//================================================================================//
//@brief Enumアイテムクラス
//--------------------------------------------------------------------------------//
//@sub 列挙された文字列を値と変更時に表示
//================================================================================//
class CEnumItem : public CItem
{
	int _Num;//	列挙されている文字列の数
	int* _Value;
	const string _List;

	string GetStringList( int Num );
public:
	CEnumItem( u32 GroupId, const string String, const string List, int* Value, const string Atr, SRect* Rect, bool ReadOnly );
	~CEnumItem(){}
	void Action( ACTION Action, CMenu* Menu );
	void Draw( u32 MaxWidth, bool Active );
};

//================================================================================//
//@brief Valueアイテムクラス
//--------------------------------------------------------------------------------//
//@sub 各型の変数の表示
//@sub 機能 : ReadOnly, PushCallback, SelectCallback
//================================================================================//
template<class _Type1, class _Type2>
class CValueItem : public CItem
{
	_Type1* _Value;
	_Type1 _Min, _Max, _Step;
public:
	CValueItem( u32 GroupId, const string String, _Type1* Value, _Type1 Step, _Type1 Min, _Type1 Max, const string Atr, SRect* Rect, bool ReadOnly );

	void Action( ACTION Action, CMenu* Menu );
	void Draw( u32 MaxWidth, bool Active );
};

typedef CValueItem<s32, s32>	S32;
typedef CValueItem<u32, u32>	U32;
typedef CValueItem<s16, s16>	S16;
typedef CValueItem<u16, u16>	U16;
typedef CValueItem<s8, s8>		S8;
typedef CValueItem<u8, u8>		U8;
typedef CValueItem<f32, f32>	F32;

//================================================================================//
//@brief アイテムリストクラス
//--------------------------------------------------------------------------------//
//@sub 複数のアイテムを一括管理するクラス
//================================================================================//
class CItemList
{
	static const int _ItemNum = DB_NUM_ITEM;	//	アイテム最大数
	CItem* _Item[_ItemNum];					//	アイテムリスト
	u32 _MaxWidth;								//	アイテムの中での最大の横幅
	int _Count;								//	使用されているアイテム数
	
	CItem* _LastItem;							//	最後にセットされたアイテム
public:
	CItemList();
	~CItemList();
	void Clear();
	void Update();

	void SetItem( CItem* Item , u32 Width );
	CItem* GetItem( u32 GroupId, u32 Num );
	CItem* GetLastItem(){ return _LastItem; } 

	u32 GetMaxWidth(){ return _MaxWidth; };
	void SetMaxWidth( u32 Width ){ if(_MaxWidth < Width)_MaxWidth = Width; }

	int GetCount(){ return _Count; }
};

//================================================================================//
//@brief デバックメニュークラス
//--------------------------------------------------------------------------------//
//@sub デバックのメニューウインドウをつくためのクラス
//@sub このメニューにアイテムを列挙していく
//================================================================================//
class CMenu
{
	CItemList _ItemList;		//　アイテムリスト

	u32 _GroupId;				//	グループ
	SRect _Rect;				//　表示位置と幅
	int _SelectNo;				//	現在選択されているアイテム
	bool _Open;				//	メニューが開かれているか
	bool _Active;				//	操作可能状態
	
	CMenu* _BeforeMenu;		//	派生もとのメニュー
public:	
	CMenu();
	~CMenu();
	void Init();
	void Exec();
	void Draw();
	void Set( u32 GroupId );
	void Open( int x, int y );
	void Open();
	void Close();
	void Clear();

	//void Select( bool Direction );

	bool IsOpen(){ return _Open; }
	bool IsActive(){ return _Active; }

	void SetActive( bool Active ){ _Active = Active; }
	CItemList* GetItemList(){ return &_ItemList; }
	u32 GetGroupId(){ return _GroupId; }

	SRect GetRect(){ return _Rect; }

	void SetBeforeMenu( CMenu* Menu ){ _BeforeMenu = Menu; }
};

//================================================================================//
//@brief デバッククラス
//--------------------------------------------------------------------------------//
//@sub スタティッククラス
//@sub 外部からのアクセス用
//================================================================================//
class CDebug
{

	static const int _MenuNum = DB_NUM_MENU; // メニュー最大登録数
	static CMenu _Menu[_MenuNum];
	static u32 _CurrentGroupId;
	static bool _ReadOnly;
	static int _MenuCount;
	
public:
	//static iex2DObject* _Object;

	static CMenu* FindMenu( u32 GroupId );

	static void Init();
	static void Rele();
	static void Exec();
	static void Draw();

	static bool IsOpen( u32 GroupId );
	static void Open( int x, int y, u32 GroupId = START_GROUP );
	static void Clear( u32 GroupId );

	static void NewGroup( u32 GroupId );
	static void SetGroup( u32 GroupId );

	static void Title( const string String );
	static void String( const string Stirng );
	static void String( const string String1, char* String2 );
	static void Bracket( const string String, u32 GroupId, bool PutEnable = true );
	static void Event( const string String, CALL_BACK PushCallback );
	static void Enum( const string String, const string List, int* Value );

	static void Value( const string String, s32* Value, s32 Step = 1, s32 Min = 0, s32 Max = 0 );
	static void Value( const string String, u32* Value, u32 Step = 1, u32 Min = 0, u32 Max = 0 );
	static void Value( const string String, s16* Value, s16 Step = 1, s16 Min = 0, s16 Max = 0 );
	static void Value( const string String, u16* Value, u16 Step = 1, u16 Min = 0, u16 Max = 0 );
	static void Value( const string String, s8* Value, s8 Step = 1, s8 Min = 0, s8 Max = 0 );
	static void Value( const string String, u8* Value, u8 Step = 1, u8 Min = 0, u8 Max = 0 );
	static void Value( const string String, f32* Value, f32 Step = 1.0f, f32 Min = 0.0f, f32 Max = 0.0f );
	//	boolItem
	static void Value( const string String, bool* Value, const string List = "false|ture" );


	static int GetMenuCount(){ return _MenuCount; }
	//	ボタンを押したときにコールバック
	static void PushCall( CALL_BACK Callback );

	//	値を変更時にコールバック
	static void ValueCall( CALL_BACK Callback );

	static void Separator();
	static void ReadOnly();

	//static void ScreenShot( LPSTR filename );
	//static void ScreenShotEX( LPSTR filename, D3DXIMAGE_FILEFORMAT FileFormat );
	
	//	速度検証用
	//static clock_t GetClock(){ return clock(); }
	//static double GetClockPerSec( clock_t c ){ return (double)c*1000.0/(double)CLOCKS_PER_SEC; }
	
	//	VSの出力窓にメッセージをだす(フォーマット指定子の使用がOK)
	//static void OutputString( const char* fmt, ... );
};

} //namespace Debug

#endif //__LIB_DEBUG_H__

#endif //_DEBUG