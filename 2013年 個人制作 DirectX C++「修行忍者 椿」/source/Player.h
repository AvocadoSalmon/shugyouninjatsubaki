#ifndef _PLAYER_H_
#define _PLAYER_H_

#include "BaseObject.h"
#include "Enemy.h"
#include "EffectManager.h"
#include <string>

/* マクロ定義 */

//	プレイヤーの行動モード
enum{
	P_MODE_WAIT,
	P_MODE_BLADE,
	P_MODE_NINJUTU,
	P_MODE_DAMAGE,
};

//	攻撃タイプ
enum{
	BLADE,
	NINJUTU,
	NORMAL_STATE = -1,
};

//	プレイヤーのモーション
enum{
	P_MOTION_LINEAR,
	P_MOTION_WAIT,
	P_MOTION_RUN,
	P_MOTION_JUMP,
	P_MOTION_LANDING,
	P_MOTION_BLADE1,
	P_MOTION_BLADE2,
	P_MOTION_BLADE3,
	P_MOTION_THROW,
	P_MOTION_CHARGE,
	P_MOTION_DAMAGE,
	P_MOTION_START,
	P_MOTION_EXIT,
};

//	モーション補間フレーム数
enum{
	P_INTERPOLATE_WAIT			= 15,
	P_INTERPOLATE_RUN			= 15,
	P_INTERPOLATE_JUMP			= 10,
	P_INTERPOLATE_LANDING		= 10,
	P_INTERPOLATE_BLADE1		= 15,
	P_INTERPOLATE_BLADE2		= 10,
	P_INTERPOLATE_BLADE3		= 15,
	P_INTERPOLATE_THROW_KUNAI	= 10,
	P_INTERPOLATE_SUMMON		= 15,
	P_INTERPOLATE_DAMAGE		= 10,
};	

//	ブレードステップ
enum Blade
{
	STEP_BLADE1,
	STEP_BLADE2,
	STEP_BLADE3,
};

//	ブレード前進スピード
#define SPEED_BLADE1	0.005f
#define SPEED_BLADE2	0.01f

//	各アクション終了フレーム
enum EndFrame
{
	BLADE1_M_START	 =	152,
	BLADE1_M_END	 =	162,
	BLADE1_END		 =	190,
	BLADE2_M_START	 =	213,
	BLADE2_JUMP		 =	229,
	BLADE2_M_END	 =	235,
	BLADE2_END		 =	240,
	BLADE3_M_START	 =	295,
	BLADE3_M_END	 =	304,
	BLADE3_END		 =	320,
	KUNAI_END		 =	360,

};

//	軌跡の頂点
enum
{
	LOCUS_VERTEX1,
	LOCUS_VERTEX2,
	LOCUS_VERTEX3,
	LOCUS_VERTEX4,
};

//	効果音タイプ
enum PLAYER{
	SE_RUN,
	SE_BLADE1,
	SE_BLADE2,
	SE_BLADE3,
	SE_CHARGE,
	SE_RELEASE,
};

#define PLAYER_HIT_PRM			obj->GetParam(2)==1	//	攻撃パラメータのフレーム
#define PLAYER_ATTACK_ON		true				//	攻撃判定ON
#define PLAYER_ATTACK_OFF		false				//	攻撃判定OFF

//#define HIT_RADIUS_BLADE1		2.0f				//	攻撃1の攻撃範囲
//#define HIT_RADIUS_BLADE2		2.0f				//	攻撃2の攻撃範囲
//#define HIT_RADIUS_BLADE3		3.0f				//	攻撃3の攻撃範囲

#define PLAYER_PRM_RUN			0					//	走りパラメータ
#define PLAYER_PRM_ATTACK		2					//	攻撃パラメータ
#define PLAYER_PRM_TRUE			1					//	パラメータON
#define PLAYER_PRM_FALSE		0					//	パラメータOFF

#define HIT_BALDE_POWER			3					//	ブレードの攻撃力
#define HIT_NINJUTU_POWER		12					//	忍術の攻撃力

#define BLADE2_JUMP_HEIGHT		3.0f				//	ブレード２のジャンプ値
#define WEAPON_SETSIZE			1.0f				//	武器のセット時のサイズ

#define PLAYER_SPEED			0.15f				//	走るスピード
#define PLAYER_SIZE_BASIC		0.009f				//	プレイヤーの基本サイズ
#define PLAYER_ENERGY_MAX		500					//	プレイヤーのエネルギーゲージの最大値
#define PLAYER_ENERGY_WIDTH		416					//	プレイヤーのエネルギーバーの幅
#define GAUGE_FLAME_X			500					//	エネルギーバーのフレームX座標
#define GAUGE_FLAME_Y			650					//	エネルギーバーのフレームY座標
#define GAUGE_FLAME_WIDTH		512					//	エネルギーバーのフレーム幅
#define GAUGE_FLAME_HEIGHT		64					//	エネルギーバーのフレーム高さ
#define GAUGE_X					500+47				//	エネルギーバーのX座標
#define GAUGE_Y					650					//	エネルギーバーのY座標
#define GAUGE_HEIGHT			64					//	エネルギーバーの高さ
#define GAUGE_READY				64					//	エネルギーバーの読み込みY座標

#define ON_EVENT				true				//	イベント中
#define NOT_EVENT				false				//	メインモード中

#define INPUT_BIAS				0.001f				//	入力処理の調整値
#define DOT_BIAS				(1-n) * 2.0f		//	内積の調整値

#define PLAYER_BONE_LEFTARM		33					//	左腕ボーン
#define PLAYER_BONE_RIGHTARM	20					//	右腕ボーン
#define LOCUS_MAX				30					//	軌跡のポリゴン
#define LOCUS_VERTEX_MAX		LOCUS_MAX*2+2		//	軌跡の頂点数
#define LOCUS_LENGTH			60.0f				//	軌跡の長さ
#define LOCUSL_ON				true				//	左手軌跡ON
#define LOCUSL_OFF				false				//	左手軌跡OFF
#define LOCUSR_ON				true				//	右手軌跡ON
#define LOCUSR_OFF				false				//	右手軌跡OFF
#define WEAPON_BONE_LENGTH		0.2f				//	武器ボーンの長さ
#define KATANA_MAX				2					//	刀の本数
#define KATANA_LEFT				0					//	左手の刀番号
#define KATANA_RIGHT			1					//	右手の刀番号
#define BLADE2_JUMP_TIME		17					//	刀２撃目のジャンプ時間(フレーム)


//*******************************************************************************
//
//	プレイヤークラス
//
//*******************************************************************************
class c_PLAYER : public BaseOBJ{
private:
	float m_RunSpeed;						//	移動スピード
	int m_state;							//	プレイヤーの状態
	int m_SubStep;							//	状態移行用
	iex2DObj* m_bboard;						//	ビルボード画像
	iex2DObj* m_gauge;						//	SLゲージ画像
	int m_SL;								//	現在のSLゲージ
	int m_SLmax;							//	SLゲージの最大値
	int m_SLwidth;							//	画面に表示する幅
	iex2DObj* m_lpLocus;					//	軌跡テクスチャ
	LVERTEX m_locus_vertexL[LOCUS_MAX*2+2];	//	左手軌跡用頂点
	LVERTEX m_locus_vertexR[LOCUS_MAX*2+2];	//	右手軌跡用頂点
	bool m_bLocusFlagL;						//	左手用軌跡表示フラグ
	bool m_bLocusFlagR;						//	右手用軌跡表示フラグ
	bool m_Event;							//	イベントフラグ
	Vector3 m_pos_old;						//	軌跡の前の位置情報
	float m_angle_old;						//	軌跡の前の方向情報
	iexMesh* m_lpKatana[KATANA_MAX];		//	刀
	bool m_bAttack;							//	攻撃フラグ
	int m_AttackType;						//	攻撃タイプ 0:ブレード,1:忍術
	iexSound *m_sound;						//	SE
	c_EffectManager* eff_manager;			//	エフェクト管理クラス

	//	行動関数
	void Blade();
	void Ninjutu();
	void Advance( float speed );
	void Wait();

protected:
	
public:
	c_PLAYER(char* name);
	c_PLAYER();
	~c_PLAYER();

	BOOL Update();
	void Stop();
	void Render();
	void Render(char *tech);

	void HitCheck( EnemyManager* enemy );

	//	イベント
	BOOL EventUpdate();
	void EventRender(char* name);
	void EventWalk();

	//	武器を指定座標にセット
	void WeaponSet(iex3DObj* lpWeapon,int BoneNum);
	void WeaponSetToMesh(iexMesh* lpWeapon,int BoneNum);
	
	void ChangeMode( int mode );	//	行動モードを変更

	//	剣の軌跡関連
	void LocusInitialize( char* filename );
	void LocusPassL();	//	左手
	void LocusPassR();	//	右手
	void RenderLocus();

	//	ゲッター
	Vector3 GetBonePos(int n, int d);
	Vector3 GetBonePos(int n);

	//	セッター
	inline void SetPos( Vector3& pos ){ Pos = pos; }
	inline void SetAngle( float Angle ){ angle = Angle; }
	inline void SetAttackType( int t ){ m_AttackType = t; }
	inline void SetAttackFlag( bool f ){ m_bAttack = f; }
};

extern c_PLAYER* player;

#endif	//	_PLAYER_H_