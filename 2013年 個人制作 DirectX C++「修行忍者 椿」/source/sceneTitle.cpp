#include	"iextreme.h"
#include	"system/system.h"

#include	"sceneTitle.h"

//**************************************************************************************//
//																						//
//																						//
//																						//
//**************************************************************************************//


//**************************************************************************************//
//																						//
//		初期化																			//
//																						//
//**************************************************************************************//

bool sceneTitle::Initialize()
{
	//	フェードイン
	m_fade = new c_Fade();
	m_fade->SetMode( MODE_FADE_INIT );
	//	パーティクル読み込み
	iexParticle::Initialize( FILE_PATH_PARTICLE, TITLE_PARTICLE_MAX );
	//	カメラ初期座標
	CAMERA.SetPos( CAMERA_FIRST_POS );
	m_bCameraStop = false;
	//	背景初期化
	m_lpBack = new iexMesh( FILE_PATH_BG_NAKATEN );
	m_lpBack->SetScale( TITLE_STAGE_SIZE );
	m_lpBack->SetPos( 0.0f, 0.0f, 0.0f );
	shader->SetValue( "SkyColor", TITLE_SKY_COLOR );
	shader->SetValue( "GroundColor", TITLE_GROUND_COLOR );
	//	2D読み込み
	m_font = new iex2DObj( FILE_PATH_TITLE_FONT );
	m_cursorY = PRESS_CURSOR_Y_MIN;
	//	プレイヤー初期化
	m_lpPlayer = new iex3DObj( FILE_PATH_PLAYER_MODEL );
	m_PlayerPos	= TITLE_PLAYER_FIRST_POS;
	m_PlayerScale    = TITLE_PLAYER_SCALE;
	//	スクリーンフィルタ
	screen = new iex2DObj( SCREEN_WIDTH, SCREEN_HEIGHT, IEX2D_RENDERTARGET );
	shader2D->SetValue( "contrast", TITLE_FILTER_CONTRAST );
	shader2D->SetValue( "chroma", TITLE_FILTER_CHROMA );
	shader2D->SetValue( "ScreenColor", TITLE_FILTER_COLOR );
	//	HDRレンダーターゲット
	HDR = new iex2DObj( HDR_SIZE, HDR_SIZE, IEX2D_RENDERTARGET );
	//	被写界深度一時レンダリング用サーフェイス
	Depth = new iex2DObj( SCREEN_WIDTH,SCREEN_HEIGHT, IEX2D_FLOAT );
	m_FocusDist  = TITLE_FIRST_FOSUS_DIST;
	m_FocusRange = TITLE_FIRST_FOSUS_RANGE;
	//	シャドウマップ初期化
	InitShadow();
	//	ライトマップテクスチャ読み込み
	lightsamp = new iex2DObj( FILE_PATH_TITLE_LIGHTSAMP );
	shader->SetValue( "LightMap" , lightsamp );
	//	フロント画像読み込み
	m_film = new iex2DObj( FILE_PATH_TITLE_FILM );
	m_filmback = new iex2DObj( FILE_PATH_TITLE_BFILM );
	//	SE読み込み
	m_sound = new iexSound();
	m_sound->Set( SE_SELECT	  , FILE_PATH_SE_SELECT );		//	選択音
	m_sound->Set( SE_DICISION , FILE_PATH_SE_DICISION );	//	決定音
	m_sound->Set( SE_LOAD	  , FILE_PATH_SE_LOAD );		//	ロード開始音
	//	BGM読み込み
	m_bgm = IEX_PlayStreamSound( FILE_PATH_BGM_TITLE);

	m_counter = 0;
	m_bPush   = false;
	m_EndType = FIRST_VALUE;

	return true;
}


//-----------------------------------------------------
//	解放
//-----------------------------------------------------
sceneTitle::~sceneTitle()
{
	iexParticle::Release();
	DEL( m_lpBack );
	DEL( m_font );
	DEL( m_fade );
	DEL( m_lpPlayer );
	DEL( m_film );
	DEL( m_filmback );
	DEL( HDR );
	DEL( screen );
	pBackBuffer->Release();
	DEL( lightsamp );
	DEL( m_sound );
	DEL( ShadowTex );
	ShadowZ->Release();
	DEL( Depth );

}

//**************************************************************************************//
//																						//
//		更新																			//
//																						//
//**************************************************************************************//
void sceneTitle::Update()
{
	//	平行光・アンビエント設定
	iexLight::DirLight( shader, NULL, &TITLE_LIGHT_DIRECTION, TITLE_LIGHT_COLOR );
	iexLight::SetAmbient( TITLE_AMBIENT_COLOR );

	if( m_bCameraStop ){ //	カメラが停止
		//	カーソル上下移動
		if( !m_bPush ){		//	ENTERを押していなければ
			if( m_lpPlayer->GetMotion() != PLAYER_MOTION_WAIT )m_lpPlayer->SetMotion( PLAYER_MOTION_WAIT );

			if( KEY(KEY_UP) == PUSH_KEY )	//	↑キー押
			{
				m_sound->Play( SE_SELECT );	//	選択SE
				m_cursorY -= PRESS_END_COORD_Y - PRESS_COORD_Y;
			}
			if( m_cursorY < PRESS_CURSOR_Y_MIN )m_cursorY = PRESS_CURSOR_Y_MAX;
			if( KEY(KEY_DOWN) == PUSH_KEY )	//	↓キー押
			{
				m_sound->Play( SE_SELECT );	//	選択SE
				m_cursorY += PRESS_END_COORD_Y - PRESS_COORD_Y;
			}
			if( m_cursorY > PRESS_CURSOR_Y_MAX )m_cursorY = PRESS_CURSOR_Y_MIN;
		}
		else m_counter++;				//	カウンタスタート

		Sakura( SAKURA_COORD );			//	桜吹雪

		//	カーソルイベント処理
		if( m_cursorY == PRESS_CURSOR_Y_MIN ){					//	カーソルが上段	
			if( KEY(KEY_ENTER) == PUSH_KEY ){					//	ENTERキーで
				m_sound->Play( SE_DICISION );					//	決定SE
				m_sound->Play( SE_LOAD );						//	決定後SE
				if( !m_bPush )m_fade->SetMode( MODE_FADE_ACT );	//	フェードアウト
				m_bPush = true;									//	キーフラグをTRUE
				m_EndType = TITLE_TO_MAIN;						//	終了タイプをメイン移行に
				if( m_lpPlayer->GetMotion() != P_MOTION_START )m_lpPlayer->SetMotion( P_MOTION_START );
			}
		}
		else if( m_cursorY == PRESS_CURSOR_Y_MAX ){				//	カーソルが下段
			if( KEY(KEY_ENTER) == PUSH_KEY ){					//	ENTERキーで
				m_sound->Play( SE_DICISION );					//	決定SE
				m_bPush = true;									//	キーフラグをTRUE
				m_EndType = DESTROY_WINDOW;						//	終了タイプをウィンドウ破棄に
				if( m_lpPlayer->GetMotion() != P_MOTION_EXIT )m_lpPlayer->SetMotion( P_MOTION_EXIT );
			}
		}
	}

	m_fade->Update();										//	フェードIO更新	
	//	プレイヤー用モデル更新
	m_lpPlayer->Update();
	m_lpPlayer->SetScale( m_PlayerScale );
	m_lpPlayer->SetPos( m_PlayerPos );
	m_lpPlayer->SetAngle( 0.0f, PI, 0.0f );
	m_lpPlayer->Animation();

	iexParticle::Update();								//	パーティクル更新

	//	被写界深度焦点合わせ処理
	if( m_FocusDist < TITLE_MAX_FOSUS_DIST )m_FocusDist += TITLE_INC_FOSUS_DIST;
	else{
		m_FocusDist = TITLE_MAX_FOSUS_DIST;
		m_FocusRange += TITLE_INC_FOSUS_RANGE;
	}
	if( m_FocusRange >= TITLE_MAX_FOSUS_RANGE )m_FocusRange = TITLE_MAX_FOSUS_RANGE;

	shader2D->SetValue( "FocusDist" , m_FocusDist );
	shader2D->SetValue( "FocusRange" , m_FocusRange );



	/* メインモード移行処理 */
	
	if( m_EndType == DESTROY_WINDOW ){					//	ウィンドウ破棄タイプの場合
		if( m_counter >= TITLE_COUNT_LIMIT_D ){			//	モーション時間+猶予が来たら
			m_counter = 0;								//	カウンタリセット
			m_bPush = false;							//	キーフラグリセット
			m_EndType = FIRST_VALUE;					//	タイプリセット
			IEX_StopStreamSound( m_bgm );				//	BGMを停止
			PostQuitMessage(0);							//	ウィンドウを閉じる
		}
	}
	else if( m_EndType == TITLE_TO_MAIN ){				//	メイン移行タイプの
		if( m_counter >= TITLE_COUNT_LIMIT_M ){			//	モーション時間+猶予が来たら
			m_counter = 0;								//	カウンタリセット
			m_bPush = false;							//	キーフラグリセット
			m_EndType = FIRST_VALUE;					//	タイプリセット
			IEX_StopStreamSound( m_bgm );				//	BGMを停止
			CAMERA.Begin();
			ChangeScene( MODE_EVENT );					//	イベントモードへ
		}
	}else if( m_counter < TITLE_COUNT_LIMIT_M || m_counter < TITLE_COUNT_LIMIT_D ){	//	モーション時間+猶予が来てない場合
		//	メインカメラ更新
		Vector3 Target( m_lpPlayer->GetPos().x, m_lpPlayer->GetPos().y + TARGET_Y_BIAS, m_lpPlayer->GetPos().z );
		CAMERA.SetTarget( Target );
		if( CAMERA.MoveCamera( Target, CAMERA_STOP ) ){	//	キャラの手前2.0でカメラを止める
			m_bCameraStop = true;						//	焦点合わせ開始
			return;
		}
	}

}

//--------------------------------------------------------------------------------
//	引数: なし
//	機能: 擬似的にHDR効果を描画する
//--------------------------------------------------------------------------------
void sceneTitle::RenderHDR()
{
	//	HDRバッファへ切り替え
	HDR->RenderTarget();
	CAMERA.SetViewPort( PRM_VIEWPORT_HDR );
	CAMERA.SetProjection( PRM_PROJECTION_HDR );
	CAMERA.Begin();
	
	m_lpBack->Render( shader, "specular" );				//	背景描画
	m_lpPlayer->Render( shader, "specular" );			//	プレイヤー描画
	//iexParticle::Render( shader,"copy" );

	//	ぼかし
	DWORD	colorHDR = ( PRM_HDR_INIT_ALPHA ) | COLOR_WHITE;
	for( int i=HDR_POLYGON_SIZE ; i<HDR_POLYGON_SIZE_MAX ; i+=HDR_POLYGON_SIZE )
	{
		HDR->Render(  i,0,HDR_SIZE,HDR_SIZE, 0,0,HDR_SIZE,HDR_SIZE, shader2D,"add", colorHDR );
		HDR->Render( -i,0,HDR_SIZE,HDR_SIZE, 0,0,HDR_SIZE,HDR_SIZE, shader2D,"add", colorHDR );
		HDR->Render( 0,i,HDR_SIZE,HDR_SIZE, 0,0,HDR_SIZE,HDR_SIZE,  shader2D,"add", colorHDR );
		HDR->Render( 0,-i,HDR_SIZE,HDR_SIZE, 0,0,HDR_SIZE,HDR_SIZE, shader2D,"add", colorHDR );

		HDR->Render(  i,0,HDR_SIZE,HDR_SIZE, 0,0,HDR_SIZE,HDR_SIZE, shader2D,"gauss", colorHDR );
		HDR->Render( -i,0,HDR_SIZE,HDR_SIZE, 0,0,HDR_SIZE,HDR_SIZE, shader2D,"gauss", colorHDR );
		HDR->Render( 0,i,HDR_SIZE,HDR_SIZE, 0,0,HDR_SIZE,HDR_SIZE,  shader2D,"gauss", colorHDR );
		HDR->Render( 0,-i,HDR_SIZE,HDR_SIZE, 0,0,HDR_SIZE,HDR_SIZE, shader2D,"gauss", colorHDR );

		//	だんだん透明に
		colorHDR -= PRM_HDR_DECR_ALPHA;
	}
	
}

//--------------------------------------------------------------------------------
//	引数: なし
//	機能: 被写界深度を描画する
//--------------------------------------------------------------------------------
void sceneTitle::RenderDepth()
{
	Depth->RenderTarget();
	CAMERA.Begin();

	//	物体描画
	m_lpBack->Render( shader,"depth" );
	m_lpPlayer->Render( shader,"depth" );

	shader2D->SetValue( "DepthTex",Depth->lpTexture );
}

//**************************************************************************************//
//																						//
//		描画																			//
//																						//
//**************************************************************************************//
void sceneTitle::Render()
{
	//	フレームバッファのポインタ保存
	iexSystem::GetDevice()->GetRenderTarget( 0, &pBackBuffer );

	RenderHDR();										//	HDR
	RenderShadow();										//	シャドウマップ作成
	screen->RenderTarget();

	CAMERA.SetViewPort( PRM_VIEWPORT_N );
	CAMERA.SetProjection( PRM_PROJECTION_N );
	CAMERA.Begin();
	//	各描画
	m_lpBack->Render( shader, "full_s" );				//	背景描画
	m_lpPlayer->Render( shader, "lightmap" );			//	プレイヤー描画
	
	//	擬似ＨＤＲ描画
	HDR->Render( 0,0,SCREEN_WIDTH,SCREEN_HEIGHT,0,0,HDR_SIZE,HDR_SIZE,shader2D,"add",HDR_ALPHA );
	//	画面に色調を変えて復元	
	screen->Render( 0,0,SCREEN_WIDTH,SCREEN_HEIGHT, 0,0,SCREEN_WIDTH,SCREEN_HEIGHT, shader2D, "Mastering" );

	//	フレームバッファへ切り替え
	iexSystem::GetDevice()->SetRenderTarget( 0, pBackBuffer );
	screen->Render( 0,0,SCREEN_WIDTH,SCREEN_HEIGHT, 0,0,SCREEN_WIDTH,SCREEN_HEIGHT, shader2D, "copy" );

	//	深度情報描画
	RenderDepth();
	//	スクリーンをぼかす
	screen->RenderTarget();

	screen->Render( DOF_COORD_1,SCREEN_WIDTH,SCREEN_HEIGHT,0,0,SCREEN_WIDTH,SCREEN_HEIGHT,shader2D,"copy",DOF_ALPHA_1 );
	screen->Render( DOF_COORD_2,SCREEN_WIDTH,SCREEN_HEIGHT,0,0,SCREEN_WIDTH,SCREEN_HEIGHT,shader2D,"copy",DOF_ALPHA_2 );
	screen->Render( DOF_COORD_3,SCREEN_WIDTH,SCREEN_HEIGHT,0,0,SCREEN_WIDTH,SCREEN_HEIGHT,shader2D,"copy",DOF_ALPHA_3 );
	screen->Render( DOF_COORD_4,SCREEN_WIDTH,SCREEN_HEIGHT,0,0,SCREEN_WIDTH,SCREEN_HEIGHT,shader2D,"copy",DOF_ALPHA_4 );
	screen->Render( DOF_COORD_5,SCREEN_WIDTH,SCREEN_HEIGHT,0,0,SCREEN_WIDTH,SCREEN_HEIGHT,shader2D,"copy",DOF_ALPHA_5 );
	//	フレームバッファへ切り替え
	iexSystem::GetDevice()->SetRenderTarget( 0,pBackBuffer );
	//	被写界深度
	screen->Render( 0,0,SCREEN_WIDTH,SCREEN_HEIGHT,0,0,SCREEN_WIDTH,SCREEN_HEIGHT,shader2D,"focus" );

	iexParticle::Render( shader,"copy" );				//	パーティクル描画
	PressEnter();										//	カメラ停止すればPRESS ENTER関係描画
	m_fade->Render();									//	フェードIO描画

}

//**************************************************************************************//
//																						//
//		処理																			//
//																						//
//**************************************************************************************//

//--------------------------------------------------------------------------------
//	引数: なし
//	機能: PRESS ENTER画像の描画
//--------------------------------------------------------------------------------
void sceneTitle::PressEnter()
{
	//	初期化
	static int posX = X_COORD_MAX;		//	ロゴX座標
	static int ux = 0;					//	X軸アニメーションの幅
	static int animeCounter = 0;		//	各文字を描画する間隔
	static bool	bFlag = false;			//	点滅フラグ
	static float count = 0.0f;			//	アルファ操作カウント
	DWORD colorEnter = COLOR_F_WHITE;	//	PRESS ENTER用の色
	DWORD colorEnd = COLOR_F_WHITE;		//	終了用の色
	DWORD color = 0;					//	最終

	//	点滅処理
	count += PRESS_DESTROY_COUNT;	//	0.02ずつカウント
	float a = sinf(count) * ALPHA_MAX;
	if( a < 0 )a = -a;
	color = ( (DWORD)a << SHIFT_VALUE ) | COLOR_WHITE;
	if( m_cursorY == PRESS_CURSOR_Y_MIN ){			//	カーソルが上段	
		colorEnter = color;
	}else if( m_cursorY == PRESS_CURSOR_Y_MAX ){	//	カーソルが下段
		colorEnd = color;
	}

	if( m_bCameraStop ){	//	カメラが停止していれば
		//	タイトルロゴ描画
		m_font->Render( TITLE_LOGO_X, TITLE_LOGO_Y, TITLE_LOGO_WIDTH, TITLE_LOGO_HEIGHT, TITLE_LOGO_READ_COORD_X, TITLE_LOGO_READ_COORD_Y, TITLE_LOGO_WIDTH, TITLE_LOGO_HEIGHT, shader2D, "copy", COLOR_F_WHITE );
		//	カーソル描画
		m_font->Render( PRESS_CURSOR_X, m_cursorY, PRESS_CURSOR_WIDTH, PRESS_CURSOR_HEIGHT, PRESS_CURSOR_COORD_X, PRESS_CURSOR_COORD_Y, PRESS_CURSOR_WIDTH, PRESS_CURSOR_HEIGHT, shader2D, "copy", COLOR_F_WHITE );
		//	PRESS ENTER(修行開始)描画
		m_font->Render( PRESS_COORD_X, PRESS_COORD_Y, PRESS_WIDTH, PRESS_HEIGHT, PRESS_READ_COORD_X, PRESS_READ_COORD_Y, PRESS_WIDTH, PRESS_HEIGHT, shader2D, "copy", colorEnter );
		//	終了
		m_font->Render( PRESS_END_COORD_X, PRESS_END_COORD_Y, PRESS_END_WIDTH, PRESS_HEIGHT, PRESS_END_READ_COORD_X, PRESS_READ_COORD_Y, PRESS_END_WIDTH, PRESS_HEIGHT, shader2D, "copy", colorEnd );
	}
}

//--------------------------------------------------------------------------------
//	引数: 出現させる座標
//	機能: 桜吹雪を降らせる
//--------------------------------------------------------------------------------
void sceneTitle::Sakura( Vector3* pos )
{
	for( int i=0 ; i<SAKURA_LOOP_MAX ; i++ )
	{
		Vector3 pos2 = *pos;

		Vector3 move;
		move.x    = SAKURA_MOVE_X;
		move.y    = SAKURA_MOVE_Y;
		move.z    = SAKURA_MOVE_Z;

		PARTICLE	p;
		p.type    = SAKURA_TYPE;
		p.aFrame  = 0;
		p.aColor  = COLOR_F_WHITE;
		p.mFrame  = SAKURA_MID_FRAME;
		p.mColor  = COLOR_F_WHITE;
		p.eFrame  = SAKURA_LAST_FRAME;
		p.eColor  = COLOR_F_WHITE;

		p.angle   = 0.0f;
		p.rotate  = SAKURA_ROTATE;
		p.scale   = SAKURA_SCALE;
		p.stretch = SAKURA_STRETCH;

		p.flag    = RS_COPY;

		p.Pos     = pos2;
		p.Move    = move;
		
		p.Power.x = SAKURA_POWER_X;
		p.Power.z = SAKURA_POWER_Y;
		p.Power.y = SAKURA_POWER_Z;


		iexParticle::Set(&p);
	}

}

//*****************************************************************************************************************************
//
//		シャドウマップ
//
//*****************************************************************************************************************************

//	シャドウ初期化
void sceneTitle::InitShadow()
{
	ShadowTex = new iex2DObj( SHADOW_SIZE,SHADOW_SIZE, IEX2D_RENDERTARGET );
	iexSystem::GetDevice()->CreateDepthStencilSurface( SHADOW_SIZE, SHADOW_SIZE, D3DFMT_D16, D3DMULTISAMPLE_NONE, 0, FALSE, &ShadowZ, NULL );
}

//	シャドウマップ作成
void sceneTitle::RenderShadow()
{
	LPDIRECT3DSURFACE9	org;
	iexSystem::GetDevice()->GetRenderTarget( 0, &org );

	//	レンダーターゲット設定
	ShadowTex->RenderTarget();
	//	Ｚバッファ設定
	Surface*	orgZ;
	iexSystem::GetDevice()->GetDepthStencilSurface( &orgZ );
	iexSystem::GetDevice()->SetDepthStencilSurface(ShadowZ);

	//	ライト方向
	Vector3 dir = TITLE_LIGHT_DIRECTION;
	dir.Normalize();

	//	シャドウ作成
	Vector3	target = TITLE_PLAYER_FIRST_POS;
	Vector3 pos = target - dir*TITLE_SHADOW_DIST;
	Vector3 up( 0, 1, 0 );
	//	視点とライト位置へ
	D3DXMATRIX	ShadowMat, work;
	LookAtLH( ShadowMat, pos, target, up );
	D3DXMatrixOrthoLH( &work, TITLE_SHADOW_PROJ_SIZE, TITLE_SHADOW_PROJ_SIZE, TITLE_SHADOW_PROJ_NEAR, TITLE_SHADOW_PROJ_FAR );	//	平行投影行列(範囲100x100)
	ShadowMat *= work;

	shader->SetValue( "ShadowProjection",  &ShadowMat );

	D3DVIEWPORT9 vp = { 0,0,SHADOW_SIZE,SHADOW_SIZE, 0, 1.0f };
	iexSystem::GetDevice()->SetViewport( &vp );
	//	レンダリング
	iexSystem::GetDevice()->Clear( 0, NULL, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER, COLOR_F_WHITE, 1.0f, 0 );

	//	描画
	m_lpBack->Render( shader,"ShadowBuf" );
	m_lpPlayer->Render( shader,"ShadowBuf" );

	shader->SetValue( "ShadowMap",ShadowTex->lpTexture );

	//	レンダーターゲット復元
	iexSystem::GetDevice()->SetRenderTarget( 0, org );
	iexSystem::GetDevice()->SetDepthStencilSurface(orgZ);
}