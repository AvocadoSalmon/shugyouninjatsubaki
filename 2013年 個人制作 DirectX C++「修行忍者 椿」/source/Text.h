#ifndef _TEXT_H_
#define _TEXT_H_

//************************************************************************************
//
//	文字列描画クラス
//
//************************************************************************************
class c_String{
private:
	iex2DObj*	m_lpFont;		//	フォント画像
	int			m_FontSize;		//	文字サイズ
	int			m_FontNum;		//	文字数
	WORD*		m_FontTable;	//	文字テーブル
public:
	c_String();
	~c_String();
	void Init( char* name );
	void RenderFont( int x, int y, WORD font, DWORD shadow , DWORD color );
	void RenderStr( int x, int y, char* str, DWORD shadow, DWORD color );

};

#endif // _TEXT_H_