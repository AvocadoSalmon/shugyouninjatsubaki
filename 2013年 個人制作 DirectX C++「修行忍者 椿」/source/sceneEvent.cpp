#include	"iextreme.h"
#include	"system/system.h"

#include	"sceneEvent.h"
#include	"ThreadObject.h"
#include	"NowLoad.h"

//*****************************************************************************************************************************
//
//
//
//*****************************************************************************************************************************



//*****************************************************************************************************************************
//
//		初期化
//
//*****************************************************************************************************************************
bool sceneEvent::Initialize()
{
	//	スレッド作成
	cNowLoad *load = new cNowLoad();
	cThread *th	   = new cThread( load );
	th->Start();

	//	フェードイン
	fade = new c_Fade();
	fade->SetMode( MODE_FADE_INIT );

	count = 0;				//	ウェイト時間初期化
	m_ChangeTimer = 0;		//	シーン変更までのカウンタ
	m_bChangeFlg = false;	//	シーン変更までのカウントフラグ
	m_bStop = false;		//	プレイヤー停止フラグ

	//	ステージ読み込み
	stage = new c_Stage1();
	stage->Load();
	shader->SetValue( "SkyColor", EVENT_SKY_COLOR );
	shader->SetValue( "GroundColor", EVENT_GROUND_COLOR );
	//	ステージ判定モデル読み込み
	collision = new c_Collision();
	collision->Load( FILE_PATH_BG_STAGE01_COLLISION );
	//	プレイヤー読み込み
	player = new c_PLAYER( FILE_PATH_PLAYER_MODEL );
	player->SetScale( EVENT_PLAYER_SIZE );					//	スケール設定
	player->SetPos( Vector3 ( EVENT_INTIAL_POS ) );			//	初期座標設定
	//	カメラ初期設定
	CAMERA.SetPos( Vector3 ( EVENT_INTIAL_CPOS ) );
	//	レンズフレア読み込み
	flare = new iex2DObj( FILE_PATH_LENZ_FLARE );
	//	2D読み込み
	Frame = new iex2DObj( FILE_PATH_EVENT_FRAME );
	//	スクリーンフィルタ
	screen = new iex2DObj( SCREEN_WIDTH, SCREEN_HEIGHT, IEX2D_RENDERTARGET );
	shader2D->SetValue( "contrast", FILTER_CONTRAST );
	shader2D->SetValue( "chroma", FILTER_CHROMA );
	shader2D->SetValue( "ScreenColor", FILTER_COLOR );
	//	環境マップ設定
	EnvTex = new iex2DObj( FILE_PATH_ENVMAP );
	shader->SetValue("EnvMap", EnvTex);
	//	海テクスチャ設定
	seaTexture = new iex2DObj( FILE_PATH_NORMAL_SEA );
	shader->SetValue("SeaMap", seaTexture->lpTexture );
	//	HDRレンダーターゲット
	HDR = new iex2DObj( HDR_SIZE, HDR_SIZE, IEX2D_RENDERTARGET );
	//	被写界深度一時レンダリング用サーフェイス
	Depth = new iex2DObj( SCREEN_WIDTH,SCREEN_HEIGHT, IEX2D_FLOAT );
	m_FocusDist = DEFAULT_FOSUS_DIST;
	m_FocusRange = DEFAULT_FOSUS_RANGE;
	//	シャドウマップ初期化
	InitShadow();
	//	スクリプト読み込み
	SetScriptScene( this );
	scr.LoadScript( EVENT_SCRIPTFILE );
	//	文字列初期化
	Font = new c_String();
	Font->Init( EVENT_FONT_FILE );

	th->End();	//	スレッドの破棄

	delete th;
	delete load;

	return true;
}

//-----------------------------------------------------
//	解放
//-----------------------------------------------------
sceneEvent::~sceneEvent()
{
	DEL( collision );
	DEL( stage );
	DEL( player );
	DEL( flare );
	DEL( screen );
	DEL( Frame );
	DEL( fade );
	DEL( HDR );
	DEL( EnvTex );
	DEL( seaTexture );
	scr.ReleaseScript();
	DEL( Font );
	DEL( ShadowTex );
	ShadowZ->Release();
	DEL( Depth );

}

//*****************************************************************************************************************************
//
//		主処理
//
//*****************************************************************************************************************************
void	sceneEvent::Update()
{
	//	平行光・アンビエント設定
	iexLight::DirLight( shader, 0, &EVENT_LIGHT_DIRECTION, EVENT_LIGHT_COLOR );
	iexLight::SetAmbient( EVENT_AMBIENT_COLOR );

	if( count <= 0 ){	//	スクリプト:WAITの待ち時間経過
		scr.DEC_Main();	//	スクリプトデコード
	}
	else count--;		//	待ち時間カウント

	fade->Update();									//	フェード更新
	stage->Update();								//	ステージ更新
	if( !m_bStop )player->EventUpdate();			//	プレイヤー更新
	else player->Stop();

	//-----------------------------
	//	デバッグ用
	//-----------------------------
#ifdef _DEBUG
	if( KEY(KEY_A) == PUSH_KEY ){					//	Zキー押
		fade->SetMode( MODE_FADE_ACT );				//	フェードアウト開始
		m_bChangeFlg = true;						//	カウントフラグをtrueに
	}
#endif

	if( m_bChangeFlg )m_ChangeTimer++;											//	カウントスタート
	if( m_ChangeTimer == EVENT_FADEOUT_TIMER )fade->SetMode( MODE_FADE_ACT );	//	フェードアウト開始
	if( m_ChangeTimer >= EVENT_SCENE_TIMER ){									//	少し待つ
		m_ChangeTimer = 0;														//	タイマーリセット
		ChangeScene( MODE_MAIN );												//	時間が来ればメインモードへ
	}

	//	被写界深度設定
	shader2D->SetValue( "FocusDist" , m_FocusDist );
	shader2D->SetValue( "FocusRange" , m_FocusRange );

	//	流れる水処理
	static float tv = 0;
	tv -= WATER_FLOW_SPEED;
	shader->SetValue("AdjustSea", tv );

}

//--------------------------------------------------------------------------------
//	引数: なし
//	機能: 擬似的にHDR効果を描画する
//--------------------------------------------------------------------------------
void sceneEvent::RenderHDR()
{
	//	HDRバッファへ切り替え
	HDR->RenderTarget();
	CAMERA.SetViewPort( 0,0,HDR_SIZE,HDR_SIZE );
	CAMERA.SetProjection( PRM_PROJECTION_HDR );
	CAMERA.Begin();
	
	stage->Render( "specular" );
	player->Render( "specular" );

	//	ぼかし
	DWORD	colorHDR = ( PRM_HDR_INIT_ALPHA ) | COLOR_WHITE;
	for( int i=HDR_POLYGON_SIZE ; i<HDR_POLYGON_SIZE_MAX ; i+=HDR_POLYGON_SIZE )
	{
		HDR->Render(  i,0,HDR_SIZE,HDR_SIZE, 0,0,HDR_SIZE,HDR_SIZE, shader,"add", colorHDR );
		HDR->Render( -i,0,HDR_SIZE,HDR_SIZE, 0,0,HDR_SIZE,HDR_SIZE, shader,"add", colorHDR );
		HDR->Render( 0,i,HDR_SIZE,HDR_SIZE, 0,0,HDR_SIZE,HDR_SIZE,  shader,"add", colorHDR );
		HDR->Render( 0,-i,HDR_SIZE,HDR_SIZE, 0,0,HDR_SIZE,HDR_SIZE, shader,"add", colorHDR );

		HDR->Render(  i,0,HDR_SIZE,HDR_SIZE, 0,0,HDR_SIZE,HDR_SIZE, shader2D,"gauss", colorHDR );
		HDR->Render( -i,0,HDR_SIZE,HDR_SIZE, 0,0,HDR_SIZE,HDR_SIZE, shader2D,"gauss", colorHDR );
		HDR->Render( 0,i,HDR_SIZE,HDR_SIZE, 0,0,HDR_SIZE,HDR_SIZE,  shader2D,"gauss", colorHDR );
		HDR->Render( 0,-i,HDR_SIZE,HDR_SIZE, 0,0,HDR_SIZE,HDR_SIZE, shader2D,"gauss", colorHDR );

		//	だんだん透明に
		colorHDR -= PRM_HDR_DECR_ALPHA;
	}
	
}

//--------------------------------------------------------------------------------
//	引数: なし
//	機能: 被写界深度を描画する
//--------------------------------------------------------------------------------
void sceneEvent::RenderDepth()
{
	Depth->RenderTarget();
	CAMERA.Begin();

	//	物体描画
	stage->Render( "depth" );
	player->EventRender( "depth" );

	shader2D->SetValue( "DepthTex",Depth->lpTexture );
}

//*****************************************************************************************************************************
//
//		描画関連
//
//*****************************************************************************************************************************
void	sceneEvent::Render()
{
	//	フレームバッファのポインタ保存
	iexSystem::GetDevice()->GetRenderTarget( 0, &pBackBuffer );

	RenderHDR();											//	バッファにHDR用描画
	RenderShadow();											//	シャドウマップ作成
	screen->RenderTarget();	

	//	カメラ設定
	CAMERA.SetViewPort( PRM_VIEWPORT_N );
	CAMERA.SetProjection( PRM_PROJECTION_N );
	CAMERA.Begin();

	stage->Render();										//	背景描画
	player->EventRender( "copy_fx2" );						//	プレイヤー描画
	LenzFlare();											//	レンズフレア描画
	HDR->Render( 0,0,SCREEN_WIDTH,SCREEN_HEIGHT,0,0,HDR_SIZE,HDR_SIZE,shader2D,"add",HDR_ALPHA );	//	擬似ＨＤＲ描画

	//	画面に色調を変えて復元	
	screen->Render( 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT, shader2D, "Mastering" );

	//	フレームバッファへ切り替え
	iexSystem::GetDevice()->SetRenderTarget( 0, pBackBuffer );
	screen->Render( 0,0,SCREEN_WIDTH,SCREEN_HEIGHT, 0,0,SCREEN_WIDTH,SCREEN_HEIGHT, shader2D, "copy" );

	//	深度情報描画
	RenderDepth();
	//	スクリーンをぼかす
	screen->RenderTarget();

	screen->Render( DOF_COORD_1,SCREEN_WIDTH,SCREEN_HEIGHT,0,0,SCREEN_WIDTH,SCREEN_HEIGHT,shader2D,"copy",DOF_ALPHA_1 );
	screen->Render( DOF_COORD_2,SCREEN_WIDTH,SCREEN_HEIGHT,0,0,SCREEN_WIDTH,SCREEN_HEIGHT,shader2D,"copy",DOF_ALPHA_2 );
	screen->Render( DOF_COORD_3,SCREEN_WIDTH,SCREEN_HEIGHT,0,0,SCREEN_WIDTH,SCREEN_HEIGHT,shader2D,"copy",DOF_ALPHA_3 );
	screen->Render( DOF_COORD_4,SCREEN_WIDTH,SCREEN_HEIGHT,0,0,SCREEN_WIDTH,SCREEN_HEIGHT,shader2D,"copy",DOF_ALPHA_4 );
	screen->Render( DOF_COORD_5,SCREEN_WIDTH,SCREEN_HEIGHT,0,0,SCREEN_WIDTH,SCREEN_HEIGHT,shader2D,"copy",DOF_ALPHA_5 );
	//	フレームバッファへ切り替え
	iexSystem::GetDevice()->SetRenderTarget( 0,pBackBuffer );
	//	被写界深度
	screen->Render( 0,0,SCREEN_WIDTH,SCREEN_HEIGHT,0,0,SCREEN_WIDTH,SCREEN_HEIGHT,shader2D,"focus" );


	/* 2D描画 */
	Frame->Render( 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT, shader2D, "copy" );	//	枠描画
	Font->RenderStr( MES_X, MES_Y, message, MES_SHADOW_COLOR, MES_COLOR );										//	文字列描画	
	fade->Render();																								//	フェードIO描画


}

//*****************************************************************************************************************************
//
//		処理
//
//*****************************************************************************************************************************

//--------------------------------------------------------------------------------
//	引数: ファイル名、プレイヤーの座標
//	機能: ステージを切り替え、プレイヤーを指定座標へセットする
//--------------------------------------------------------------------------------
void sceneEvent::ChageStage( char* name,Vector3& pos )
{
	stage->Load( name );
	player->SetPos( pos );
}

//--------------------------------------------------------------------------------
//	引数: ステージ切り替え番号
//	機能: データを読み込んでn番のステージに切り替える
//--------------------------------------------------------------------------------
void sceneEvent::ChageStage( int n )
{
	char name[NAME_MAX];
	Vector3 pos;

	//	読み込み
	char file[LOAD_NAME_MAX];
	sprintf( file,"DATA\\TXT\\%d.txt",n );
	FILE* fp = fopen( file,"rt" );
	fscanf( fp,"%s",name );
	fscanf( fp,"%f,%f,%f",&pos.x,&pos.y,&pos.z );
	fclose( fp );

	ChageStage( name,pos );
}

//--------------------------------------------------------------------------------
//	引数: なし
//	機能: レンズフレアを描画する
//--------------------------------------------------------------------------------
void sceneEvent::LenzFlare()
{
	//	太陽位置
	Vector3 Sun( SUN_COORD );
	Matrix m = matView * matProjection;
	float SunX = Sun.x * m._11 + Sun.y * m._21 + Sun.z * m._31 + m._41;
	float SunY = Sun.x * m._12 + Sun.y * m._22 + Sun.z * m._32 + m._42;
	float z    = Sun.x * m._13 + Sun.y * m._23 + Sun.z * m._33 + m._43;
	float w    = Sun.x * m._14 + Sun.y * m._24 + Sun.z * m._34 + m._44;
	if( z < 0 )return;
	SunX = ( SunX/w + FLARE_BIAS ) * ( SCREEN_WIDTH/2 );
	SunY = ( -SunY/w + FLARE_BIAS )* ( SCREEN_HEIGHT/2 );


	//	光源から画面中心へのベクトル
	float vx = ( SCREEN_HALF_WIDTH ) - SunX;
	float vy = ( SCREEN_HALF_HEIGHT ) - SunY;

	//	フレアの間隔
	float FlareRate[] = {
		FLARE_RATE
	};

	//	フレアのタイプ
	int flareType[] = {
		FLARE_PTN
	};

	//	フレアのサイズ
	int flareSize[] = {
		FLARE_SIZE
	};

	//	フレアの色
	DWORD flareColor[] = {
		FLARE_COLOR
	};

	for( int i=0; i < FLARE_MAX; i++ ){
		int fx = (int)( FlareRate[i] * vx + SunX );
		int fy = (int)( FlareRate[i] * vy + SunY );
		
		flare->Render( fx-flareSize[i],fy-flareSize[i],flareSize[i]*2,flareSize[i]*2,flareType[i]*128,0,128,128,RS_ADD,flareColor[i] );
	}

	
}

//--------------------------------------------------------------------------------
//	引数: 表示するメッセージ
//	機能: 文字列を描画する
//--------------------------------------------------------------------------------
void sceneEvent::SetMessage( char* msg )
{
	if(msg==NULL){message[0]='\0';return;	}	//	描画文字がなければ戻る
	CopyMemory(message,msg,lstrlen(msg)+1);
}

//--------------------------------------------------------------------------------
//	引数: 待つカウント
//	機能: スクリプトのウェイト時間の設定
//--------------------------------------------------------------------------------
void sceneEvent::SetCount( int s_count )
{
	count = s_count;
}

//--------------------------------------------------------------------------------
//	引数: なし
//	機能: プレイヤーを移動させる
//--------------------------------------------------------------------------------
void sceneEvent::SetWalk()
{
	player->EventWalk();
}

//--------------------------------------------------------------------------------
//	引数: モーション番号
//	機能: プレイヤーのモーション変更
//--------------------------------------------------------------------------------
void sceneEvent::SetMotion( int m )
{
	player->SetMotion( m );
}

//--------------------------------------------------------------------------------
//	引数: 座標
//	機能: カメラ座標設定
//--------------------------------------------------------------------------------
void sceneEvent::SetCameraPos( Vector3& p )
{
	CAMERA.SetPos( p );
}

//--------------------------------------------------------------------------------
//	引数: 座標
//	機能: カメラターゲット設定
//--------------------------------------------------------------------------------
void sceneEvent::SetCameraTarget( Vector3& t )
{
	CAMERA.SetTarget( t );
}

//--------------------------------------------------------------------------------
//	引数: なし
//	機能: カメラターゲットをプレイヤーに
//--------------------------------------------------------------------------------
void sceneEvent::SetCameraTargetToPlayer()
{
	CAMERA.SetTargetToPlayer();
}

//--------------------------------------------------------------------------------
//	引数: モード番号
//	機能: カメラモード切り替え
//--------------------------------------------------------------------------------
void sceneEvent::ChangeCamera( int m )
{
	CAMERA.SetMode( m );
}

//--------------------------------------------------------------------------------
//	引数: 距離
//	機能: 被写界深度の焦点距離設定
//--------------------------------------------------------------------------------
void sceneEvent::ChangeFocusDist( float dist )
{
	m_FocusDist = dist;
}

//--------------------------------------------------------------------------------
//	引数: 範囲
//	機能: 被写界深度の焦点範囲設定
//--------------------------------------------------------------------------------
void sceneEvent::ChangeFocusRange( float range )
{
	m_FocusRange = range;
}

//*****************************************************************************************************************************
//
//		シャドウマップ
//
//*****************************************************************************************************************************

//	シャドウ初期化
void sceneEvent::InitShadow()
{
	ShadowTex = new iex2DObj( SHADOW_SIZE,SHADOW_SIZE, IEX2D_RENDERTARGET );
	iexSystem::GetDevice()->CreateDepthStencilSurface( SHADOW_SIZE, SHADOW_SIZE, D3DFMT_D16, D3DMULTISAMPLE_NONE, 0, FALSE, &ShadowZ, NULL );
}

//	シャドウマップ作成
void sceneEvent::RenderShadow()
{
	LPDIRECT3DSURFACE9	org;
	iexSystem::GetDevice()->GetRenderTarget( 0, &org );

	//	レンダーターゲット設定
	ShadowTex->RenderTarget();
	//	Ｚバッファ設定
	Surface*	orgZ;
	iexSystem::GetDevice()->GetDepthStencilSurface( &orgZ );
	iexSystem::GetDevice()->SetDepthStencilSurface(ShadowZ);

	//	ライト方向
	Vector3 dir = EVENT_LIGHT_DIRECTION;
	dir.Normalize();

	//	シャドウ作成
	Vector3	target = player->GetPos();
	Vector3 pos = target - dir*SHADOW_DIST;
	Vector3 up( 0, 1, 0 );			//	カメラ上方向設定
	//	視点とライト位置へ
	D3DXMATRIX	ShadowMat, work;
	LookAtLH( ShadowMat, pos, target, up );
	D3DXMatrixOrthoLH( &work, SHADOW_PROJ_SIZE, SHADOW_PROJ_SIZE, SHADOW_PROJ_NEAR, SHADOW_PROJ_FAR );	//	平行投影行列(範囲100x100)
	ShadowMat *= work;

	shader->SetValue( "ShadowProjection",  &ShadowMat );

	D3DVIEWPORT9 vp = { 0,0,SHADOW_SIZE,SHADOW_SIZE, 0, 1.0f };
	iexSystem::GetDevice()->SetViewport( &vp );
	//	レンダリング
	iexSystem::GetDevice()->Clear( 0, NULL, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER, COLOR_F_WHITE, 1.0f, 0 );

	//	描画
	stage->Render( "ShadowBuf" );
	player->Render( "ShadowBuf" );

	shader->SetValue( "ShadowMap",ShadowTex->lpTexture );

	//	レンダーターゲット復元
	iexSystem::GetDevice()->SetRenderTarget( 0, org );
	iexSystem::GetDevice()->SetDepthStencilSurface(orgZ);
}