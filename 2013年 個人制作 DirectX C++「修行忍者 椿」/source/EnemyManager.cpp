#include	"iextreme.h"
#include	"system/system.h"

#include	"Collision.h"
#include	"Enemy.h"


//**************************************************************************************//
//																						//
//																						//
//																						//
//**************************************************************************************//



//**************************************************************************************//
//																						//
//	敵管理																				//
//																						//
//**************************************************************************************//
EnemyManager::EnemyManager()
{
	for(int i=0;i<ENEMY_MAX;i++){
		enemy[i] = NULL;
	}
	destroy = 0;

	AppearStep = ENEMY_FIRST;	//	出現段階：第一陣

	eff_manager = new c_EffectManager();	//	エフェクト管理クラス
}

EnemyManager::~EnemyManager()
{
	for(int i=0;i<ENEMY_MAX;i++){
		DEL( enemy[i] );
	}
	DEL( eff_manager );
}

//**************************************************************************************//
//																						//
//	全敵更新																			//
//																						//
//**************************************************************************************//
void EnemyManager::Update( Vector3& ppos )
{
	//	更新と生存確認
	for(int i=0;i<ENEMY_MAX;i++){
		if( enemy[i] == NULL )continue;
		enemy[i]->Update( ppos );
		for( int j=0;j<i;j++ ){
			if( enemy[j] == NULL )continue;
			enemy[i]->SetPos( collision->CheckPos( enemy[j],enemy[i] ) );
		}
		//	消滅処理
		int hp = enemy[i]->GetHP();
		if( hp <= 0 ){	//	敵を撃破
			eff_manager->ExplosionBlack( &enemy[i]->GetPos() );
			eff_manager->ExplosionRed( &enemy[i]->GetPos() );
			destroy++;
			delete enemy[i];
			enemy[i] = NULL;
		}
	}

	eff_manager->Update();	//	エフェクト更新

	switch( AppearStep ){
		case ENEMY_FIRST:
			if( ppos.z >= ENEMY_FIRST_POS ){
				for(int i=0;i<ENEMY_APPEAR_MAX;i++)Appear( FILE_PATH_ENEMY01, ENEMY_01, Vector3( 0.0f, 1.0f, ppos.z + ENEMY_APPEAR_BIAS1 ) , 0.0f, ENEMY01_SIZE, ENEMY01_HP );
				AppearStep++;
				break;
			}
			break;
		case ENEMY_SECOND: 
			if( ppos.z >= ENEMY_SECOND_POS ){
				for(int i=0;i<ENEMY_APPEAR_MAX;i++)Appear( FILE_PATH_ENEMY02, ENEMY_02, Vector3( 0.0f, 1.0f, ppos.z + ENEMY_APPEAR_BIAS2 ) , 0.0f, ENEMY02_SIZE, ENEMY02_HP );
				AppearStep++;
				break;
			}
			break;
		case ENEMY_THIRD:
			if( ppos.z >= ENEMY_THIRD_POS ){
				for(int i=0;i<ENEMY_APPEAR_MAX;i++)Appear( FILE_PATH_ENEMY03, ENEMY_03, Vector3( 0.0f, 1.0f, ppos.z + ENEMY_APPEAR_BIAS3 ) , 0.0f, ENEMY03_SIZE, ENEMY03_HP );
				AppearStep++;
				break;
			}
			break;
	}

//	//	デバッグ処理
//#ifdef _DEBUG
//
//	if(KEY(KEY_SPACE) == PUSH_KEY){
//		Appear( FILE_PATH_ENEMY01, ENEMY_01, Vector3( 0,3,0 ) ,0.0f,ENEMY01_SIZE,ENEMY01_HP );
//	}
//#endif

}

//**************************************************************************************//
//																						//
//	全敵描画																			//
//																						//
//**************************************************************************************//
void EnemyManager::Render( char* name )
{
	for(int i=0;i<ENEMY_MAX;i++){
		if( enemy[i] == NULL )continue;
		enemy[i]->Render( name );
	}
	eff_manager->Render();
}

//**************************************************************************************//
//																						//
//	敵管理処理																			//
//																						//
//**************************************************************************************//

//--------------------------------------------------------------------------------
//	引数: 敵のファイル名、敵の種類、敵の位置、敵の向く方向、大きさ、体力
//	機能: 指定した種類の敵を出現させる
//--------------------------------------------------------------------------------
void EnemyManager::Appear(char *filename, int kind, Vector3 &pos, float angle,float scale,int hp)
{
	//	空き検索
	int index = -1;

	//	空き検索
	for(int i=0;i<ENEMY_MAX;i++){
		if( enemy[i] != NULL )continue;
		index = i;
		break;
	}

	//	敵出現UVエフェクトセット
	eff_manager->uvAppear( pos, Vector3( 0.0f,angle,0.0f ), 0.15f );


	enemy[index] = new ENEMY( filename, kind );
	enemy[index]->SetScale( scale );
	enemy[index]->SetPos( pos );
	enemy[index]->SetAngle( angle );
	enemy[index]->SetHP( hp );

}

//--------------------------------------------------------------------------------
//	引数: 敵の番号
//	機能: 指定した種類の敵を消す
//--------------------------------------------------------------------------------
void EnemyManager::Reset( int no )
{
	if( no == -1 )	//	デストラクタ読み込み後
	{
		for( int i=0 ; i<ENEMY_MAX ; i++ )
		{
			if( enemy[i] != NULL ) delete enemy[i];	//	削除して
			enemy[i] = NULL;	//	NULLに
		}
		return;
	}
	if( enemy[no] != NULL ) delete enemy[no];	//	中身があれば削除
	enemy[no] = NULL;	//	NULLに
}

//--------------------------------------------------------------------------------
//	引数: なし
//	機能: クリア条件を満たしているかチェックする
//--------------------------------------------------------------------------------
bool EnemyManager::CheckClear()
{
	if( destroy >= ENEMY_MAX ){	//	クリア条件を満たしたら
		static int timer = 0;
		
		timer++;
		
		if( timer >= 60*3 )return true;
	}

	return false;
}

//--------------------------------------------------------------------------------
//	引数: プレイヤー座標
//	機能: 当たり判定
//--------------------------------------------------------------------------------
bool EnemyManager::CrashCheck( Vector3* ppos, int power )
{
	for( int i=0 ; i<ENEMY_MAX ; i++ )
	{
		if( enemy[i] != NULL )
		{
			int	n = enemy[i]->CrashCheck(ppos,power);
			//	当たり判定
			if( n == ENEMY_DEATH )			//	敵死亡
			{
				//	死亡処理
				return true;
			}
			if( n == ENEMY_HIT ){			//	敵にヒット
				enemy[i]->ModeChange( MODE_DAMAGE );
				return true;
			}

		}
	}
	return false;
}