#include	"iextreme.h"
#include	"system/system.h"

#include	"Environment.h"


//**************************************************************************************//
//																						//
//																						//
//																						//
//**************************************************************************************//


//**************************************************************************************//
//																						//
//		コンストラクタ・デストラクタ													//
//																						//
//**************************************************************************************//

//---------------------------------
//	コンストラクタ
//---------------------------------
c_Sky::c_Sky()
{
	//	雲読み込み
	m_lpcloud = new iex2DObj( FILE_PATH_ENV_CLOUD );
	//	座標初期設定
	for( int i=0;i<CLOUD_MAX;i++ ){	//	128 = 16種類*8コ
		Pos[i] = CLOUD_INITIAL_POS;
	}
}

//---------------------------------
//	デストラクタ
//---------------------------------
c_Sky::~c_Sky()
{
	DEL( m_lpcloud );
}

//**************************************************************************************//
//																						//
//		描画																			//
//																						//
//**************************************************************************************//

void c_Sky::Render()
{
	//	雲描画
	for( int i=0;i<CLOUD_MAX;i++ ){	//	128 = 16種類*8コ
		CloudRender( i/CLOUD_TYPE_BIAS,Pos[i] );
	}
}

//**************************************************************************************//
//																						//
//		処理																			//
//																						//
//**************************************************************************************//

//--------------------------------------------------------------------------------
//	引数: 雲の種類、雲の位置
//	機能: 雲を描画する
//--------------------------------------------------------------------------------
void c_Sky::CloudRender(int type, Vector3& pos)
{
	LVERTEX v[CLOUD_VERTEX_MAX];	//	頂点カラー付き頂点

	const int d = 60;	//	頂点間隔

	//	頂点設定
	v[CLOUD_VERTEX1].x = pos.x -d;
	v[CLOUD_VERTEX1].y = pos.y;
	v[CLOUD_VERTEX1].z = pos.z -d;
	v[CLOUD_VERTEX1].color = COLOR_F_WHITE;
	v[CLOUD_VERTEX2].x = pos.x + d;
	v[CLOUD_VERTEX2].y = pos.y;
	v[CLOUD_VERTEX2].z = pos.z - d;
	v[CLOUD_VERTEX2].color = COLOR_F_WHITE;
	v[CLOUD_VERTEX3].x = pos.x -d;
	v[CLOUD_VERTEX3].y = pos.y;
	v[CLOUD_VERTEX3].z = pos.z + d;
	v[CLOUD_VERTEX3].color = COLOR_F_WHITE;
	v[CLOUD_VERTEX4].x = pos.x + d;
	v[CLOUD_VERTEX4].y = pos.y;
	v[CLOUD_VERTEX4].z = pos.z + d;
	v[CLOUD_VERTEX4].color = COLOR_F_WHITE;

	const float  su = (type%CLOUD_VERTEX_MAX) * CLOUD_UV_BIAS;
	const float  sv = (type/CLOUD_VERTEX_MAX) * CLOUD_UV_BIAS;
	v[CLOUD_VERTEX1].tu = su;
	v[CLOUD_VERTEX1].tv = sv;
	v[CLOUD_VERTEX2].tu = su + CLOUD_UV_BIAS;
	v[CLOUD_VERTEX2].tv = sv;
	v[CLOUD_VERTEX3].tu = su;
	v[CLOUD_VERTEX3].tv = sv + CLOUD_UV_BIAS;
	v[CLOUD_VERTEX4].tu = su + CLOUD_UV_BIAS;
	v[CLOUD_VERTEX4].tv = sv + CLOUD_UV_BIAS;

	iexPolygon::Render3D( v,CLOUD_POLY_MAX,m_lpcloud,shader, "add" );

}
