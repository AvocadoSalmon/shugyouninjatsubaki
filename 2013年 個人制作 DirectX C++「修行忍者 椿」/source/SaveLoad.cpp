#include	"iextreme.h"
#include	"system/system.h"

#include	<stdio.h>
#include	"SaveLoad.h"

//**************************************************************************************//
//																						//
//																						//
//																						//
//**************************************************************************************//




//**************************************************************************************//
//																						//
//		処理																			//
//																						//
//**************************************************************************************//
bool Save( char* filename,c_PLAYER *player )
{
	Vector3 pos = player->GetPos();		//プレイヤー座標

	FILE *fp = NULL;
	//	ファイルのオープン
	if( fopen_s( &fp, filename, "wb" ) != 0 )return false;
	//	データ書き込み
	fwrite( &pos, sizeof(Vector3), 1, fp );
	fclose( fp );

	return true;
}

bool Load( char* filename,c_PLAYER *player )
{
	Vector3 pos;	//プレイヤー座標

	FILE *fp = NULL;
	//	ファイルのオープン
	if( fopen_s( &fp, filename, "rb" ) != 0 )return false;
	//	データ読み込み
	fread( &pos, sizeof(Vector3), 1, fp );
	fclose( fp );

	player->SetPos( pos );	//	座標をセット

	return true;
}
