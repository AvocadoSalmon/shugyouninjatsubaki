#ifndef _BASE_OBJ_H_
#define _BASE_OBJ_H_

//*****************************************************************************
//
//		キャラ基本クラス
//
//*****************************************************************************

#define OBJECT_SIZE_COLLIDE	0.5f					//	当たりの大きさ
#define OBJECT_SIZE_BASIC	0.01f					//	オブジェクトサイズ
#define GRAVITY				0.01f					//	重力値
#define IK_QUOLITY			10						//	IKのクオリティ
#define	BASE_2D_BIAS		Vector3( 0,1.5f,0 )		//	２D変換座標値の調整値

class BaseOBJ{
private:

protected:
	iex3DObj* obj;
	Vector3 Pos;		//	座標
	Vector3	Move;		//	移動量
	float	angle;		//	方向
	float	scale;		//	モデルの大きさ
	float	size;		//	当たり判定の大きさ
	float	gravity;	//	Y軸スピード
	bool	bAlive;		//	生存フラグ

public:

	BaseOBJ();
	BaseOBJ(char* name);
	~BaseOBJ();

	void Update();		//	更新	
	void Render();		//	描画
	void Render( char* techname );

	void SetMotion( int n );				//	モーション設定
	bool GroundCheck();

	//	セッター
	void SetPos( Vector3 &p ){ Pos = p; }
	void SetAngle( float Angle ){ angle = Angle; }
	void SetScale( float Scale ){ scale = Scale; }
	void SetAlive( bool alive ){ bAlive = alive; }

	//	ゲッター
	Vector3 GetPos(){ return Pos; }
	float GetAngle(){ return angle; }
	float GetScale(){ return scale; }
	float GetSize(){ return size; }
	bool GetAlive(){ return bAlive; }
	Vector3	Get2DCoord( Vector3 &pos );
};

#endif	//	_BASE_OBJ_H_