#ifndef _EFF_MANAGER_H_
#define _EFF_MANAGER_H_

//*****************************************************************************
//
//		エフェクト管理クラス
//
//*****************************************************************************

#define EFF_UV_MAX					24										//	UVアニメーションモデル最大数
#define EFF_SIZE_BASIC				Vector3(0.01f,0.01f,0.01f)				//	モデル基本サイズ
#define EFF_ALPHA_BASIC				1.0f									//	モデルテクスチャ基本アルファ
#define EFF_APPEAR_TIME				60 * 1									//	敵出現エフェクトの出現時間
#define EFF_APPEAR_ANIME			0.25f									//	敵出現エフェクトのUVアニメ値
#define EFF_ANIMATION_MAX			1.0f									//	エフェクトの最大UV値

#define EFF_PARTICLE_LOOP_MAX		5										//	パーティクルの数
#define EFF_KUNAI_APPEAR_LOOP_MAX	1										//	クナイ出現パーティクルの数

//	UVアニメーションモデルリスト
enum{
	UV_APPEAR,	//	敵出現モデル
};

//**************************************
//	エフェクト基本クラス
//**************************************
class c_Effect
{
protected:
	iex3DObj* m_lpEffect;
	Vector3 Pos;
	Vector3 Angle;
	Vector3 Scale;
	float tu,tv;
	float alpha;
	int timer;
	bool bRender;

public:
	~c_Effect(){}

	void Update(){}
	void Render(){}

	void SetScale(Vector3 scale){ Scale = scale; }
	void SetPos(Vector3 pos){ Pos = pos; }
	void SetAngle(Vector3 angle){ Angle = angle; }

};

//**************************************
//	出現エフェクトクラス
//**************************************
class c_EffAppear : public c_Effect
{
private:


public:
	c_EffAppear();
	c_EffAppear( char* filename );
	~c_EffAppear();

	void Update();
	void Render();
};

//**************************************
//	エフェクト管理クラス
//**************************************

class c_EffectManager
{
private:
	c_EffAppear* appear[EFF_UV_MAX];

public:
	c_EffectManager();
	~c_EffectManager();

	void Update();
	void Render();

	void uvAppear(Vector3 pos, Vector3 angle,float scale);

	void ExplosionRed( Vector3* pos );
	void ExplosionBlack( Vector3* pos );
	void NinjutuFire( Vector3 *pos );
};


#endif	//	_EFF_MANAGER_H_