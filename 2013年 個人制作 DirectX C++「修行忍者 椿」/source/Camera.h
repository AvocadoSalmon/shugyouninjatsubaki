#ifndef _CAMERA_H_
#define _CAMERA_H_

#include	"Player.h"



//	カメラモード
#define CAMERA_NORMAL	0
#define CAMERA_MOVE		1
#define CAMERA_EVENT	2
//	通常カメラ距離判定用
#define CAMERA_NEAR		7.0f
#define CAMERA_FAR		10.0f


//*****************************************************************************
//
//		カメラクラス
//
//*****************************************************************************
class Camera
{
private:
	Vector3	Pos;
	Vector3	Move;
	Vector3	Target;
	float NearLimit;
	float FarLimit;
	int	mode;
	iexView* view;
public:
	Camera();
	~Camera();
	void Update( c_PLAYER* player );
	void Begin();

	void BasicCamera();	//	基本カメラ
	bool MoveCamera(Vector3 pos,float dist=0.0f);	//	移動カメラ

	//	セッター
	void SetPos(Vector3& p){ Pos = p; }
	void SetTarget(Vector3& t){ Target = t; }
	void SetTargetToPlayer(){ Target = Vector3( player->GetPos().x,player->GetPos().y + 8.0f,player->GetPos().z ); }
	void SetViewPort(int x, int y, int w, int h);
	void SetProjection(float fovY,float Near,float Far,float Asp);
	void SetMode( int m ){ mode = m; }

	//	ゲッター
	Vector3 GetPos(){ return Pos; }
	Vector3 GetTarget(){ return Target; }

	static Camera &GetInstance(){
		static Camera	camera;
		return camera;
	}
};

#define CAMERA Camera::GetInstance()


#endif	//	_CAMERA_H_