#ifndef __THREADOBJECT_H__
#define __THREADOBJECT_H__

#define LOAD_STATE_EXEC 0
#define LOAD_STATE_END 1

class cThreadObject
{
protected:
	int		_State;
	bool	_bRender;
	float	_fFrameTime;
public:
	cThreadObject();
	virtual ~cThreadObject();

	bool CheckFrameRate();
	virtual void Exec() = 0;
	virtual void Render() = 0;
	
	inline int GetState(){ return _State; }
	inline void SetState( int State ){ _State = State; }
	inline bool GetRender(){ return _bRender; }
};

class cThread
{
private:
	cThreadObject* _ThreadObject;
	
	DWORD	_ThreadId;
	HANDLE	_Thread;
	
public:
	cThread( cThreadObject* Object );
	~cThread();

	void Start();
	void End();

};

#endif // __THREADOBJECT_H__