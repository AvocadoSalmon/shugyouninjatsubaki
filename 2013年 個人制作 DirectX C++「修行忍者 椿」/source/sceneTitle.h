#ifndef _TITLE_H_
#define _TITLE_H_

#include	"Fade.h"
#include	"Camera.h"
#include	"Player.h"

/* マクロ定義 */
#define TITLE_LIGHT_DIRECTION	Vector3( 1,-0.5f,1 )					//	ライト方向
#define TITLE_LIGHT_COLOR		0.5f, 0.5f, 0.45f						//	ライト色
#define TITLE_AMBIENT_COLOR		0x202020								//	アンビエント色

#define TITLE_FILTER_CONTRAST	1.0f									//	スクリーンフィルタコントラスト値
#define TITLE_FILTER_CHROMA		1.0f									//	スクリーンフィルタコントラスト値
#define TITLE_FILTER_COLOR		Vector3( 1.2f, 1.0f, 1.0f )				//	スクリーンフィルタコントラスト値

#define	TITLE_SHADOW_PROJ_SIZE	5										//	影の投影サイズ
#define	TITLE_SHADOW_PROJ_NEAR	1										//	影の投影近Z
#define	TITLE_SHADOW_PROJ_FAR	30										//	影の投影遠Z
#define	TITLE_SHADOW_DIST		20										//	ターゲットからの距離

#define	TITLE_FIRST_FOSUS_DIST	-15.0f									//	被写界深度の初期焦点距離
#define	TITLE_INC_FOSUS_DIST	  0.2f									//	被写界深度の焦点距離増加量
#define	TITLE_MAX_FOSUS_DIST	  5.0f									//	被写界深度の最大焦点距離
#define	TITLE_FIRST_FOSUS_RANGE	  0.0f									//	被写界深度の初期焦点範囲
#define	TITLE_INC_FOSUS_RANGE	  0.2f									//	被写界深度の焦点範囲増加量
#define	TITLE_MAX_FOSUS_RANGE	 10.0f									//	被写界深度の最大焦点範囲

#define TARGET_Y_BIAS			1.0f									//	カメラターゲットY座標調整値
#define PLAYER_MOTION_WAIT		1										//	プレイヤー待機モーション
#define TITLE_PLAYER_FIRST_POS	Vector3( 0.0f,0.2f,10.0f )				//	プレイヤーの初期座標
#define TITLE_PLAYER_SCALE		0.005f									//	プレイヤーの初期スケール
	
#define ALPHA_MAX				255										//	アルファの最大値
#define SHIFT_VALUE				24										//	アルファ処理のシフトの値
#define X_COORD_MAX				1280									//	X幅の最大値
#define COUNT_INTERVAL			10										//	文字表示の間隔
#define X_SLIDE					64										//	表示をずらす間隔

#define TITLE_LOGO_X			250										//	タイトルロゴのX座標
#define TITLE_LOGO_Y			150										//	タイトルロゴのY座標
#define TITLE_LOGO_WIDTH		768										//	タイトルロゴの幅
#define TITLE_LOGO_HEIGHT		256										//	タイトルロゴの高さ
#define TITLE_LOGO_READ_COORD_X 0										//	タイトルロゴの読み込みX座標
#define TITLE_LOGO_READ_COORD_Y 835										//	タイトルロゴの読み込みY座標
#define TITLE_COUNT_LIMIT_M		60 + 30									//	タイトルカウンタ（メインへ）（モーション時間+猶予）
#define TITLE_COUNT_LIMIT_D		120 + 30								//	タイトルカウンタ（ウィンドウ破棄）（モーション時間+猶予）

#define PRESS_COORD_X			500										//	PRESS ENTERのX座標
#define PRESS_COORD_Y			500										//	PRESS ENTERのY座標
#define PRESS_WIDTH				315										//	PRESS ENTERの幅
#define PRESS_HEIGHT			110										//	PRESS ENTERの高さ
#define PRESS_READ_COORD_X		0										//	PRESS ENTERの読み込みX座標
#define PRESS_READ_COORD_Y		721										//	PRESS ENTERの読み込みY座標
#define PRESS_DESTROY_COUNT		0.08f									//	PRESS ENTERの文字消滅までのカウント増加値

#define PRESS_END_READ_COORD_X	64*5									//	終了ボタンの読み取りＸ座標
#define PRESS_END_COORD_X		PRESS_COORD_X + 80						//	終了ボタンのX座標
#define PRESS_END_COORD_Y		PRESS_COORD_Y + 110						//	終了ボタンのY座標
#define PRESS_END_WIDTH			64*2									//	終了ボタンの幅

#define PRESS_CURSOR_X			PRESS_COORD_X-100						//	カーソルのX座標
#define PRESS_CURSOR_Y_MIN		PRESS_COORD_Y-20						//	カーソルの初期Y座標
#define PRESS_CURSOR_Y_MAX		PRESS_END_COORD_Y-20					//	カーソルの最大Y座標
#define PRESS_CURSOR_WIDTH		768										//	カーソルの幅
#define PRESS_CURSOR_HEIGHT		128										//	カーソルの高さ
#define PRESS_CURSOR_COORD_X	1280									//	カーソルの読み込み位置X座標
#define PRESS_CURSOR_COORD_Y	0										//	カーソルの読み込み位置Y座標

#define CAMERA_FIRST_POS		Vector3(0,2.0f,4.5f)					//	カメラ初期座標
#define CAMERA_STOP				2.0f									//	カメラをキャラの手前いくらで止めるか

#define TITLE_PARTICLE_MAX		2048									//	パーティクル最大数

#define TITLE_STAGE_SIZE		0.5f									//	ステージサイズ
#define TITLE_SKY_COLOR			Vector3( 0.1f, 0.1f, 0.1f )				//	空の色
#define TITLE_GROUND_COLOR		Vector3( 0.04f, 0.04f, 0.04f )			//	地面の色

#define SAKURA_LOOP_MAX			5										//	桜パーティクルループ数
#define SAKURA_COORD			&Vector3( 4,4,10 )						//	桜吹雪の座標
#define SAKURA_MOVE_X			(rand()%-50)*-0.0006f					//	桜移動量X
#define SAKURA_MOVE_Y			(rand()%-50)*-0.0003f					//	桜移動量Y
#define SAKURA_MOVE_Z			(rand()%-50)*-0.0002f					//	桜移動量Z
#define SAKURA_TYPE				10										//	桜パーティクルのタイプ
#define SAKURA_MID_FRAME		250										//	桜パーティクル中間フレーム
#define SAKURA_LAST_FRAME		350										//	桜パーティクル最終フレーム
#define SAKURA_ROTATE			(rand()%50) * 0.001f					//	桜回転量
#define SAKURA_SCALE			0.01f									//	桜スケール
#define SAKURA_STRETCH			1.0f									//	桜伸縮量(デフォルト:1.0)
#define SAKURA_POWER_X			(rand()%-50)*-0.00001f					//	桜移動増加量X
#define SAKURA_POWER_Y			0.0f									//	桜移動増加量Y
#define SAKURA_POWER_Z			-0.00001f								//	桜移動増加量Z

//	コマンドタイプ
enum{
	FIRST_VALUE = -1,	//	初期値
	TITLE_TO_MAIN,		//	メインモードへ
	DESTROY_WINDOW,		//	ウィンドウを破棄
};

//	効果音タイプ
enum TITLE{
	SE_SELECT,
	SE_DICISION,
	SE_LOAD,
};

class sceneTitle : public Scene
{
private:
	iex2DObj* m_font;		//	タイトル文字画像
	c_Fade*		m_fade;
	iexMesh*	m_lpBack;	//	背景
	iex3DObj*	m_lpPlayer;	//	プレイヤーモデル
	float m_PlayerScale;	//	プレイヤースケール
	Vector3 m_PlayerPos;	//	プレイヤー座標
	iex2DObj* m_film;		//	フロント画像
	iex2DObj* m_filmback;	//	フロント背景画像
	iex2DObj *screen;		//	スクリーンフィルタ
	Surface* pBackBuffer;	//	フレームバッファ
	iex2DObj*	HDR;		//	擬似ＨＤＲ
	iex2DObj* lightsamp;	//	ライトマップ
	int m_cursorY;			//	カーソルY座標
	int m_counter;			//	シーン切り替え用カウンター
	bool m_bPush;			//	Enterを押したかどうか
	int m_EndType;			//	タイトル終了タイプ
	iexSound *m_sound;		//	SE
	LPDSSTREAM m_bgm;		//	BGM
	iex2DObj*	ShadowTex;	//	シャドウテクスチャ
	Surface*	ShadowZ;	//	影Zバッファ
	iex2DObj*	Depth;		//	被写界深度
	bool m_bCameraStop;		//	カメラ停止フラグ
	float m_FocusDist;		//	焦点距離
	float m_FocusRange;		//	焦点範囲

public:
	sceneTitle(){};
	~sceneTitle();

	bool Initialize();
	void Update();
	void Render();

	void PressEnter();
	void Sakura( Vector3* pos );
	void RenderHDR();		//	HDR描画
	void InitShadow();		//	影初期化
	void RenderShadow();	//	影描画
	void RenderDepth();		//	被写界深度描画

	
};


#endif	//	_TITLE_H_