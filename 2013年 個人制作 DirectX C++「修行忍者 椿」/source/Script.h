#ifndef _SCRIPT_H_
#define _SCRIPT_H_

//************************************************************************************
//
//	スクリプトクラス
//
//************************************************************************************
class c_Script{
private:
	Vector3 Pos;
public:
	/* 初期化・解放 */
	BOOL	LoadScript( LPSTR filename );
	BOOL	LoadScript2( LPSTR filename );
	BOOL	ReleaseScript();

	/* 値のゲッター */
	void	GetParamS( LPSTR buf );
	int		GetParamN( void );
	float	GetParamFloat( void );
	Vector3	GetPos();
	
	BOOL	SetScriptIP( int ip );
	BOOL	SearchParamTop( void );
	BOOL	SearchTop( void );
	BOOL	DEC_Main( void );

	BOOL	Decipherment( LPSTR Command );
	
	
	//	各コマンド用関数
	void EventWait();					//	イベント中の各コマンド間の待ち時間設定
	void SetMSG();						//	メッセージ配置設定
	void EventWalk();					//	プレイヤー移動フラグ設定
	void EventMotion();					//	プレイヤーモーション設定
	void EventStop();					//	プレイヤー停止設定
	void EventEnd();					//	イベントモード終了
	void SceneShift();					//	メインモードへの切り替え設定
	void SetCameraPos();				//	カメラ座標設定
	void SetCameraTarget();				//	カメラターゲット設定
	void SetCameraTargetToPlayer();		//	カメラターゲットをプレイヤーに
	void SetCameraMode();				//	カメラモード設定
	void SetFocusDist();				//	被写界深度焦点距離設定
	void SetFocusRange();				//	被写界深度焦点範囲設定
};

#endif //	_SCRIPT_H_