#ifndef _ENVIRONMENT_H_
#define _ENVIRONMENT_H_


#define CLOUD_MAX			16*30																//	_|S¶¬
#define CLOUD_INITIAL_POS	Vector3( rand()%600-300*1.0f,300-i*0.1f,rand()%1800-1000*1.0f )		//	_úÀW
#define CLOUD_TYPE_BIAS		48																	//	_^Cv²®l
#define CLOUD_VERTEX_MAX	4																	//	_Åå¸_
#define CLOUD_UV_BIAS		0.25f																//	_UV²®l
#define CLOUD_POLY_MAX		2																	//	_1Â ½èÌOp|S

//	_¸_
enum{
	CLOUD_VERTEX1,
	CLOUD_VERTEX2,
	CLOUD_VERTEX3,
	CLOUD_VERTEX4,
};

class c_Sky
{
private:
	iex2DObj* m_lpcloud;		//	_æ
	Vector3 Pos[CLOUD_MAX];		//	_|SÀW
public:
	c_Sky();
	~c_Sky();

	void CloudRender(int type,Vector3& pos);
	void Render();
};



#endif	//	_ENVIRONMENT_H_