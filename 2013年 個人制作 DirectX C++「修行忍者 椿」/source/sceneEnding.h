#ifndef _ENDING_H_
#define _ENDING_H_

#include	"Fade.h"
#include	"Camera.h"
#include	"Player.h"

#define ENDING_AMBIENT_COLOR		0x202020													//	アンビエント色
#define ENDING_COUNT				60*3														//	カウント
#define ENDING_BG_PARAM				0,0,1280,720,0,0,1280,720,shader2D,"copy"					//	背景画像パラメータ
#define ENDING_CLEAR_PARAM			250,300,768,128,1280,128,768,128,shader2D,"copy"			//	背景画像パラメータ

class sceneEnding : public Scene
{
private:
	iex2DObj* m_ending;		//	画像
	c_Fade*		fade;
	iex2DObj* lpClear;		//	クリア表示
	int		  m_count;
	LPDSSTREAM m_bgm;		//	BGM

public:
	sceneEnding(){};
	~sceneEnding();

	bool Initialize();
	void Update();
	void Render();

	
};


#endif	//	_ENDING_H_