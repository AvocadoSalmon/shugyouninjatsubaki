#ifdef _DEBUG

#include	"iextreme.h"
#include	"Lib_Debug.h"

namespace Debug
{
//================================================================================//
//
//		CValueItem
//
//================================================================================//
template<class _Type1, class _Type2>
CValueItem< _Type1, _Type2>::CValueItem( u32 GroupId, const string String, _Type1* Value, _Type1 Step, _Type1 Min, _Type1 Max, const string Atr, SRect* Rect, bool ReadOnly )
: CItem( GroupId, String, Atr, Rect, ReadOnly )
{
	
	_Value = Value;
	_Step = Step;
	_Min = Min;
	_Max = Max;
}

template<class _Type1, class _Type2>
void CValueItem< _Type1, _Type2>::Action( ACTION Action, CMenu *Menu )
{
	switch(Action)
	{
	case ACT_LEFT:
		*_Value -= _Step;
		if( _Min != _Max )
		{
			if( (( _Type2 )*_Value ) < (( _Type2 )_Min ) )
			{
				*_Value = _Min;
			}
		}
		if(_ValueCallback)_ValueCallback( _Value );
	break;

	case ACT_RIGHT:
		*_Value += _Step;

		if( _Min != _Max )
		{
			if( (( _Type2 )*_Value ) > (( _Type2 )_Max ) )
			{
				*_Value = _Max;
			}
		}
		if(_ValueCallback)_ValueCallback( _Value );
	break;

	case ACT_SELECT:
		if(_PushCallback)_PushCallback( _Value );
	break;

	case ACT_NONE:
	break;
	}
}

template<class _Type1, class _Type2>
void CValueItem< _Type1, _Type2>::Draw( u32 MaxWidth, bool Active )
{
	char str[DB_STRING_LENGTH];
	char atr[DB_ATR_STRING_LEGTH];
	COLOR color;
	COLOR db_select_back_c, db_normal_c, db_select_text_c, db_atribute_c, db_separator_c;

	if( Active )
	{
		db_select_back_c = DB_SELECT_BACK_COLOR;
		db_normal_c = DB_NORMAL_TEXT_COLOR;
		db_select_text_c = DB_SELECT_TEXT_COLOR;
		db_atribute_c = DB_ATRIBUTE_TEXT_COLRO;
		db_separator_c = DB_SEPARATOR_COLOR;
	}
	else
	{
		db_select_back_c = DB_SELECT_BACK_NONE_ACTIVE_COLOR;
		db_normal_c = DB_NORMAL_TEXT_NONE_ACTIVE_COLOR;
		db_select_text_c = DB_SELECT_TEXT_NONE_ACTIVE_COLOR;
		db_atribute_c = DB_ATRIBUTE_TEXT_NONE_ACTIVE_COLRO;
		db_separator_c = DB_SEPARATOR_NONE_ACTIVE_COLOR;		
	}
	//	選択描画
	if( _Select )
	{	
		color = db_select_text_c;
		IEX_DrawRect( _Rect.x, _Rect.y+1, MaxWidth, _Rect.h-1, RS_COPY, db_select_back_c );
	}
	else color = db_normal_c;

	//セパレート
	if(_Separator)
	IEX_DrawRect( _Rect.x, _Rect.y + _Rect.h, MaxWidth, 1, RS_COPY, db_separator_c );


	if(_Rect.h == NORMAL_FONT_SIZE)
	{
		sprintf_s( str, _String.c_str(), *_Value );
		_Rect.w = (u32)strlen(str) * DB_FONT_SIZE_W;
		IEX_DrawText( str, _Rect.x, _Rect.y, _Rect.w, _Rect.h, color );
		
		strcpy_s( atr , _Atr.c_str());
		if(!_Atr.empty())IEX_DrawText( atr, _Rect.x + _Rect.w, _Rect.y, (int)_Atr.length() * DB_FONT_SIZE / 2, DB_FONT_SIZE, db_atribute_c);
	}else{
		sprintf_s( str, _String.c_str(), *_Value );
		_Rect.w = (u32)strlen(str) * DB_FONT_SIZE_W;
		IEX_DrawText( str, _Rect.x, _Rect.y, _Rect.w, _Rect.h, color, true );
		
		strcpy_s( atr , _Atr.c_str());
		if(!_Atr.empty())IEX_DrawText( atr, _Rect.x + _Rect.w, _Rect.y, (int)_Atr.length() * DB_FONT_SIZE / 2, DB_FONT_SIZE, db_atribute_c, true );
	}

	//	横幅の最大値更新<--メニューの横幅に影響
	_MaxWidth = ((u32)strlen(str) + (u32)strlen(atr)) * DB_FONT_SIZE_W;
}

//================================================================================//
//
//		CBoolItem
//
//================================================================================//
CBoolItem::CBoolItem( u32 GroupId, const string String, bool* Value, const string List, const string Atr, SRect* Rect, bool ReadOnly )
: CItem( GroupId, String, Atr, Rect, ReadOnly )
, _List(List)
{
	_Value = Value;
}

void CBoolItem::Action( ACTION Action, CMenu* Menu )
{
	static int step;
	switch(Action)
	{
	case ACT_LEFT:case ACT_RIGHT:
		*_Value ^= 1;
		if(_ValueCallback)_ValueCallback( _Value );
	break;

	case ACT_SELECT:
		if(_PushCallback)_PushCallback( _Value );
	break;

	case ACT_NONE:
	break;
	}
}

void CBoolItem::Draw( u32 MaxWidth, bool Active )
{
	char str[DB_STRING_LENGTH];
	char atr[DB_ATR_STRING_LEGTH];
	string value;
	COLOR color;
	COLOR db_select_back_c, db_normal_c, db_select_text_c, db_atribute_c, db_separator_c;

	if( Active )
	{
		db_select_back_c = DB_SELECT_BACK_COLOR;
		db_normal_c = DB_NORMAL_TEXT_COLOR;
		db_select_text_c = DB_SELECT_TEXT_COLOR;
		db_atribute_c = DB_ATRIBUTE_TEXT_COLRO;
		db_separator_c = DB_SEPARATOR_COLOR;
	}
	else
	{
		db_select_back_c = DB_SELECT_BACK_NONE_ACTIVE_COLOR;
		db_normal_c = DB_NORMAL_TEXT_NONE_ACTIVE_COLOR;
		db_select_text_c = DB_SELECT_TEXT_NONE_ACTIVE_COLOR;
		db_atribute_c = DB_ATRIBUTE_TEXT_NONE_ACTIVE_COLRO;
		db_separator_c = DB_SEPARATOR_NONE_ACTIVE_COLOR;		
	}
	//	選択描画
	if( _Select )
	{	
		color = db_select_text_c;
		IEX_DrawRect( _Rect.x, _Rect.y+1, MaxWidth, _Rect.h-1, RS_COPY, db_select_back_c );
	}
	else color = db_normal_c;

	//セパレート
	if(_Separator)
	IEX_DrawRect( _Rect.x, _Rect.y + _Rect.h, MaxWidth, 1, RS_COPY, db_separator_c );


	if(*_Value){
		value = _List.substr( _List.find('|')+1);
	}else{
		value = _List.substr( 0, _List.find('|') );
	}

	if(_Rect.h == NORMAL_FONT_SIZE)
	{
		wsprintf( str, _String.c_str(), value.c_str() );
		_Rect.w = (u32)strlen(str) * DB_FONT_SIZE_W;
		IEX_DrawText( str, _Rect.x, _Rect.y, _Rect.w, _Rect.h, color );
		
		strcpy_s( atr , _Atr.c_str());
		if(!_Atr.empty())IEX_DrawText( atr, _Rect.x + _Rect.w, _Rect.y, (int)_Atr.length() * DB_FONT_SIZE / 2, DB_FONT_SIZE, db_atribute_c);
	}else{
		wsprintf( str, _String.c_str(), value.c_str() );
		_Rect.w = (u32)strlen(str) * DB_FONT_SIZE_W;
		IEX_DrawText( str, _Rect.x, _Rect.y, _Rect.w, _Rect.h, color, true );
		
		strcpy_s( atr , _Atr.c_str());
		if(!_Atr.empty())IEX_DrawText( atr, _Rect.x + _Rect.w, _Rect.y, (int)_Atr.length() * DB_FONT_SIZE / 2, DB_FONT_SIZE, db_atribute_c, true );
	}

	//	横幅の最大値更新<--メニューの横幅に影響
	_MaxWidth = ((u32)strlen(str) + (u32)strlen(atr)) * DB_FONT_SIZE_W;
}


//================================================================================//
//
//		CBracketItem
//
//================================================================================//
CBracketItem::CBracketItem( u32 GroupId, u32 BracketGroupId, const string String, const string Atr, SRect* Rect, bool ReadOnly, bool PutEnable ) : CItem( GroupId, String, Atr, Rect, ReadOnly )
{
	_BracketGroupId = BracketGroupId;
	_PutEnable = PutEnable;
}

void CBracketItem::Action( ACTION Action, CMenu* Menu )
{
	CMenu* BracketMenu = CDebug::FindMenu( _BracketGroupId );
	int ypos;
	if(_PutEnable)ypos = _Rect.y;
	else ypos = Menu->GetRect().y;

	switch(Action)
	{
	case ACT_RIGHT:
		if( BracketMenu && BracketMenu->GetItemList()->GetCount() > 0)
		{
			Menu->SetActive( false );
			BracketMenu->Open( _Rect.x + Menu->GetItemList()->GetMaxWidth() / 3, ypos );
			BracketMenu->SetBeforeMenu( Menu );
		}
		if(_ValueCallback)_ValueCallback( NULL );
	break;

	case ACT_SELECT:
		if(_PushCallback)_PushCallback( NULL );
	break;

	case ACT_NONE:
	break;
	}
}
//================================================================================//
//
//		CEventItem
//
//================================================================================//
CEventItem::CEventItem( u32 GroupId, const string String, const string Atr, SRect* Rect, bool ReadOnly ) : CItem( GroupId, String, Atr, Rect, ReadOnly )
{

}

void CEventItem::Action( ACTION Action, CMenu* Menu )
{
	switch(Action)
	{
	case ACT_SELECT:
		if(_PushCallback)_PushCallback( NULL );
	break;
	}
}



//================================================================================//
//
//		CEnumItem
//
//================================================================================//
CEnumItem::CEnumItem( u32 GroupId, const string String, const string List, int* Value, const string Atr, SRect* Rect, bool ReadOnly )
: CItem( GroupId, String, Atr, Rect, ReadOnly ),
_List( List ),
_Num(1),
_Value(Value)
{
	for( int i = 0;i < (int)List.length(); i++)
	{
		if( List[i] == '|' )_Num++;
	}
}

string CEnumItem::GetStringList( int Num )
{
	string str = _List;

	if( 0 > Num )return "";
	if( _Num < Num )return "";

	if( Num == 0 )
	{
		if( str.find('|') == string::npos )return str;
		return str.erase(str.find('|'));
	}

	for( int i = 0; i < Num; i++)
	{
		str.erase(0, str.find('|')+1);
		
	}
	if(Num < _Num-1)str.erase(str.find('|'));

	return str;
}

void CEnumItem::Action( ACTION Action, CMenu* Menu )
{
	switch(Action)
	{
	case ACT_LEFT:
		*_Value -= 1;
		if( *_Value < 0)
		{
			*_Value = _Num-1;
		}
		if(_ValueCallback)_ValueCallback( _Value );
	break;

	case ACT_RIGHT:
		*_Value += 1;
		if( *_Value >= _Num )
		{
			*_Value = 0;
		}

		if(_ValueCallback)_ValueCallback( _Value );
	break;

	case ACT_SELECT:
		if(_PushCallback)_PushCallback( _Value );
	break;

	case ACT_NONE:
	break;
	}
}

void CEnumItem::Draw( u32 MaxWidth, bool Active )
{
	char str[DB_STRING_LENGTH];
	char atr[DB_ATR_STRING_LEGTH];
	string value;
	COLOR color;
	COLOR db_select_back_c, db_normal_c, db_select_text_c, db_atribute_c, db_separator_c;

	if( Active )
	{
		db_select_back_c = DB_SELECT_BACK_COLOR;
		db_normal_c = DB_NORMAL_TEXT_COLOR;
		db_select_text_c = DB_SELECT_TEXT_COLOR;
		db_atribute_c = DB_ATRIBUTE_TEXT_COLRO;
		db_separator_c = DB_SEPARATOR_COLOR;
	}
	else
	{
		db_select_back_c = DB_SELECT_BACK_NONE_ACTIVE_COLOR;
		db_normal_c = DB_NORMAL_TEXT_NONE_ACTIVE_COLOR;
		db_select_text_c = DB_SELECT_TEXT_NONE_ACTIVE_COLOR;
		db_atribute_c = DB_ATRIBUTE_TEXT_NONE_ACTIVE_COLRO;
		db_separator_c = DB_SEPARATOR_NONE_ACTIVE_COLOR;		
	}
	//	選択描画
	if( _Select )
	{	
		color = db_select_text_c;
		IEX_DrawRect( _Rect.x, _Rect.y+1, MaxWidth, _Rect.h-1, RS_COPY, db_select_back_c );
	}
	else color = db_normal_c;

	//セパレート
	if(_Separator)
	IEX_DrawRect( _Rect.x, _Rect.y + _Rect.h, MaxWidth, 1, RS_COPY, db_separator_c );


	value = GetStringList( *_Value );

	if(_Rect.h == NORMAL_FONT_SIZE)
	{
		wsprintf( str, _String.c_str(), value.c_str() );
		_Rect.w = (u32)strlen(str) * DB_FONT_SIZE_W;
		IEX_DrawText( str, _Rect.x, _Rect.y, _Rect.w, _Rect.h, color );
		
		strcpy_s( atr , _Atr.c_str());
		if(!_Atr.empty())IEX_DrawText( atr, _Rect.x + _Rect.w, _Rect.y, (int)_Atr.length() * DB_FONT_SIZE / 2, DB_FONT_SIZE, db_atribute_c);
	}else{
		wsprintf( str, _String.c_str(), value.c_str() );
		_Rect.w = (u32)strlen(str) * DB_FONT_SIZE_W;
		IEX_DrawText( str, _Rect.x, _Rect.y, _Rect.w, _Rect.h, color, true );
		
		strcpy_s( atr , _Atr.c_str());
		if(!_Atr.empty())IEX_DrawText( atr, _Rect.x + _Rect.w, _Rect.y, (int)_Atr.length() * DB_FONT_SIZE / 2, DB_FONT_SIZE, db_atribute_c, true );
	}

	//	横幅の最大値更新<--メニューの横幅に影響
	_MaxWidth = ((u32)strlen(str) + (u32)strlen(atr)) * DB_FONT_SIZE_W;
}

//================================================================================//
//
//		CTitleItem
//
//================================================================================//
CTitleItem::CTitleItem( u32 GroupId, const string String, SRect* Rect, bool ReadOnly ) : CItem( GroupId, String, "", Rect, ReadOnly )
{

}

void CTitleItem::Draw( u32 MaxWidth, bool Active )
{
	char str[DB_STRING_LENGTH];
	COLOR db_select_back_c, db_normal_c, db_select_text_c, db_title_c, db_atribute_c, db_separator_c;

	if( Active )
	{
		db_select_back_c = DB_SELECT_BACK_COLOR;
		db_normal_c = DB_NORMAL_TEXT_COLOR;
		db_title_c = DB_SELECT_TEXT_COLOR;
		db_select_text_c = DB_SELECT_TEXT_COLOR;
		db_atribute_c = DB_ATRIBUTE_TEXT_COLRO;
		db_separator_c = DB_SEPARATOR_COLOR;
	}
	else
	{
		db_select_back_c = DB_SELECT_BACK_NONE_ACTIVE_COLOR;
		db_normal_c = DB_NORMAL_TEXT_NONE_ACTIVE_COLOR;
		db_title_c = DB_SELECT_TEXT_NONE_ACTIVE_COLOR;
		db_select_text_c = DB_SELECT_TEXT_NONE_ACTIVE_COLOR;
		db_atribute_c = DB_ATRIBUTE_TEXT_NONE_ACTIVE_COLRO;
		db_separator_c = DB_SEPARATOR_NONE_ACTIVE_COLOR;	
	}

	//	選択描画
	if( _Select )
	{	
		IEX_DrawRect( _Rect.x, _Rect.y+1, MaxWidth, _Rect.h-1, RS_COPY, db_select_back_c );
	}
	else
	{
		IEX_DrawRect( _Rect.x, _Rect.y+1, MaxWidth, _Rect.h-1, RS_COPY, db_normal_c );
	}

	//セパレート
	if(_Separator)
	IEX_DrawRect( _Rect.x, _Rect.y + _Rect.h, MaxWidth, 1, RS_COPY, db_separator_c );

	if(_Rect.h == NORMAL_FONT_SIZE)
	{
		strcpy_s( str , _String.c_str());
		IEX_DrawText( str, _Rect.x, _Rect.y, _Rect.w, _Rect.h, db_select_text_c );
		strcpy_s( str , _Atr.c_str());
		if(!_Atr.empty())IEX_DrawText( str, _Rect.x + _Rect.w, _Rect.y, (int)_Atr.length() * DB_FONT_SIZE / 2, DB_FONT_SIZE, db_atribute_c);
	}else{
		strcpy_s( str , _String.c_str());
		IEX_DrawText( str, _Rect.x, _Rect.y, _Rect.w, _Rect.h, db_select_text_c, true );
		strcpy_s( str , _Atr.c_str());
		if(!_Atr.empty())IEX_DrawText( str, _Rect.x + _Rect.w, _Rect.y, (int)_Atr.length() * DB_FONT_SIZE / 2, DB_FONT_SIZE, db_atribute_c, true );
	}

	//	横幅の最大値更新<--メニューの横幅に影響
	_MaxWidth = ((u32)_String.length() + (u32)_Atr.length()) * DB_FONT_SIZE_W;
}

//================================================================================//
//
//		CStringItem
//
//================================================================================//
CStringItem::CStringItem( u32 GroupId, const string String, char* Value, const string Atr, SRect* Rect, bool ReadOnly ) : CItem( GroupId, String, Atr, Rect, ReadOnly ),
_Value(Value)
{
}

void CStringItem::Draw(  u32 MaxWidth, bool Active )
{
	
	char str[DB_STRING_LENGTH];
	char atr[DB_ATR_STRING_LEGTH];
	COLOR color;
	COLOR db_select_back_c, db_normal_c, db_select_text_c, db_atribute_c, db_separator_c;

	if( Active )
	{
		db_select_back_c = DB_SELECT_BACK_COLOR;
		db_normal_c = DB_NORMAL_TEXT_COLOR;
		db_select_text_c = DB_SELECT_TEXT_COLOR;
		db_atribute_c = DB_ATRIBUTE_TEXT_COLRO;
		db_separator_c = DB_SEPARATOR_COLOR;
	}
	else
	{
		db_select_back_c = DB_SELECT_BACK_NONE_ACTIVE_COLOR;
		db_normal_c = DB_NORMAL_TEXT_NONE_ACTIVE_COLOR;
		db_select_text_c = DB_SELECT_TEXT_NONE_ACTIVE_COLOR;
		db_atribute_c = DB_ATRIBUTE_TEXT_NONE_ACTIVE_COLRO;
		db_separator_c = DB_SEPARATOR_NONE_ACTIVE_COLOR;		
	}

	//	選択描画
	if( _Select )
	{	
		color = db_select_text_c;
		IEX_DrawRect( _Rect.x, _Rect.y+1, MaxWidth, _Rect.h-1, RS_COPY, db_select_back_c );
		//CPolygon::RenderRect( _Rect.x, _Rect.y+1, MaxWidth, _Rect.h-1, RS_COPY, db_select_back_c );
	}
	else color = db_normal_c;

	//セパレート
	if(_Separator)
	IEX_DrawRect( _Rect.x, _Rect.y + _Rect.h, MaxWidth, 1, RS_COPY, db_separator_c );

	if(_Rect.h == NORMAL_FONT_SIZE)
	{
		sprintf_s( str , _String.c_str(), _Value);
		_Rect.w = (u32)strlen(str) * DB_FONT_SIZE_W;
		IEX_DrawText( str, _Rect.x, _Rect.y, _Rect.w, _Rect.h, color );
		strcpy_s( atr , _Atr.c_str());
		if(!_Atr.empty())IEX_DrawText( atr, _Rect.x + _Rect.w, _Rect.y, (int)_Atr.length() * DB_FONT_SIZE / 2, DB_FONT_SIZE, db_atribute_c);
	}else{
		sprintf_s( str , _String.c_str(), _Value);
		_Rect.w = (u32)strlen(str) * DB_FONT_SIZE_W;
		IEX_DrawText( str, _Rect.x, _Rect.y, _Rect.w, _Rect.h, color, true );
		strcpy_s( atr , _Atr.c_str());
		if(!_Atr.empty())IEX_DrawText( atr, _Rect.x + _Rect.w, _Rect.y, (int)_Atr.length() * DB_FONT_SIZE / 2, DB_FONT_SIZE, db_atribute_c, true );
	}

	//	横幅の最大値更新<--メニューの横幅に影響
	_MaxWidth = ((u32)strlen(str) + (u32)strlen(atr)) * DB_FONT_SIZE_W;

}

//================================================================================//
//
//		CItem
//
//================================================================================//
CItem::CItem( u32 GroupId, const string String, const string Atr, SRect* Rect, bool ReadOnly ):
_String(String),
_GroupId(GroupId),
_Atr(Atr),
_Select(false),
_ReadOnly(ReadOnly),
_Separator(false),
_MaxWidth(0),
_PushCallback(null),
_ValueCallback(null)
{
	_Rect.x = Rect->x;
	_Rect.y = Rect->y;
	_Rect.w = (u32)_String.length() * DB_FONT_SIZE_W;
	_Rect.h = DB_FONT_SIZE;
}

void CItem::Action( ACTION Action, CMenu* Menu )
{
	switch( Action )
	{
	case ACT_LEFT:case ACT_RIGHT:
		if(_ValueCallback)_ValueCallback( NULL );
	break;

	case ACT_SELECT:
		if(_PushCallback)_PushCallback( NULL );
	break;

	case ACT_NONE:
	break;
	}
}

void CItem::Draw(  u32 MaxWidth, bool Active )
{
	
	char str[DB_STRING_LENGTH];
	COLOR color;
	COLOR db_select_back_c, db_normal_c, db_select_text_c, db_atribute_c, db_separator_c;

	if( Active )
	{
		db_select_back_c = DB_SELECT_BACK_COLOR;
		db_normal_c = DB_NORMAL_TEXT_COLOR;
		db_select_text_c = DB_SELECT_TEXT_COLOR;
		db_atribute_c = DB_ATRIBUTE_TEXT_COLRO;
		db_separator_c = DB_SEPARATOR_COLOR;
	}
	else
	{
		db_select_back_c = DB_SELECT_BACK_NONE_ACTIVE_COLOR;
		db_normal_c = DB_NORMAL_TEXT_NONE_ACTIVE_COLOR;
		db_select_text_c = DB_SELECT_TEXT_NONE_ACTIVE_COLOR;
		db_atribute_c = DB_ATRIBUTE_TEXT_NONE_ACTIVE_COLRO;
		db_separator_c = DB_SEPARATOR_NONE_ACTIVE_COLOR;		
	}

	//	選択描画
	if( _Select )
	{	
		color = db_select_text_c;
		IEX_DrawRect( _Rect.x, _Rect.y+1, MaxWidth, _Rect.h-1, RS_COPY, db_select_back_c );
		//CPolygon::RenderRect( _Rect.x, _Rect.y+1, MaxWidth, _Rect.h-1, RS_COPY, db_select_back_c );
	}
	else color = db_normal_c;

	//セパレート
	if(_Separator)
	IEX_DrawRect( _Rect.x, _Rect.y + _Rect.h, MaxWidth, 1, RS_COPY, db_separator_c );

	if(_Rect.h == NORMAL_FONT_SIZE)
	{
		strcpy_s( str , _String.c_str());
		IEX_DrawText( str, _Rect.x, _Rect.y, _Rect.w, _Rect.h, color );
		strcpy_s( str , _Atr.c_str());
		if(!_Atr.empty())IEX_DrawText( str, _Rect.x + _Rect.w, _Rect.y, (int)_Atr.length() * DB_FONT_SIZE / 2, DB_FONT_SIZE, db_atribute_c);
	}else{
		strcpy_s( str , _String.c_str());
		IEX_DrawText( str, _Rect.x, _Rect.y, _Rect.w, _Rect.h, color, true );
		strcpy_s( str , _Atr.c_str());
		if(!_Atr.empty())IEX_DrawText( str, _Rect.x + _Rect.w, _Rect.y, (int)_Atr.length() * DB_FONT_SIZE / 2, DB_FONT_SIZE, db_atribute_c, true );
	}

	//	横幅の最大値更新<--メニューの横幅に影響
	_MaxWidth = ((u32)_String.length() + (u32)_Atr.length()) * DB_FONT_SIZE_W;

}

void CItem::UpdatePosition( int x, int y )
{
	_Rect.x = x;
	_Rect.y = y;
}

//================================================================================//
//
//		CItemList
//
//================================================================================//
CItemList::CItemList()
{
	_MaxWidth = 0;
	_Count = 0;
	
	for(int i =0;i < _ItemNum;i++)
	_Item[i] = NULL;
}

CItemList::~CItemList()
{
	Clear();
}

void CItemList::Clear()
{
	for(int i = 0;i < _ItemNum;i++)
	{
		if(_Item[i])
		{
			delete _Item[i];
			_Item[i] = NULL;
		}
	}
	_Count = 0;
}

void CItemList::Update()
{
	for(int i = 0;i < _ItemNum;i++)
	{
		if(i == 0)_MaxWidth = 0;
		if(!_Item[i])continue;
		SetMaxWidth(_Item[i]->GetMaxWidth());
	}
}

void CItemList::SetItem( CItem* Item, u32 Width )
{
	SetMaxWidth( Width );

	_Item[_Count++] = Item;
	_LastItem = Item;
}

CItem* CItemList::GetItem( u32 GroupId, u32 Num )
{
	if(!_Item[Num])return NULL;
	if(_ItemNum < Num)return NULL;

	if( _Item[Num]->GetGroupId() == GroupId )
	{
		return _Item[Num];
	}
	return NULL;
}

//================================================================================//
//
//		CMenu
//
//================================================================================//

CMenu::CMenu()
{
	_GroupId = INVALID_GROUP;
	_Rect.x = _Rect.y = _Rect.w = _Rect.h = 0;
	_SelectNo = 0;
	_Open = false;
	_Active = false;

	_BeforeMenu = NULL;
}

CMenu::~CMenu()
{
	Clear();
}

void CMenu::Clear()
{
	_GroupId = INVALID_GROUP;
	_Rect.x = _Rect.y = _Rect.w = _Rect.h = 0;
	_SelectNo = 0;
	_Open = false;
	_Active = false;

	_BeforeMenu = NULL;

	_ItemList.Clear();
}


void CMenu::Init()
{

}

void CMenu::Exec()
{

	static int step = 0;
	CItemList* list = GetItemList();
	list->Update();

	//if( !list->GetItem( _GroupId, _SelectNo) )return;

	//	UP
	if( KEY_Get( DB_BUT_UP ) == FIRST_PRESS )
	{	
		list->GetItem( _GroupId, _SelectNo )->SetSelect(false);

		_SelectNo--;
		if(_SelectNo < 0)_SelectNo = list->GetCount()-1;

		list->GetItem( _GroupId, _SelectNo )->SetSelect(true);
	}
	else if( KEY_Get( DB_BUT_UP ) == KEEP_PRESS )
	{
		step++;
		if( step >= DB_PRESS_WAITTIME )
		{

			list->GetItem( _GroupId, _SelectNo )->SetSelect(false);

			_SelectNo--;
			if(_SelectNo < 0)_SelectNo = list->GetCount()-1;

			list->GetItem( _GroupId, _SelectNo )->SetSelect(true);

			step = DB_PRESS_WAITTIME - 2;
		}
	}
	else if( KEY_Get( DB_BUT_UP ) == FIRST_SEPARATE )
	{
		step = 0;
	}
	//	DOWN
	else if( KEY_Get( DB_BUT_DOWN ) == FIRST_PRESS )
	{
		list->GetItem( _GroupId, _SelectNo )->SetSelect(false);

		_SelectNo++;
		if(_SelectNo > list->GetCount()-1 )_SelectNo = 0;

		list->GetItem( _GroupId, _SelectNo )->SetSelect(true);
	}
	else if( KEY_Get( DB_BUT_DOWN) == KEEP_PRESS )
	{
		step++;
		if( step >= DB_PRESS_WAITTIME )
		{
			list->GetItem( _GroupId, _SelectNo )->SetSelect(false);

			_SelectNo++;
			if(_SelectNo > list->GetCount()-1 )_SelectNo = 0;

			list->GetItem( _GroupId, _SelectNo )->SetSelect(true);

			step = DB_PRESS_WAITTIME - 2;
		}

	}
	else if( KEY_Get( DB_BUT_DOWN) == FIRST_SEPARATE )
	{
		step = 0;
	}
	//	LEFT
	else if( KEY_Get( DB_BUT_LEFT ) == FIRST_PRESS )
	{

		if( list->GetItem( _GroupId, _SelectNo )->IsReadOnly() == false )
		{
			list->GetItem( _GroupId, _SelectNo )->Action( ACT_LEFT, this );
		}
	}
	else if( KEY_Get( DB_BUT_LEFT ) == KEEP_PRESS )
	{
		step++;
		if( step >= DB_PRESS_WAITTIME )
		{
			if( list->GetItem( _GroupId, _SelectNo )->IsReadOnly() == false )
			{
				list->GetItem( _GroupId, _SelectNo )->Action( ACT_LEFT, this );
			}

			step = DB_PRESS_WAITTIME - 2;
		}
	}
	else if( KEY_Get( DB_BUT_LEFT ) == FIRST_SEPARATE )
	{
		step = 0;
	}
	//	RIGHT
	else if( KEY_Get( DB_BUT_RIGHT ) == FIRST_PRESS )
	{
		if( list->GetItem( _GroupId, _SelectNo )->IsReadOnly() == false )
		{
			list->GetItem( _GroupId, _SelectNo )->Action( ACT_RIGHT, this );
		}
	}
	else if( KEY_Get( DB_BUT_RIGHT ) == KEEP_PRESS )
	{
		step++;
		if( step >= DB_PRESS_WAITTIME )
		{
			if( list->GetItem( _GroupId, _SelectNo )->IsReadOnly() == false )
			{
				list->GetItem( _GroupId, _SelectNo )->Action( ACT_RIGHT, this );
			}

			step = DB_PRESS_WAITTIME - 2;
		}
	}
	else if( KEY_Get( DB_BUT_RIGHT ) == FIRST_SEPARATE )
	{
		step = 0;
	}
	//	SELECT
	else if( KEY_Get( DB_BUT_SELECT ) == FIRST_PRESS )
	{
		if( list->GetItem( _GroupId, _SelectNo )->IsReadOnly() == false )
		{
			list->GetItem( _GroupId, _SelectNo )->Action( ACT_SELECT, this );
		}
	}
	else if(  KEY_Get( DB_BUT_CANCEL ) == FIRST_PRESS ) 
	{
		//	派生本のメニューをアクティブ状態
		if( _BeforeMenu )
			_BeforeMenu->SetActive( true );

		//	今アクティブ状態のメニュー閉じる
		Close();
	}
	else
	{
		//if( _SelectNo != -1 )return;
		if( list->GetItem( _GroupId, _SelectNo )->IsReadOnly() == false )
		{
			list->GetItem( _GroupId, _SelectNo )->Action( ACT_NONE, this );
		}
	}
}

void CMenu::Set( u32 GroupId )
{
	_GroupId = GroupId;
}

void CMenu::Open( int x, int y )
{
	_Open = true;
	_Active = true;
	_Rect.x = x; _Rect.y = y;

	CItem* Item = _ItemList.GetItem( _GroupId, _SelectNo );
	if( Item )Item->SetSelect(true);
}

void CMenu::Open()
{
	_Open = true;
	_Active = true;
	CItem* Item = _ItemList.GetItem( _GroupId, _SelectNo );
	if( Item )Item->SetSelect(true);
}

void CMenu::Close()
{
	_Open = false;
	_Active = false;
}

void CMenu::Draw()
{
	//メニューウインドウ
	if( _Active )
	IEX_DrawRect(_Rect.x, _Rect.y, _ItemList.GetMaxWidth(), _ItemList.GetCount() * DB_FONT_SIZE, RS_COPY, DB_MANU_COLOR );
	else IEX_DrawRect(_Rect.x, _Rect.y, _ItemList.GetMaxWidth(), _ItemList.GetCount() * DB_FONT_SIZE, RS_COPY, DB_MENU_NONE_ACTIVE_COLOR );
	//アイテム
	for(int i = 0;i < DB_NUM_ITEM;i++)
	{
		CItem* Item = _ItemList.GetItem( _GroupId, i );
		
		if( !Item )break;
		
		if( Item )
		{
			Item->UpdatePosition( _Rect.x, _Rect.y + i * DB_FONT_SIZE );
			Item->Draw( _ItemList.GetMaxWidth(), _Active );
		}
	}
}

//================================================================================//
//
//		CDebug
//
//================================================================================//

//	メニュー
CMenu CDebug::_Menu[DB_NUM_MENU];

bool CDebug::_ReadOnly = false;

u32 CDebug::_CurrentGroupId = INVALID_GROUP;

//iex2DObject* CDebug::_Object = NULL;

int CDebug::_MenuCount = 0;

CMenu* CDebug::FindMenu( u32 GroupId )
{
	for(int i = 0;i < _MenuNum;i++)
	{
		if( _Menu[i].GetGroupId() == INVALID_GROUP )continue;

		if( _Menu[i].GetGroupId() == GroupId )
		return &_Menu[i];
	}
	return NULL;
}

void CDebug::Init()
{
	//if(!_Object)_Object = NEW( iex2DObject(DEBUG_RESOURCE_PATH) );
}

void CDebug::Rele()
{
	#ifdef _DEBUG

		//DEL( _Object );

		for(int i = 0;i < _MenuNum;i++)
		{
			_Menu[i].Clear();
		}
		_MenuCount = 0;

	#endif
}

void CDebug::Exec()
{
	for(int i = 0;i < _MenuNum;i++)
	{
		if( _Menu[i].GetGroupId() == INVALID_GROUP )continue;

		//	アクティブ状態のメニューのExec実行
		if( _Menu[i].IsActive() )
		{
			_Menu[i].Exec();
			break;
		}
	}
}

bool CDebug::IsOpen( u32 GroupId )
{
	CMenu* Menu = CDebug::FindMenu( GroupId );
	
	return ( Menu ) ? Menu->IsOpen() : false;
}

void CDebug::Open( int x, int y, u32 GroupId )
{
	CMenu* Menu = CDebug::FindMenu( GroupId );

	if( !Menu )return;

	//	アイテムが一つもない場合は開かない<--Menuは存在している状態
	if( !Menu->GetItemList()->GetCount() )return;

	Menu->Open( x, y );
}

void CDebug::Clear( u32 GroupId )
{
	CMenu* Menu = CDebug::FindMenu( GroupId );

	if( Menu )
	{
		Menu->Clear();
		_MenuCount--;
	}
}

void CDebug::NewGroup( u32 GroupId )
{
	for(int i = 0;i < _MenuNum;i++)
	{
		//	すでに存在するＩＤの場合設定できない
		if( _Menu[i].GetGroupId() == GroupId )break;

		if( _Menu[i].GetGroupId() != INVALID_GROUP )continue;

		_Menu[i].Set( GroupId );
		_MenuCount++;
		break;
	}
}

void CDebug::SetGroup( u32 GroupId )
{
	_CurrentGroupId = GroupId;
}

void CDebug::Title( const string String )
{
	CMenu* Menu = CDebug::FindMenu( _CurrentGroupId );
	
	if( Menu )
	{
		CItemList* list = Menu->GetItemList();
		if(list->GetCount() >= DB_NUM_ITEM-1)return;//	最大アイテム登録数を越えた

		list->SetItem( 
			new CTitleItem( _CurrentGroupId, String, &_Menu->GetRect(), true ),
			(u32)String.length() * DB_FONT_SIZE_W );
	}
}

void CDebug::String( const string String )
{
	CMenu* Menu = CDebug::FindMenu( _CurrentGroupId );
	
	if( Menu )
	{
		CItemList* list = Menu->GetItemList();
		if(list->GetCount() >= DB_NUM_ITEM-1)return;//	最大アイテム登録数を越えた

		string atr;

		if( _ReadOnly )atr += DB_ATTRIBUTE_READONLY;

		list->SetItem( 
			new CItem( _CurrentGroupId, String, atr, &_Menu->GetRect(), _ReadOnly ),
			((u32)String.length() + (u32)atr.length()) * DB_FONT_SIZE_W );
	}
}

void CDebug::String( const string String1, char* String2 )
{
	CMenu* Menu = CDebug::FindMenu( _CurrentGroupId );
	
	if( Menu )
	{
		CItemList* list = Menu->GetItemList();
		if(list->GetCount() >= DB_NUM_ITEM-1)return;//	最大アイテム登録数を越えた
		string atr;

		if( _ReadOnly )atr += DB_ATTRIBUTE_READONLY;

		list->SetItem( 
			new CStringItem( _CurrentGroupId, String1, String2, atr, &_Menu->GetRect(), _ReadOnly ),
			((u32)String1.length() + (u32)atr.length()) * DB_FONT_SIZE_W );
	}
}

void CDebug::Bracket( const string String, u32 GroupId, bool PutEnable )
{
	CMenu* Menu = CDebug::FindMenu( _CurrentGroupId );

	//新しいグループ作成
	NewGroup( GroupId );

	if( Menu )
	{
		CItemList* list = Menu->GetItemList();
		string atr;

		if( _ReadOnly )atr += DB_ATTRIBUTE_READONLY;
		if(list->GetCount() >= DB_NUM_ITEM-1)return;//	最大アイテム登録数を越えた
		atr += DB_ATTRIBUTE_BRACKET;

		list->SetItem(
			new CBracketItem( _CurrentGroupId, GroupId, String, atr, &Menu->GetRect(), _ReadOnly, PutEnable ),
			((u32)String.length() + (u32)atr.length()) * DB_FONT_SIZE_W );
	}
}

void CDebug::Event( const string String, CALL_BACK PushCallback )
{
	CMenu* Menu = CDebug::FindMenu( _CurrentGroupId );

	if( Menu )
	{
		CItemList* list = Menu->GetItemList();
		if(list->GetCount() >= DB_NUM_ITEM-1)return;//	最大アイテム登録数を越えた
		string atr;

		if( _ReadOnly )atr += DB_ATTRIBUTE_READONLY;

		list->SetItem(
			new CEventItem( _CurrentGroupId, String, atr, &_Menu->GetRect(), _ReadOnly ),
			((u32)String.length() + (u32)atr.length()) * DB_FONT_SIZE_W );
	}

	PushCall( PushCallback );
}

void CDebug::Enum( const string String, const string List, int* Value )
{
	CMenu* Menu = CDebug::FindMenu( _CurrentGroupId );

	if( Menu )
	{
		CItemList* list = Menu->GetItemList();
		if(list->GetCount() >= DB_NUM_ITEM-1)return;//	最大アイテム登録数を越えた
		string atr;

		if( _ReadOnly )atr += DB_ATTRIBUTE_READONLY;

		list->SetItem(
			new CEnumItem( _CurrentGroupId, String, List, Value, atr, &_Menu->GetRect(), _ReadOnly ),
			((u32)String.length() + (u32)atr.length()) * DB_FONT_SIZE_W );
	}
}

void CDebug::Value( const string String, s32* Value, s32 Step, s32 Min, s32 Max )
{
	CMenu* Menu = CDebug::FindMenu( _CurrentGroupId );

	if( Menu )
	{
		CItemList* list = Menu->GetItemList();
		if(list->GetCount() >= DB_NUM_ITEM-1)return;//	最大アイテム登録数を越えた
		string atr;

		if( _ReadOnly )atr += DB_ATTRIBUTE_READONLY;

		list->SetItem(
			new S32( _CurrentGroupId, String, Value, Step, Min, Max, atr, &_Menu->GetRect(), _ReadOnly ),
			((u32)String.length() + (u32)atr.length()) * DB_FONT_SIZE_W );
	}
}

void CDebug::Value( const string String, u32* Value, u32 Step, u32 Min, u32 Max )
{
	CMenu* Menu = CDebug::FindMenu( _CurrentGroupId );

	if( Menu )
	{
		CItemList* list = Menu->GetItemList();
		if(list->GetCount() >= DB_NUM_ITEM-1)return;//	最大アイテム登録数を越えた
		string atr;

		if( _ReadOnly )atr += DB_ATTRIBUTE_READONLY;

		list->SetItem(
			new U32( _CurrentGroupId, String, Value, Step, Min, Max, atr, &_Menu->GetRect(), _ReadOnly ),
			((u32)String.length() + (u32)atr.length()) * DB_FONT_SIZE_W );
	}
}

void CDebug::Value( const string String, s16* Value, s16 Step, s16 Min, s16 Max )
{
	CMenu* Menu = CDebug::FindMenu( _CurrentGroupId );

	if( Menu )
	{
		CItemList* list = Menu->GetItemList();
		if(list->GetCount() >= DB_NUM_ITEM-1)return;//	最大アイテム登録数を越えた
		string atr;

		if( _ReadOnly )atr += DB_ATTRIBUTE_READONLY;

		list->SetItem(
			new S16( _CurrentGroupId, String, Value, Step, Min, Max, atr, &_Menu->GetRect(), _ReadOnly ),
			((u32)String.length() + (u32)atr.length()) * DB_FONT_SIZE_W );
	}
}

void CDebug::Value( const string String, u16* Value, u16 Step, u16 Min, u16 Max )
{
	CMenu* Menu = CDebug::FindMenu( _CurrentGroupId );

	if( Menu )
	{
		CItemList* list = Menu->GetItemList();
		if(list->GetCount() >= DB_NUM_ITEM-1)return;//	最大アイテム登録数を越えた
		string atr;

		if( _ReadOnly )atr += DB_ATTRIBUTE_READONLY;

		list->SetItem(
			new U16( _CurrentGroupId, String, Value, Step, Min, Max, atr, &_Menu->GetRect(), _ReadOnly ),
			((u32)String.length() + (u32)atr.length()) * DB_FONT_SIZE_W );
	}
}

void CDebug::Value( const string String, s8* Value, s8 Step, s8 Min, s8 Max )
{
	CMenu* Menu = CDebug::FindMenu( _CurrentGroupId );

	if( Menu )
	{
		CItemList* list = Menu->GetItemList();
		if(list->GetCount() >= DB_NUM_ITEM-1)return;//	最大アイテム登録数を越えた
		string atr;

		if( _ReadOnly )atr += DB_ATTRIBUTE_READONLY;

		list->SetItem(
			new S8( _CurrentGroupId, String, Value, Step, Min, Max, atr, &_Menu->GetRect(), _ReadOnly ),
			((u32)String.length() + (u32)atr.length()) * DB_FONT_SIZE_W );
	}
}

void CDebug::Value( const string String, u8* Value, u8 Step, u8 Min, u8 Max )
{
	CMenu* Menu = CDebug::FindMenu( _CurrentGroupId );

	if( Menu )
	{
		CItemList* list = Menu->GetItemList();
		if(list->GetCount() >= DB_NUM_ITEM-1)return;//	最大アイテム登録数を越えた
		string atr;

		if( _ReadOnly )atr += DB_ATTRIBUTE_READONLY;

		list->SetItem(
			new U8( _CurrentGroupId, String, Value, Step, Min, Max, atr, &_Menu->GetRect(), _ReadOnly ),
			((u32)String.length() + (u32)atr.length()) * DB_FONT_SIZE_W );
	}
}

void CDebug::Value( const string String, f32* Value, f32 Step, f32 Min, f32 Max )
{
	CMenu* Menu = CDebug::FindMenu( _CurrentGroupId );

	if( Menu )
	{
		CItemList* list = Menu->GetItemList();
		if(list->GetCount() >= DB_NUM_ITEM-1)return;//	最大アイテム登録数を越えた
		string atr;

		if( _ReadOnly )atr += DB_ATTRIBUTE_READONLY;

		list->SetItem(
			new F32( _CurrentGroupId, String, Value, Step, Min, Max, atr, &_Menu->GetRect(), _ReadOnly ),
			((u32)String.length() + (u32)atr.length()) * DB_FONT_SIZE_W );
	}
}

void CDebug::Value( const string String, bool* Value, const string List )
{
	CMenu* Menu = CDebug::FindMenu( _CurrentGroupId );

	if( Menu )
	{
		CItemList* list = Menu->GetItemList();
		if(list->GetCount() >= DB_NUM_ITEM-1)return;//	最大アイテム登録数を越えた
		string atr;

		if( _ReadOnly )atr += DB_ATTRIBUTE_READONLY;

		list->SetItem(
			new CBoolItem( _CurrentGroupId, String, Value, List, atr, &_Menu->GetRect(), _ReadOnly ),
			((u32)String.length() + (u32)atr.length()) * DB_FONT_SIZE_W );
	}
}

void CDebug::PushCall( CALL_BACK Callback )
{
	CMenu* Menu = CDebug::FindMenu( _CurrentGroupId );
	if( Menu )
	{
		CItem* Item = Menu->GetItemList()->GetLastItem();
		if( Item )
		{
			Item->AddAtributeString( DB_ATTRIBUTE_PUSH_CALL );
			Item->SetPushCallback( Callback );
		}
	}
}

void CDebug::ValueCall( CALL_BACK Callback )
{
	CMenu* Menu = CDebug::FindMenu( _CurrentGroupId );
	if( Menu )
	{
		CItem* Item = Menu->GetItemList()->GetLastItem();
		if( Item )
		{
			Item->AddAtributeString( DB_ATTRIBUTE_VALUE_CALL );
			Item->SetValueCallback( Callback );
		}
	}
}

void CDebug::Separator()
{
	CMenu* Menu = CDebug::FindMenu( _CurrentGroupId );
	if( Menu )
	{
		CItem* Item = Menu->GetItemList()->GetLastItem();
		if( Item )Item->SetSeparator(true);
	}
}

void CDebug::ReadOnly()
{
	if(_ReadOnly) _ReadOnly = false;
	else _ReadOnly = true;
}

void CDebug::Draw()
{
	for(int i = 0;i < _MenuNum;i++)
	{
		if( !_Menu[i].IsOpen() )continue;

		_Menu[i].Draw();
	}
}


//*****************************************************************************
//
//		スクリーンショット
//
//*****************************************************************************

static bool SaveBMP( LPSTR filename, LPBYTE lpSurface, int width, int height )
{
    HANDLE hf;                  // file handle 
    BITMAPFILEHEADER hdr;       // bitmap file-header
    BITMAPINFOHEADER pbmih;
    DWORD	dwTmp;

	ZeroMemory( &pbmih, sizeof(BITMAPINFOHEADER) );
	ZeroMemory( &hdr, sizeof(BITMAPFILEHEADER) );

    pbmih.biSize     = sizeof(BITMAPINFOHEADER);
	pbmih.biWidth    = width;
	pbmih.biHeight   = -height;
    pbmih.biPlanes   = 1;
    pbmih.biBitCount = 24;
    pbmih.biCompression  = BI_RGB;
    pbmih.biSizeImage    = 0;
    pbmih.biClrImportant = 0;

    hf = CreateFile( filename, GENERIC_READ | GENERIC_WRITE, (DWORD) 0, (LPSECURITY_ATTRIBUTES) NULL,
                   CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, (HANDLE) NULL);

    hdr.bfType = 0x4d42;        // 0x42 = "B" 0x4d = "M" 
    hdr.bfSize = (DWORD) (sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER) + width*3*height);
    hdr.bfReserved1 = 0;
    hdr.bfReserved2 = 0;
    hdr.bfOffBits = sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER);

    WriteFile(hf, &hdr, sizeof(BITMAPFILEHEADER),(LPDWORD) &dwTmp, (LPOVERLAPPED) NULL);
    WriteFile(hf, &pbmih, sizeof(BITMAPINFOHEADER),(LPDWORD) &dwTmp, (LPOVERLAPPED) NULL);


	WriteFile(hf, lpSurface, width*height*3, (LPDWORD)&dwTmp, (LPOVERLAPPED)NULL );

	CloseHandle( hf);

	return TRUE;
}

//void CDebug::ScreenShot( LPSTR filename )
//{
//	int		x, y;
//	LPBYTE	temp, bits;
//	IDirect3DSurface9 *pDestSurface;
//	D3DLOCKED_RECT	rect;
//	int		width, height;
//
//	int deskTopX = GetSystemMetrics(SM_CXSCREEN);
//    int deskTopY = GetSystemMetrics(SM_CYSCREEN);
//	CDirect3D::GetDevice()->CreateOffscreenPlainSurface(deskTopX,deskTopY,D3DFMT_A8R8G8B8,D3DPOOL_SYSTEMMEM,&pDestSurface, NULL );
//	CDirect3D::GetDevice()->GetFrontBufferData( 0, pDestSurface );
//
//	POINT	p = { 0, 0 };
//	RECT	rc;
//	ClientToScreen( CDirect3D::GetWindow(), &p ); 
//	GetClientRect( CDirect3D::GetWindow(), &rc );
//	width  = rc.right  - rc.left;
//	height = rc.bottom - rc.top;
//
//	pDestSurface->LockRect( &rect, NULL, D3DLOCK_READONLY );
//	bits = new BYTE[width*height*3];
//	for( x=0 ; x<width ; x++ ){
//		for( y=0 ; y<height ; y++ ){
//			temp = (LPBYTE)(rect.pBits) + (y+p.y)*rect.Pitch + (x+p.x)*4;
//			CopyMemory( &bits[ (y*width+x) * 3 ], temp, 3 );
//		}
//	}
//
//	pDestSurface->UnlockRect();
//	pDestSurface->Release();
//
//	SaveBMP( filename, bits, width,height );
//
//	delete[] bits;
//}
//
//void CDebug::ScreenShotEX(LPSTR filename, D3DXIMAGE_FILEFORMAT FileFormat)
//{
//	if(!CDirect3D::GetDevice())return;
//	
//	int w = CDirect3D::GetPP()->BackBufferWidth;
//	int h = CDirect3D::GetPP()->BackBufferHeight;
//	iex2DObject* obj = NEW( iex2DObject( w, h, 1));
//	CDirect3D::GetBackBuffer(obj->_Texture, w, h);
//	D3DXSaveSurfaceToFile(filename, FileFormat, obj->_Surface , NULL, NULL);
//	DEL(obj);
//}

//void CDebug::OutputString( const char* fmt, ... )
//{
//  char buf[256];
//  va_list ap;
//  va_start(ap, fmt);
//  _vsntprintf(buf, 256, fmt, ap);
//  va_end(ap);
//  OutputDebugString(buf);
//}

}

#endif //_DEBUG
