#ifndef _COLLISION_H_
#define _COLLISION_H_


#include	"BaseObject.h"


/* マクロ定義 */
#define COLLISION_FLOOR_BIAS		Vector3( 0, 1 ,0 )			//	床判定の調整値
#define COLLISION_FLOOR_RAY_DIST	50.0f						//	床判定のレイ距離
#define COLLISION_ERROR_DIST		100.0f						//	壁判定エラー値
#define COLLISION_MODEL_SIZE		1.0f						//	コリジョンモデルサイズ


//-----------------------------------
//	当たり判定クラス
//-----------------------------------
class c_Collision
{
private:
	iexMesh*	m_CollisionModel;
public:
	c_Collision();
	~c_Collision();

	void	Load( char* name );

	float FloorHeight( Vector3& pos );
	float Wall( Vector3& pos, Vector3& v );
	Vector3 CheckMove( Vector3& pos, Vector3& move );

	//	位置チェック
	Vector3 CheckPos( BaseOBJ* actorA, BaseOBJ* actorB );
	Vector3 CheckPos( Vector3& PosA, float size, BaseOBJ* actorB );

};

extern	c_Collision* collision;


#endif	//	_COLLISION_H_