#include	"iextreme.h"
#include	"system/system.h"

#include	"Player.h"
#include	"Camera.h"

//**************************************************************************************//
//																						//
//																						//
//																						//
//**************************************************************************************//



//**************************************************************************************//
//																						//
//		コンストラクタ・デストラクタ													//
//																						//
//**************************************************************************************//

c_PLAYER::c_PLAYER(char *name) : BaseOBJ(name)
{
	m_state		  = P_MODE_WAIT;
	m_RunSpeed    = PLAYER_SPEED;
	m_SubStep	  = 0;
	m_bLocusFlagL = LOCUSL_OFF;
	m_bLocusFlagR = LOCUSR_OFF;
	m_Event		  = NOT_EVENT;
	m_bAttack	  = PLAYER_ATTACK_OFF;
	m_AttackType  = NORMAL_STATE;

	scale	 = PLAYER_SIZE_BASIC;
	angle	 = 0.0f;

	//	刀読み込み
	for( int i=0;i<KATANA_MAX;i++ )m_lpKatana[i] = new iexMesh( FILE_PATH_PLAYER_KATANA );

	//	軌跡テクスチャ読み込み
	LocusInitialize( FILE_PATH_PLAYER_LOCUS );

	m_bboard = new iex2DObj( FILE_PATH_PLAYER_GAUGE );
	
	//	ゲージ
	m_gauge = new iex2DObj( FILE_PATH_PLAYER_GAUGE );
	m_SL = PLAYER_ENERGY_MAX;
	m_SLmax = PLAYER_ENERGY_MAX;
	m_SLwidth = m_SL * PLAYER_ENERGY_WIDTH / m_SLmax;

	//	SEセット
	m_sound = new iexSound();
	m_sound->Set( SE_RUN	  , FILE_PATH_SE_RUN );			//	走る音
	m_sound->Set( SE_BLADE1   , FILE_PATH_SE_BLADE1 );		//	攻撃1音
	m_sound->Set( SE_BLADE2	  , FILE_PATH_SE_BLADE2 );		//	攻撃2音
	m_sound->Set( SE_BLADE3	  , FILE_PATH_SE_BLADE3 );		//	攻撃3音
	m_sound->Set( SE_CHARGE	  , FILE_PATH_SE_CHARGE);		//	溜め音
	m_sound->Set( SE_RELEASE  , FILE_PATH_SE_RELEASE);		//	放射音
	eff_manager = new c_EffectManager();	//	エフェクト管理クラス
}

c_PLAYER::c_PLAYER()
{
	obj = NULL;

	m_state		  = 0;
	m_RunSpeed    = 0.0f;
	m_SubStep	  = 0;
	m_bLocusFlagL = false;
	m_AttackType  = NORMAL_STATE;
	m_bLocusFlagR = false;
	m_Event		  = false;
	m_bAttack	  = false;

	scale	 = 0.0f;
	angle	 = 0.0f;

	//	ゲージ
	m_gauge = NULL;
	m_SL = PLAYER_ENERGY_MAX;
	m_SLmax = PLAYER_ENERGY_MAX;
	m_SLwidth = m_SL * PLAYER_ENERGY_WIDTH / m_SLmax;

	m_sound = NULL;
	eff_manager = NULL;
}

c_PLAYER::~c_PLAYER()
{
	DEL( obj );
	DEL( m_bboard );
	DEL( m_gauge );
	DEL( m_lpLocus );
	for( int i=0;i<KATANA_MAX;i++ ){ DEL( m_lpKatana[i] ); }
	DEL( m_sound );
	DEL( eff_manager );
}

//**************************************************************************************//
//																						//
//		更新																			//
//																						//
//**************************************************************************************//
BOOL c_PLAYER::Update()
{
	Move = Vector3(0,0,0);		//	滑り止め

	//*******************************
	//	行動モード
	//*******************************
	switch( m_state ){
		case P_MODE_WAIT:		//	通常モード
			Wait();
			break;
		case P_MODE_BLADE:		//	ブレードモード
			Blade();
			break;
		case P_MODE_NINJUTU:	//	忍術モード
			Ninjutu();
			break;
		case P_MODE_DAMAGE:		//	ダメージモード
			break;
	}	

	//collision->SetHit( GetBonePos( PLAYER_BONE_RIGHTARM, (int)WEAPON_BONE_LENGTH ), HIT_RADIUS_BLADE1 );	//	当たり判定

	//---------------------------------
	//	ダメージ処理
	//---------------------------------
	//int damage = collision->CheckHit( this, 10 );	//	当たれば10のダメージ
	
	//if( damage > 0 ){	//	被ダメージの場合
	//	m_SL -= damage;
	//	m_SLwidth = m_SL * PLAYER_ENERGY_WIDTH / m_SLmax;
	//	SetMotion( P_MOTION_DAMAGE );
	//	ModeChange( MODE_DAMAGE );
	//}

	BaseOBJ::Update();

//	//-----------------------------
//	//	デバッグ用
//	//-----------------------------
//#ifdef _DEBUG
//
//
//#endif

	return TRUE;
}
//**************************************************************************************//
//		イベント中更新																			//
//**************************************************************************************//
BOOL c_PLAYER::EventUpdate()
{
	//-----------------------------
	//	デバッグ用
	//-----------------------------
#ifdef _DEBUG
	if(KEY(KEY_B) == PUSH_KEY)m_Event = !m_Event;	//	イベントを飛ばす
#endif

	if( obj->GetParam( SE_RUN ) == PLAYER_PRM_TRUE ){					//	パラメータが足の着地時なら
		m_sound->Play( SE_RUN );										//	走りSE
		obj->SetParam( SE_RUN , PLAYER_PRM_FALSE );						//	パラメータを元に戻す
	}

	BaseOBJ::Update();

	return TRUE;
}

void c_PLAYER::Stop()
{
	Move = Vector3(0,0,0);		//	滑り止め
	BaseOBJ::Update();
}

//**************************************************************************************//
//																						//
//		描画																			//
//																						//
//**************************************************************************************//

//--------------------------------------------
//	通常描画
//--------------------------------------------
void c_PLAYER::Render()
{

	obj->Render( shader, "copy_fx2" );										//	プレイヤー描画
	//	行列変換＆武器セット
	WeaponSetToMesh( m_lpKatana[KATANA_RIGHT], PLAYER_BONE_RIGHTARM );
	WeaponSetToMesh( m_lpKatana[KATANA_LEFT], PLAYER_BONE_LEFTARM );
	for( int i=0;i<KATANA_MAX;i++ )m_lpKatana[i]->Render( shader, "copy" );	//	刀描画

	LocusPassL();							//	ブレード軌跡の頂点送り
	LocusPassR();							//	ブレード軌跡の頂点送り
	RenderLocus();							//	ブレード軌跡の描画






	//Vector3 p = Get2DCoord( Pos );	//	２Ｄ座標変換
	//if( p.z > 0.0f )m_gauge->Render( (int)p.x-60,(int)p.y,200,128,0,64,150,64,shader2D,"Particle" );	//	ゲージ描画

	//m_gauge->Render( GAUGE_FLAME_X,GAUGE_FLAME_Y,GAUGE_FLAME_WIDTH,GAUGE_FLAME_HEIGHT,0,0,GAUGE_FLAME_WIDTH,GAUGE_FLAME_HEIGHT,shader2D,"copy" );		//	SLゲージ枠描画
	//m_gauge->Render( GAUGE_X,GAUGE_Y,m_SLwidth,GAUGE_HEIGHT,0,GAUGE_READY,PLAYER_ENERGY_WIDTH,GAUGE_HEIGHT,shader2D,"copy" );							//	SLゲージ描画
}

void c_PLAYER::Render( char *tech )
{
	obj->Render( shader, tech );		//	プレイヤー描画
}

//--------------------------------------------
//	イベント描画
//--------------------------------------------
void c_PLAYER::EventRender( char* name )
{

	obj->Render( shader, name );		//	プレイヤー描画
}

//**************************************************************************************//
//																						//
//		処理																			//
//																						//
//**************************************************************************************//

//--------------------------------------------------------------------------------
//	引数: 新しく指定するモード
//	機能: プレイヤー行動モードの切り替えを行う
//--------------------------------------------------------------------------------
void c_PLAYER::ChangeMode( int mode )
{
	SetAttackType( NORMAL_STATE );	//	攻撃タイプを攻撃なしに設定
	m_SubStep = 0;
	if( m_state != mode ){
		m_state = mode;
	}
}

//--------------------------------------------------------------------------------
//	引数: なし
//	機能: プレイヤーを待機状態にする
//--------------------------------------------------------------------------------
void c_PLAYER::Wait()
{	
	//	初期設定
	Vector3 move(0,0,0);
	Vector3 front(matView._13,0,matView._33);
	Vector3 right(matView._11,0,matView._31);

	//	正規化
	front.Normalize();
	right.Normalize();

	//	軸の検出
	float axisX =  KEY_GetAxisX() * INPUT_BIAS;
	float axisY =  -KEY_GetAxisY() * INPUT_BIAS;
	
	Move = ( front * axisY + right * axisX ) * m_RunSpeed;

	if( Move.x != 0 || Move.z != 0 ){	//	入力あり
		float vx = Move.x;
		float vz = Move.z;
		float vx2 = sinf(angle);
		float vz2 = cosf(angle);

		//	内積による補正量
		float d = sqrtf(vx*vx + vz*vz);
		vx /= d;
		vz /= d;
		float	n = (vx*vx2 + vz*vz2);
		n = DOT_BIAS;

		//	外積による方向補正
		float cross = vx * vz2 - vz * vx2;
		if( cross < 0 )angle -= n;
		else angle += n;
		SetMotion( P_MOTION_RUN );											//	走りモーション

		if( obj->GetParam( PLAYER_PRM_RUN ) == PLAYER_PRM_TRUE ){			//	パラメータが足の着地時なら
			m_sound->Play( SE_RUN );										//	走りSE
			obj->SetParam( PLAYER_PRM_RUN , PLAYER_PRM_FALSE );				//	パラメータを元に戻す
		}

	}else{																	//	入力なし
		SetMotion( P_MOTION_WAIT );											//	待機モーション
		obj->MotionInterpolation( P_MOTION_LINEAR, P_INTERPOLATE_WAIT );	//	待機モーションへ補間
		obj->UpdateSkinMeshFrame( (float)P_INTERPOLATE_WAIT );				//	フレームを更新
	}

	//	状態切り替え
	if( KEY( KEY_A ) == PUSH_KEY ){					//	Ｚキー押
			m_sound->Play( SE_BLADE1 );				//	攻撃1SE
			SetMotion( P_MOTION_BLADE1 );			//	ブレード１モーション
			ChangeMode( P_MODE_BLADE );				//	ブレードモードへ
	}
	//if( KEY( KEY_B ) == PUSH_KEY ){					//	Ｚキー押
	//		m_sound->Play( SE_CHARGE );				//	溜めSE
	//		SetMotion( P_MOTION_CHARGE );			//	溜めモーション
	//		ChangeMode( P_MODE_NINJUTU );			//	忍術モードへ
	//}

}

//--------------------------------------------------------------------------------
//	引数: なし
//	機能: 刀で攻撃する(最大2連コンボ)
//--------------------------------------------------------------------------------
void c_PLAYER::Blade()
{	
	SetAttackType( BLADE );	//	攻撃タイプをブレードに設定

	switch( m_SubStep ){
		case STEP_BLADE1: 	//	１撃目

			//	攻撃判定フラグ
			if( PLAYER_HIT_PRM )SetAttackFlag( PLAYER_ATTACK_ON );
			else SetAttackFlag( PLAYER_ATTACK_OFF );

			if( obj->GetFrame() >= BLADE1_M_START && obj->GetFrame() <= BLADE1_M_END ){	//	振っている間
				m_bLocusFlagR = true;			//	右手軌跡を表示
				Advance( SPEED_BLADE1 );		//	ちょっと前進
			}

			//	次ステップ処理
			if( obj->GetFrame() >= BLADE1_M_END && KEY( KEY_A ) == PUSH_KEY ){			//	Aキー押かつ振り終わったら
				m_sound->Play( SE_BLADE2 );												//	攻撃2SE
				m_bLocusFlagR = false;													//	右手軌跡を非表示に
				Move.y = BLADE2_JUMP_HEIGHT;											//	ジャンプ
				m_SubStep++;															//	２撃目へシフト
			}
			
			//	終了フレームで通常モードへ戻る
			if( obj->GetFrame() >= BLADE1_END ){
				m_bLocusFlagR = false;			//	右手軌跡を非表示に
				ChangeMode( P_MODE_WAIT );		//	待機モードへ
			}

			break;
		case STEP_BLADE2:	//	２撃目（空中）
			
			SetMotion( P_MOTION_BLADE2 );													//	ブレード２モーション

			//	攻撃判定フラグ
			if( PLAYER_HIT_PRM )SetAttackFlag( PLAYER_ATTACK_ON );
			else SetAttackFlag( PLAYER_ATTACK_OFF );

			if( obj->GetFrame() <= BLADE2_JUMP )angle += EVERYDIRECTION / BLADE2_JUMP_TIME;	//	ジャンプ中は回転

			if( obj->GetFrame() >= BLADE2_M_START && obj->GetFrame() < BLADE2_M_END ){		//	振っている間
				m_bLocusFlagL = true;			//	左手軌跡を表示
			}

			//	次ステップ処理(自動)
			if( obj->GetFrame() >= BLADE2_M_END )	//	振り切ったら次へ
			{
				m_bLocusFlagL = false;				//	左手軌跡を非表示に
				m_bLocusFlagR = false;				//	右手軌跡を非表示に
				m_SubStep++;						//	３撃目へシフト
			}

			//	終了フレームで通常モードへ戻る
			if( obj->GetFrame() >= BLADE2_END ){
				m_bLocusFlagL = false;				//	左手軌跡を非表示に
				ChangeMode( P_MODE_WAIT );			//	待機モードへ
			}	

			break;

		case STEP_BLADE3:	//	３撃目
			SetMotion( P_MOTION_BLADE3 );		//	ブレード３モーション

			//	攻撃判定フラグ
			if( PLAYER_HIT_PRM )SetAttackFlag( PLAYER_ATTACK_ON );
			else SetAttackFlag( PLAYER_ATTACK_OFF );
			
			if( obj->GetFrame() >= BLADE3_M_START && obj->GetFrame() <= BLADE3_M_END ){	//	振っている間
				m_bLocusFlagL = true;			//	左手軌跡を表示
				m_bLocusFlagR = true;			//	右手軌跡を表示
			}

			if( obj->GetParam( PLAYER_PRM_ATTACK ) == PLAYER_PRM_TRUE ){		//	パラメータが攻撃時なら
				m_sound->Play( SE_BLADE3 );										//	攻撃3SE
				obj->SetParam( PLAYER_PRM_ATTACK , PLAYER_PRM_FALSE );			//	パラメータを元に戻す
			}
			

			//	終了フレームで通常モードへ戻る
			if( obj->GetFrame() >= BLADE3_END ){
				m_bLocusFlagL = false;			//	左手軌跡を非表示に
				m_bLocusFlagR = false;			//	右手手軌跡を非表示に
				ChangeMode( P_MODE_WAIT );		//	待機モードへ				
			}
			break;		
	}
}

//--------------------------------------------------------------------------------
//	引数: 
//	機能: 忍術を発動させる
//--------------------------------------------------------------------------------
void c_PLAYER::Ninjutu()
{
	//SetAttackType( NINJUTU );	//	攻撃タイプを忍術に設定

	//static int counter = 0;


	//switch( m_SubStep )
	//{
	//	case 0: 	//	溜め

	//		counter++;

	//		eff_manager->NinjutuFire( &GetBonePos( 12 ) );

	//		//	次ステップ処理
	//		if( counter >= 60 * 2 ){													//	溜め終わったら
	//			counter = 0;
	//			SetMotion( P_MOTION_THROW );											//	投射モーション
	//			m_sound->Play( SE_RELEASE );											//	投射SE
	//			m_SubStep++;															//	投射へシフト
	//		}

	//		break;
	//	case 1:		//	投射
	//		
	//		//	攻撃判定フラグ
	//		if( PLAYER_HIT_PRM )SetAttackFlag( PLAYER_ATTACK_ON );
	//		else SetAttackFlag( PLAYER_ATTACK_OFF );

	//		//	終了フレームで通常モードへ戻る
	//		if( obj->GetFrame() >= 360 ){
	//			ChangeMode( P_MODE_WAIT );			//	待機モードへ
	//		}

	//		break;
	//}
}

//--------------------------------------------------------------------------------
//	引数: 移動量
//	機能: プレイヤーを移動量分前進させる
//--------------------------------------------------------------------------------
void c_PLAYER::Advance( float speed )
{
	Move.x = sinf( angle ) * speed;
	Move.z = cosf( angle ) * speed;
}

//--------------------------------------------------------------------------------
//	引数: なし
//	機能: 武器をセットする
//--------------------------------------------------------------------------------
void c_PLAYER::WeaponSet(iex3DObj* lpWeapon,int BoneNum)
{
	//	変換行列 〜TransMatrix〜
	D3DXMATRIX	mat	= *obj->GetBone( BoneNum ) * obj->TransMatrix;

	CopyMemory(&lpWeapon->TransMatrix , &mat, sizeof(mat) );
	D3DXMatrixIdentity(&mat);
	D3DXMatrixScaling( &mat,WEAPON_SETSIZE,WEAPON_SETSIZE,WEAPON_SETSIZE );

	D3DXMatrixMultiply(&lpWeapon->TransMatrix,&mat,&lpWeapon->TransMatrix);
}

void c_PLAYER::WeaponSetToMesh(iexMesh* lpWeapon,int BoneNum)
{
	//	変換行列 〜TransMatrix〜
	D3DXMATRIX	mat;
	mat	= *obj->GetBone( BoneNum ) * obj->TransMatrix;

	CopyMemory(&lpWeapon->TransMatrix , &mat, sizeof(mat) );
	D3DXMatrixIdentity(&mat);
	D3DXMatrixScaling( &mat,WEAPON_SETSIZE,WEAPON_SETSIZE,WEAPON_SETSIZE );

	D3DXMatrixMultiply(&lpWeapon->TransMatrix,&mat,&lpWeapon->TransMatrix);
}

//--------------------------------------------------------------------------------
//	引数: ボーン番号、(ボーンの長さ)
//	機能: 武器の先の座標を取得する
//--------------------------------------------------------------------------------
Vector3 c_PLAYER::GetBonePos(int n, int d)
{
	Matrix mat =  *obj->GetBone(n) * obj->TransMatrix;

	Vector3 p;
	p.x = mat._41 + mat._31 * d;
	p.y = mat._42 + mat._32 * d;
	p.z = mat._43 + mat._33 * d;

	return p;
}

Vector3 c_PLAYER::GetBonePos(int n)
{
	Matrix mat =  *obj->GetBone(n) * obj->TransMatrix;

	Vector3 p;
	p.x = mat._41;
	p.y = mat._42;
	p.z = mat._43;

	return p;
}

//**************************************************************************************//
//																						//
//		剣の軌跡																		//
//																						//
//**************************************************************************************//
//--------------------------------------------------------------------------------
//	引数:ソースファイル名
//	機能: 軌跡の初期化
//--------------------------------------------------------------------------------
void c_PLAYER::LocusInitialize( char* filename )
{
	//	今の頂点
	for( int i=0;i<LOCUS_VERTEX_MAX;i++ ){
		m_locus_vertexL[i].x = 0.0f;
		m_locus_vertexL[i].y = 0.0f;
		m_locus_vertexL[i].z = 0.0f;
		m_locus_vertexL[i].color = COLOR_F_WHITE;

		m_locus_vertexR[i].x = 0.0f;
		m_locus_vertexR[i].y = 0.0f;
		m_locus_vertexR[i].z = 0.0f;
		m_locus_vertexR[i].color = COLOR_F_WHITE;
	}

	//	定数
	const float uv_max = 1.0f;	//	UV座標(0.0〜1.0)の最大値
	const float uv_min = 0.0f;	//	UV座標(0.0〜1.0)の最小値

	//	UV設定(左手)
	m_locus_vertexL[LOCUS_VERTEX1].tu = uv_min;
	m_locus_vertexL[LOCUS_VERTEX1].tv = uv_min;
	m_locus_vertexL[LOCUS_VERTEX2].tv = uv_max;
	m_locus_vertexL[LOCUS_VERTEX2].tu = uv_min;
	m_locus_vertexL[LOCUS_VERTEX3].tu = uv_max;
	m_locus_vertexL[LOCUS_VERTEX3].tv = uv_max;
	m_locus_vertexL[LOCUS_VERTEX4].tu = uv_max;
	m_locus_vertexL[LOCUS_VERTEX4].tv = uv_min;

	//	UV設定(右手)
	m_locus_vertexR[LOCUS_VERTEX1].tu = uv_min;
	m_locus_vertexR[LOCUS_VERTEX1].tv = uv_min;
	m_locus_vertexR[LOCUS_VERTEX2].tu = uv_min;
	m_locus_vertexR[LOCUS_VERTEX2].tv = uv_max;
	m_locus_vertexR[LOCUS_VERTEX3].tu = uv_max;
	m_locus_vertexR[LOCUS_VERTEX3].tv = uv_max;
	m_locus_vertexR[LOCUS_VERTEX4].tu = uv_max;	
	m_locus_vertexR[LOCUS_VERTEX4].tv = uv_min;
	
	//	軌跡用テクスチャ読み込み
	m_lpLocus = new iex2DObj( filename );
	
}

//--------------------------------------------------------------------------------
//	引数: 
//	機能: 左手軌跡の更新
//--------------------------------------------------------------------------------
void c_PLAYER::LocusPassL()
{
	/*	--	フレーム分割処理	--	*/ 
	{
		/*剣の軌跡のクオリティー*/ 
		const int LucusQuality = 8;					//	分割数
		const float	adjust = 1.0f / LucusQuality;	//	調整値

		//変形情報を分割
		Vector3	pos_adj = (m_pos_old-Pos ) / (float)LucusQuality;
		float angle_abj = (m_angle_old-angle) / (float)LucusQuality;

		//変換行列
		Matrix sMat,oMat;

		//拡大行列を準備
		oMat = obj->TransMatrix;
		D3DXMatrixScaling( &sMat, obj->GetScale().x, obj->GetScale().y, obj->GetScale().z );
 
		//分割ループ
		for( int i=LucusQuality-1 ; i>=0 ; i-- )
		{
			//分割情報算出
			Vector3 p = Pos + pos_adj * (float)i;
			float an = angle + angle_abj * (float)i;

			//変換行列作成
			SetTransformMatrixXYZ(&obj->TransMatrix,p.x, p.y, p.z, 0, an, 0);
			D3DXMatrixMultiply( &obj->TransMatrix, &sMat, &obj->TransMatrix );

			//アニメーション情報を分割
			obj->UpdateSkinMeshFrame( obj->GetFrame()-adjust*i );
			obj->UpdateBoneMatrix();

			//	剣の根元の頂点
			m_locus_vertexL[LOCUS_VERTEX1].x = GetBonePos(PLAYER_BONE_LEFTARM).x;
			m_locus_vertexL[LOCUS_VERTEX1].y = GetBonePos(PLAYER_BONE_LEFTARM).y;
			m_locus_vertexL[LOCUS_VERTEX1].z = GetBonePos(PLAYER_BONE_LEFTARM).z;
			m_locus_vertexL[LOCUS_VERTEX1].color = COLOR_F_WHITE;
			//	剣の先の頂点
			m_locus_vertexL[LOCUS_VERTEX2].x = GetBonePos(PLAYER_BONE_LEFTARM,(int)LOCUS_LENGTH).x;
			m_locus_vertexL[LOCUS_VERTEX2].y = GetBonePos(PLAYER_BONE_LEFTARM,(int)LOCUS_LENGTH).y;
			m_locus_vertexL[LOCUS_VERTEX2].z = GetBonePos(PLAYER_BONE_LEFTARM,(int)LOCUS_LENGTH).z;
			m_locus_vertexL[LOCUS_VERTEX2].color = COLOR_F_WHITE;

			//	頂点送り
			for(int i=LOCUS_MAX-1;i>=0;i--){
				CopyMemory( &m_locus_vertexL[i*2+3],&m_locus_vertexL[i*2+1],sizeof(LVERTEX) );
				CopyMemory( &m_locus_vertexL[i*2+2],&m_locus_vertexL[i*2],sizeof(LVERTEX) );
			}
		}
		obj->TransMatrix = oMat;
	}

	obj->Update();
	/*過去位置用に保存*/ 
	m_pos_old = Pos;
	m_angle_old = angle;
}

//--------------------------------------------------------------------------------
//	引数: 
//	機能: 右手軌跡の更新
//--------------------------------------------------------------------------------
void c_PLAYER::LocusPassR()
{
	/*	--	フレーム分割処理	--	*/ 
	{
		/*剣の軌跡のクオリティー*/ 
		const int LucusQuality = 8;					//	分割数
		const float	adjust = 1.0f / LucusQuality;	//	調整値

		//変形情報を分割
		Vector3	pos_adj = (m_pos_old-Pos ) / (float)LucusQuality;
		float angle_abj = (m_angle_old-angle) / (float)LucusQuality;

		//変換行列
		Matrix sMat,oMat;

		//拡大行列を準備
		oMat = obj->TransMatrix;
		D3DXMatrixScaling( &sMat, obj->GetScale().x, obj->GetScale().y, obj->GetScale().z );
 
		//分割ループ
		for( int i=LucusQuality-1 ; i>=0 ; i-- )
		{
			//分割情報算出
			Vector3 p = Pos + pos_adj * (float)i;
			float an = angle + angle_abj * (float)i;

			//変換行列作成
			SetTransformMatrixXYZ(&obj->TransMatrix,p.x, p.y, p.z, 0, an, 0);
			D3DXMatrixMultiply( &obj->TransMatrix, &sMat, &obj->TransMatrix );

			//アニメーション情報を分割
			obj->UpdateSkinMeshFrame( obj->GetFrame()-adjust*i );
			obj->UpdateBoneMatrix();

			//	剣の根元の頂点
			m_locus_vertexR[LOCUS_VERTEX1].x = GetBonePos(PLAYER_BONE_RIGHTARM).x;
			m_locus_vertexR[LOCUS_VERTEX1].y = GetBonePos(PLAYER_BONE_RIGHTARM).y;
			m_locus_vertexR[LOCUS_VERTEX1].z = GetBonePos(PLAYER_BONE_RIGHTARM).z;
			m_locus_vertexR[LOCUS_VERTEX1].color = COLOR_F_WHITE;
			//	剣の先の頂点
			m_locus_vertexR[LOCUS_VERTEX2].x = GetBonePos(PLAYER_BONE_RIGHTARM,(int)LOCUS_LENGTH).x;
			m_locus_vertexR[LOCUS_VERTEX2].y = GetBonePos(PLAYER_BONE_RIGHTARM,(int)LOCUS_LENGTH).y;
			m_locus_vertexR[LOCUS_VERTEX2].z = GetBonePos(PLAYER_BONE_RIGHTARM,(int)LOCUS_LENGTH).z;
			m_locus_vertexR[LOCUS_VERTEX2].color = COLOR_F_WHITE;

			//	頂点送り
			for(int i=LOCUS_MAX-1;i>=0;i--){
				CopyMemory( &m_locus_vertexR[i*2+3],&m_locus_vertexR[i*2+1],sizeof(LVERTEX) );
				CopyMemory( &m_locus_vertexR[i*2+2],&m_locus_vertexR[i*2],sizeof(LVERTEX) );
			}
		}
		obj->TransMatrix = oMat;
	}

	obj->Update();
	/*過去位置用に保存*/ 
	m_pos_old = Pos;
	m_angle_old = angle;
}

//--------------------------------------------------------------------------------
//	引数: なし
//	機能: 軌跡を描画する
//--------------------------------------------------------------------------------
void c_PLAYER::RenderLocus()
{
	/*  左手  */
	if( m_bLocusFlagL ){
		//	剣の軌跡	
		iexSystem::Device->SetRenderState( D3DRS_CULLMODE,D3DCULL_NONE );		//	両面描画
		iexPolygon::Render3D( m_locus_vertexL,LOCUS_MAX*2,m_lpLocus,RS_ADD );	//	ポリゴン描画
		iexSystem::Device->SetRenderState( D3DRS_CULLMODE,D3DCULL_NONE );		//	元に戻す
	}
	/*  右手  */
	if( m_bLocusFlagR ){
		//	剣の軌跡	
		iexSystem::Device->SetRenderState( D3DRS_CULLMODE,D3DCULL_NONE );		//	両面描画
		iexPolygon::Render3D( m_locus_vertexR,LOCUS_MAX*2,m_lpLocus,RS_ADD );	//	ポリゴン描画
		iexSystem::Device->SetRenderState( D3DRS_CULLMODE,D3DCULL_NONE );		//	元に戻す
	}
}

//--------------------------------------------------------------------------------
//	引数: 敵管理クラスポインタ
//	機能: 当たり判定
//--------------------------------------------------------------------------------
void c_PLAYER::HitCheck( EnemyManager* enemy )
{
	//	敵との当たり判定
	if( m_bAttack ){
		if( m_AttackType == BLADE ){
			enemy->CrashCheck( &GetBonePos( PLAYER_BONE_RIGHTARM, (int)WEAPON_BONE_LENGTH ), HIT_BALDE_POWER );
			enemy->CrashCheck( &GetBonePos( PLAYER_BONE_LEFTARM, (int)WEAPON_BONE_LENGTH ), HIT_BALDE_POWER );
			SetAttackFlag( PLAYER_ATTACK_OFF );		//	攻撃判定リセット
		}
		if( m_AttackType == NINJUTU ){
			//for( int i=0;i<KUNAI_MAX;i++ )enemy->CrashCheck( &m_KunaiPos[i] , HIT_NINJUTU_POWER );
			SetAttackFlag( PLAYER_ATTACK_OFF );		//	攻撃判定リセット
		}
	}
}

//--------------------------------------------------------------------------------
//	引数: 歩く時間
//	機能: イベント中歩かせる
//--------------------------------------------------------------------------------
void c_PLAYER::EventWalk()
{
	SetMotion( P_MOTION_RUN );

	Move.x = sinf( angle ) * PLAYER_SPEED;
	Move.z = cosf( angle ) * PLAYER_SPEED;
}