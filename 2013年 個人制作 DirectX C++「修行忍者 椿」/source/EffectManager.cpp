#include	"iextreme.h"
#include	"system/system.h"

#include	"EffectManager.h"


//**************************************************************************************//
//																						//
//																						//
//																						//
//**************************************************************************************//



//**************************************************************************************//
//																						//
//		エフェクト基本クラス															//
//																						//
//**************************************************************************************//

//**************************************************************************************//
//																						//
//		コンストラクタ・デストラクタ													//
//																						//
//**************************************************************************************//
c_EffAppear::c_EffAppear()
{
	m_lpEffect = NULL;

	Pos = Vector3(0,0,0); 
	Angle = Vector3(0,0,0);
	Scale = Vector3(0,0,0);
	tu = tv = 0.0f;
	alpha = 0.0f;
	timer = 0;
	bRender = false;
}

c_EffAppear::c_EffAppear( char* filename )
{
	//	モデル読み込み
	m_lpEffect = new iex3DObj( filename );

	Pos = Vector3(0,0,0); 
	Angle = Vector3(0,0,0);
	Scale = EFF_SIZE_BASIC;

	tu = tv = 0.0f;
	alpha = EFF_ALPHA_BASIC;
	timer = EFF_APPEAR_TIME;
	bRender = true;	//	最初は表示
}

c_EffAppear::~c_EffAppear()
{
	DEL( m_lpEffect );
}

//**************************************************************************************//
//																						//
//		更新																			//
//																						//
//**************************************************************************************//
void c_EffAppear::Update()
{
	//	時間による消滅処理
	timer--;
	if( timer < 0 ){
		timer = 0;
		bRender = false;
	}

	if( bRender ){												//	描画している間
		//	UVアニメーション
		tv += EFF_APPEAR_ANIME;
		if( tv >= EFF_ANIMATION_MAX )tv -= EFF_ANIMATION_MAX;	//	リセット

		m_lpEffect->Update();
		m_lpEffect->SetScale( Scale );
		m_lpEffect->SetAngle( Angle );
		m_lpEffect->SetPos( Pos );
		m_lpEffect->Animation();
	}
}

//**************************************************************************************//
//																						//
//		描画																			//
//																						//
//**************************************************************************************//
void c_EffAppear::Render()
{
	if( bRender ){
		shader->SetValue("SU", 0.0f );
		shader->SetValue("SV", tv );
		shader->SetValue("UValpha", alpha );

		m_lpEffect->Render( shader,"add_uv" );
	}
}

//**************************************************************************************//
//																						//
//		エフェクト管理クラス															//
//																						//
//**************************************************************************************//

//**************************************************************************************//
//																						//
//		コンストラクタ・デストラクタ													//
//																						//
//**************************************************************************************//
c_EffectManager::c_EffectManager()
{
	for(int i=0;i<EFF_UV_MAX;i++){
		appear[i] = NULL;
	}
}

c_EffectManager::~c_EffectManager()
{
	for(int i=0;i<EFF_UV_MAX;i++){
		DEL( appear[i] );
	}
}

//**************************************************************************************//
//																						//
//		更新																			//
//																						//
//**************************************************************************************//
void c_EffectManager::Update()
{
	for(int i=0;i<EFF_UV_MAX;i++){
		//	更新と生存確認
		if( appear[i] == NULL )continue;
		appear[i]->Update();
	}
}

//**************************************************************************************//
//																						//
//		描画																			//
//																						//
//**************************************************************************************//
void c_EffectManager::Render()
{
	for(int i=0;i<EFF_UV_MAX;i++){
		//	更新と生存確認
		if( appear[i] == NULL )continue;
		appear[i]->Render();
	}
}

//**************************************************************************************//
//																						//
//		処理																			//
//																						//
//**************************************************************************************//

//--------------------------------------------------------------------------------
//	引数: 種類、ループ数、出現位置、向く方向、大きさ
//	機能: 指定した種類の敵を出現させる
//--------------------------------------------------------------------------------
void c_EffectManager::uvAppear( Vector3 pos, Vector3 angle,float scale)
{
	//	空き検索
	int index = -1;

	//	空き検索
	for(int i=0;i<EFF_UV_MAX;i++){
		if( appear[i] != NULL )continue;
		index = i;
		break;
	}

	//	空き番号のエフェクトを出現
	appear[index] = new c_EffAppear( FILE_PATH_EFFUV_APPEAR );
	appear[index]->SetScale( Vector3(scale,scale,scale) );
	appear[index]->SetPos( pos );
	appear[index]->SetAngle( angle );
}

//******************************************************************************//
//																				//
//		パーティクル															//
//																				//
//******************************************************************************//

//--------------------------------------------------------------------------------
//	引数: 出現させる座標
//	機能: 爆発エフェクトの赤成分
//--------------------------------------------------------------------------------
void c_EffectManager::ExplosionRed( Vector3* pos )
{
	for( int i=0 ; i<EFF_PARTICLE_LOOP_MAX ; i++ )
	{
		Vector3 pos2 = *pos;

		Vector3 move;
		move.x =  (rand()%20-10)*0.002f;
		move.y =  (rand()%50-25)*0.002f + 0.15f;
		move.z =  (rand()%20-10)*0.002f;

		PARTICLE	p;
		p.type = 7;
		p.aFrame = 0;
		p.aColor = 0xFF886611;
		p.mFrame = 20;
		p.mColor = 0xFF886611;
		p.eFrame = 60;
		p.eColor = 0xFF886611;

		p.angle = .0f;
		p.rotate = (rand()%200-100) * 0.0001f;
		p.scale = 0.2f;
		p.stretch = 1.05f;

		p.flag = RS_ADD;

		p.Pos = pos2;
		p.Move = move;
		
		p.Power.x = 0.0f;
		p.Power.z = 0.0f;
		p.Power.y = -0.005f;


		iexParticle::Set(&p);
	}

}

//--------------------------------------------------------------------------------
//	引数: 出現させる座標
//	機能: 爆発エフェクトの黒成分
//--------------------------------------------------------------------------------
void c_EffectManager::ExplosionBlack( Vector3* pos )
{
	for( int i=0 ; i<EFF_PARTICLE_LOOP_MAX ; i++ )
	{
		Vector3 pos2 = *pos;
		Vector3 move;

		move.x =  (rand()%20-10)*0.004f;
		move.y =  (rand()%50-25)*0.004f + 0.15f;
		move.z =  (rand()%20-10)*0.004f;

		PARTICLE	p;
		p.type = 5;
		p.aFrame = 0;
		p.aColor = 0xFF888888;
		p.mFrame = 20;
		p.mColor = 0xFF888888;
		p.eFrame = 60;
		p.eColor = 0xFF888888;

		p.angle = .0f;
		p.rotate = (rand()%200-100) * 0.0001f;
		p.scale = 0.2f;
		p.stretch = 1.05f;

		p.flag = RS_SUB;

		p.Pos = pos2;
		p.Move = move;
		
		p.Power.x = 0.0f;
		p.Power.z = 0.0f;
		p.Power.y = -0.01f;


		iexParticle::Set(&p);
	}

}


//--------------------------------------------------------------------------------
//	引数: 出現させる座標
//	機能: 忍術エフェクト
//--------------------------------------------------------------------------------
void c_EffectManager::NinjutuFire( Vector3 *pos )
{
	PARTICLE	p;

	Vector3 pos2 = *pos;
	Vector3 move;

	for( int i=0 ; i<10 ; i++ )
	{

		//	移動量
		move.x =   0.0f;
		move.y =   0.01f;
		move.z =   0.0f;

		//	出現位置
		pos2.x = (rand()%20-10)*0.004f;
		pos2.y = (rand()%20-10)*0.004f;
		pos2.z = (rand()%20-10)*0.004f;

		
		p.type = 7;			//	パーティクルの種類

		//	開始フレーム
		p.aFrame = 0;
		p.aColor = 0xFFFF8800;
		//	中間フレーム
		p.mFrame = 30;
		p.mColor = 0x88FF0000;
		//	終了フレーム
		p.eFrame = 60;
		p.eColor = 0x00FF00FF;

		p.angle = 0.0f;
		p.rotate = 0.0f;
		p.scale = 0.2f;
		p.stretch = 0.98f;	//	1.05初期値

		p.flag = RS_ADD;

		p.Pos  = pos2;		//	出現座標
		p.Move = move;		//	移動量
		
		//	パワー
		p.Power.x = 0.0f;
		p.Power.z = 0.0f;
		p.Power.y = 0.0f;


		iexParticle::Set(&p);

	}
}