#ifndef __NOWLOAD_H__
#define __NOWLOAD_H__

#include "ThreadObject.h"

class cNowLoad : public cThreadObject
{
private:
	iex2DObj*	_obj;
public:
	cNowLoad();
	~cNowLoad();

	void Exec();
	void Render();
};

#endif // __NOWLOAD_H__