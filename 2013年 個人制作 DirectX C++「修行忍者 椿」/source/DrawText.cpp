#include	"iextreme.h"
#include	"system/system.h"
#include	"Text.h"

//**************************************************************************************//
//																						//
//																						//
//																						//
//**************************************************************************************//

//*****************************************************************************************************************************
//
//	フォント関連
//
//*****************************************************************************************************************************
c_String::c_String()
{
	m_lpFont = NULL;
	m_FontSize = NULL;
	m_FontNum = NULL;
	m_FontTable = NULL;
}

c_String::~c_String()
{
	DEL( m_lpFont );
	DEL( m_FontTable );

}

void c_String::Init(char *name)
{
	char	filename[256];

	//	文字テーブルファイル
	wsprintf( filename, "%s_ch.txt", name );
	FILE* fp = fopen(filename, "rt" );
	fscanf( fp, "%d", &m_FontSize);		//	文字サイズ
	fscanf( fp, "%d", &m_FontNum);		//	文字数

	//	文字テーブル確保
	m_FontTable = new WORD[m_FontNum+2];

	//	文字テーブル読み込み
	fscanf( fp, "%s",m_FontTable );
	fclose(fp);

	//	フォント画像読み込み
	wsprintf( filename, "%s.tga", name );
	m_lpFont = new iex2DObj( filename );

}


//*****************************************************************************************************************************
//	文字描画
//*****************************************************************************************************************************

void c_String::RenderFont(int x, int y, WORD font, DWORD shadow, DWORD color)
{
	for( int index=0 ; index<m_FontNum; index++ )
	{
		if(m_FontTable[index] == font )
		{
			int l = (512/m_FontSize);
			int sx = (index%l) * m_FontSize;
			int sy = (index/l) * m_FontSize;
			//	影描画
			m_lpFont->Render(x,y,m_FontSize, m_FontSize,
			sx+1, sy+1, m_FontSize, m_FontSize,shader2D,"copy", shadow );
			//	文字描画
			m_lpFont->Render(x,y,m_FontSize, m_FontSize,
			sx, sy, m_FontSize, m_FontSize,shader2D,"copy", color );
			break;
		}
	}
}

//*****************************************************************************************************************************
//	文字列描画
//*****************************************************************************************************************************
void c_String::RenderStr(int x, int y, char *str, DWORD shadow, DWORD color)
{
	DWORD RenderColor = color;	//	元の色
	int dx = x;
	int dy = y;
	for( int i=0 ; i<(int)strlen(str); ){
		switch( str[i] ){
			case '0': RenderColor = COLOR_WHITE; i++; continue; // 白
			case '1': RenderColor = COLOR_RED; i++; continue;	// 赤
			case '2': RenderColor = COLOR_GREEN; i++; continue;	// 緑
			case '3': RenderColor = COLOR_BLUE; i++; continue;	// 青
			case '4': RenderColor = color; i++; continue;		// 元の色
			case '/':	//	改行
				dx = x;
				dy += m_FontSize;
				i++;
				continue;
		}
		RenderFont( dx, dy, *(LPWORD)(&str[i]), shadow ,RenderColor );
		dx += m_FontSize;
		i += 2;
	}
	
}