#include	"iextreme.h"
#include	"system/system.h"
#include	"Enemy.h"
#include	"Collision.h"

//**************************************************************************************//
//																						//
//																						//
//																						//
//**************************************************************************************//



//**************************************************************************************//
//																						//
//		コンストラクタ・デストラクタ													//
//																						//
//**************************************************************************************//

ENEMY::ENEMY( char *filename, int kind ) : BaseOBJ( filename )
{
	obj->SetMotion( ENEMY_MOTION_WAIT );
	m_state		= MODE_WAIT;
	m_SubStep	= 0;
	scale		= 0.01f;
	m_hp		= 0;
	m_kind		= kind;

}

ENEMY::~ENEMY()
{
	DEL( obj );
}

//**************************************************************************************//
//																						//
//		更新																			//
//																						//
//**************************************************************************************//
bool ENEMY::Update( Vector3& ppos )
{
	Move = Vector3(0,0,0);	//	滑り止め


	//	敵死亡処理
	if( m_hp <= 0 ){
		bAlive = false;
	}

	//	行動モード
	switch( m_state ){
		case MODE_WAIT:
			Wait(ppos);
			break;
		case MODE_ATTACK:
			Attack(ppos);
			break;
		case MODE_DAMAGE:
			Damage();
			break;
	}

	Pos = collision->CheckPos( ppos,1.0f,this );	//	プレイヤーとの当たり判定

	BaseOBJ::Update();



	return true;
}

//**************************************************************************************//
//																						//
//		描画																			//
//																						//
//**************************************************************************************//


//**************************************************************************************//
//																						//
//		処理																			//
//																						//
//**************************************************************************************//

//--------------------------------------------------------------------------------
//	引数: モード番号
//	機能: 敵の行動を切り替える
//--------------------------------------------------------------------------------
void ENEMY::ModeChange(int mode)
{
	m_SubStep = 0;
	if( m_state != mode ){
		m_state = mode;
	}
}

//--------------------------------------------------------------------------------
//	引数: なし
//	機能: 待機またはプレイヤーに接近
//--------------------------------------------------------------------------------
void ENEMY::Wait( Vector3& ppos )
{
	//	プレイヤーとのキョリ
	float dx = ppos.x - Pos.x;
	float dz = ppos.z - Pos.z;
	float d = sqrtf( dx*dx + dz*dz );

	if( d < 2.0f ){
		SetMotion( ENEMY_MOTION_WAIT );
		Move = Vector3( 0,0,0 );
	}
	else{
		SetMotion( ENEMY_MOTION_WALK );

		Vector3 v2(sinf(angle),0,cosf(angle));
		Vector3 v1 = Move;

		//	方向設定(Y軸は無視)
		float dx = ppos.x - Pos.x;
		float dz = ppos.z - Pos.z;
		angle = atan2f( dx,dz );
		
		//	移動量決定
		Move.x = sinf(angle) * 0.1f;	
		Move.z = cosf(angle) * 0.1f;
		
		//	内積
		float d = sqrtf(v1.x*v1.x + v1.z*v1.z);
		v1.x /= d;
		v1.z /= d;
		float	n = (v1.x*v2.x + v1.z*v2.z);
		n = (1-n) * 2.0f;

		//	外積
		float g = v1.x*v2.z - v1.z*v2.x;
		if( g > 0 )angle += 0.1f;
		else angle -= 0.1f;
	}
}

//--------------------------------------------------------------------------------
//	引数: なし
//	機能: プレイヤーに攻撃する
//--------------------------------------------------------------------------------
void ENEMY::Attack( Vector3& ppos )
{
	#define STEP_MOTION	0
	#define STEP_CHARGE	1

	static int Charge_Counter = 120;

	switch( m_SubStep ){
		case STEP_MOTION: float dx,dz;
			SetMotion( ENEMY_MOTION_CHARGE );	//	突進モーション
			//	方向設定(Y軸は無視)
			dx = ppos.x - Pos.x;
			dz = ppos.z - Pos.z;
			angle = atan2f( dx,dz );
			m_SubStep++;
			break;
		case STEP_CHARGE:
			Charge_Counter--;	//	カウントダウン
			//	移動量決定
			Move.x = sinf(angle) * 0.2f;	
			Move.z = cosf(angle) * 0.2f;
			if( Charge_Counter <= 0 ){
				Charge_Counter = 120;
				ModeChange( MODE_WAIT );
			}
			break;
	}
}

//--------------------------------------------------------------------------------
//	引数: なし
//	機能: ダメージ時の処理
//--------------------------------------------------------------------------------
void ENEMY::Damage()
{
	switch( m_SubStep ){
		case 0:
			if( m_kind == ENEMY_03 )SetMotion( ENEMY03_MOTION_DAMAGE );
			m_SubStep++;
			break;
		case 2:
			//	終了フレームで通常モードへ戻る
			if( m_kind == ENEMY_03 ){
				if( obj->GetFrame() >= 651 )ModeChange( MODE_WAIT );
			}
			else ModeChange( MODE_WAIT );
			break;
	}
}

//--------------------------------------------------------------------------------
//	引数: ボーン番号、(ボーンの長さ)
//	機能: ボーン座標を取得する
//--------------------------------------------------------------------------------
Vector3 ENEMY::GetBonePos(int n, int d)
{
	Matrix mat =  *obj->GetBone(n) * obj->TransMatrix;

	Vector3 p;
	p.x = mat._41 + mat._31 * d;
	p.y = mat._42 + mat._32 * d;
	p.z = mat._43 + mat._33 * d;

	return p;
}

Vector3 ENEMY::GetBonePos(int n)
{
	Matrix mat =  *obj->GetBone(n) * obj->TransMatrix;

	Vector3 p;
	p.x = mat._41 + mat._31;
	p.y = mat._42 + mat._32;
	p.z = mat._43 + mat._33;

	return p;
}

//--------------------------------------------------------------------------------
//	引数: プレイヤー座標、敵座標
//	機能: 当たり判定、当たれば１を返す
//--------------------------------------------------------------------------------
int ENEMY::Damage( Vector3* ppos, Vector3* epos )
{
	//	敵当たり判定
	float vx = ppos->x - epos->x;
	float vy = ppos->y - epos->y;
	float vz = ppos->z - epos->z;

	//	距離判定
	float d=sqrt(vx*vx + vy*vy + vz*vz);
	if(d < ENEMY_AWAY_DIST)return ENEMY_HIT;

	return 0;
}

//--------------------------------------------------------------------------------
//	引数: プレイヤー座標
//	機能: 当たり判定
//--------------------------------------------------------------------------------
int ENEMY::CrashCheck( Vector3* ppos, int power )
{	
	int n = Damage( ppos, &Pos );

	if( n == ENEMY_HIT )		//　ヒット
	{
		m_hp -= power;

		if( m_hp <= 0 )			//	死亡
		{
			
			return ENEMY_DEATH;	
		}
		return ENEMY_HIT;
	}
	return 0;
}