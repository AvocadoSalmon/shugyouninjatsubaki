#ifndef _ENEMY_H_
#define _ENEMY_H_

#include "BaseObject.h"
#include "Script.h"
#include "EffectManager.h"

#define	ENEMY_MAX				3				//	敵の最大数
#define	ENEMY_HIT				1				//	敵にヒット
#define	ENEMY_DEATH				-1				//	敵死亡

#define	ENEMY_AWAY_DIST			2.0f			//	敵がプレイヤーから離れる距離
#define	ENEMY_CHARGE_DIST		0.2f			//	敵の突進距離
#define	ENEMY_CHARGE_TIME		120				//	敵の突進時間
//******************//
//	ENEMY01設定		//
//******************//
#define	ENEMY01_SIZE			0.2f			//	敵のサイズ
#define	ENEMY01_HP				5				//	敵のHP
#define	ENEMY01_BODY_RADIUS		1.0f			//	敵の体の当たり半径
#define	ENEMY01_POWER			2				//	攻撃力
//******************//
//	ENEMY02設定		//
//******************//
#define	ENEMY02_SIZE			0.04f			//	敵のサイズ
#define	ENEMY02_HP				20				//	敵のHP
#define	ENEMY02_BODY_RADIUS		1.5f			//	敵の体の当たり半径
#define	ENEMY02_POWER			5				//	攻撃力
//******************//
//	ENEMY03設定		//
//******************//
#define	ENEMY03_SIZE			0.015f			//	敵のサイズ
#define	ENEMY03_HP				20				//	敵のHP
#define	ENEMY03_BODY_RADIUS		1.0f			//	敵の体の当たり半径
#define	ENEMY03_POWER			10				//	攻撃力

//	攻撃ステップ
#define ENENMY_STEP_ADJUST		0
#define ENEMY_STEP_CHARGE		1

//	敵種類
enum{
	ENEMY_01,
	ENEMY_02,
	ENEMY_03,
};

//	敵行動モード
enum{
	MODE_WAIT,
	MODE_ATTACK,
	MODE_DAMAGE,
};

//	敵1モーション
enum{
	ENEMY01_MOTION_WAIT,
};

//	敵2モーション
enum{
	ENEMY_MOTION_LINEAR,
	ENEMY_MOTION_WAIT,
	ENEMY_MOTION_WALK,
	ENEMY_MOTION_CHARGE,
	ENEMY_MOTION_DAMAGE,
};

//	敵3モーション
enum{
	ENEMY03_MOTION_WAIT,
	ENEMY03_MOTION_WALK,
	ENEMY03_MOTION_RUN,
	ENEMY03_MOTION_RIGHT,
	ENEMY03_MOTION_LEFT,
	ENEMY03_MOTION_PUNCH,
	ENEMY03_MOTION_KICK,
	ENEMY03_MOTION_AWAY,
	ENEMY03_MOTION_CHARGE,
	ENEMY03_MOTION_LEFT_STEP,
	ENEMY03_MOTION_RIGHT_STEP,
	ENEMY03_MOTION_DAMAGE,
	ENEMY03_MOTION_DEATH,
};


//-----------------------------------
//	敵クラス
//-----------------------------------
class ENEMY : public BaseOBJ{
private:
	int m_state;
	int m_SubStep;
	int m_hp;
	bool m_Hot;
	int m_kind;
protected:
	
public:
	ENEMY(char *filename, int kind);
	~ENEMY();

	bool Update( Vector3& ppos );
	void ModeChange( int mode );

	void Wait(Vector3& ppos);
	void Attack(Vector3& ppos);
	void Damage();

	int Damage( Vector3* ppos, Vector3* epos );
	int CrashCheck( Vector3* ppos, int power );

	//	セッター
	inline void SetHP( int HP ){ m_hp = HP; }

	//	ゲッター
	int GetHP(){ return m_hp; }
	Vector3 GetBonePos(int n, int d);
	Vector3 GetBonePos(int n);


};

#define		ENEMY_APPEAR_MAX	1			//	最大同時出現数
#define		ENEMY_APPEAR_BIAS1	30.0f		//	出現座標調整値
#define		ENEMY_APPEAR_BIAS2	30.0f		//	出現座標調整値
#define		ENEMY_APPEAR_BIAS3	40.0f		//	出現座標調整値
//	出現ステップ
enum{
	ENEMY_FIRST,
	ENEMY_SECOND,
	ENEMY_THIRD,
};
//	出現処理
enum{
	ENEMY_FIRST_POS		= 14,
	ENEMY_SECOND_POS	= 140,
	ENEMY_THIRD_POS		= 250,	
};

//-----------------------------------
//	敵管理クラス
//-----------------------------------
class EnemyManager{
private:
	ENEMY*	enemy[ENEMY_MAX];
	int		destroy;	//	撃破数
	int		AppearStep;	//	出現用ステップ
	c_EffectManager* eff_manager;
public:
	EnemyManager();
	~EnemyManager();
	

	void Appear(char *filename,int kind,Vector3& pos, float angle,float scale,int hp);	//	敵出現
	void Reset( int no );	//	敵消去

	void Update( Vector3& ppos );
	void Render( char* name );

	bool CheckClear();
	bool CrashCheck( Vector3* ppos, int power  );


};

#endif	//	_ENEMY_H_