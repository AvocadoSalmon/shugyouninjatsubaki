#ifndef _FADE_H_
#define _FADE_H_


#define MODE_FADE_RESET		-1
#define MODE_FADE_INIT		0
#define MODE_FADE_ACT		1

#define FADE_ALPHA_SHIFT	24
#define FADE_MAX_COLOR		255

#define FADE_INC_VALUE		8
#define FADE_DEC_VALUE		8

class c_Fade
{
private:
	int m_mode;		//	0->イン 1->アウト
	int m_current;	//	現在の値
	int	m_step;		//	ステップ処理用
	bool m_in;		//	フェードイン完了フラグ
	bool m_out;		//	フェードアウト完了フラグ
public:
	c_Fade();
	~c_Fade();

	void Update();
	void Render();

	bool FadeIn();
	bool FadeOut();
	
	bool GetIn(){ return m_in; }	//	フェードインが完了したかどうか調べる
	bool GetOut(){ return m_out; }	//	フェードアウトが完了したかどうか調べる
	
	void SetColor(int c){  m_current = c; }
	void SetMode(int m){  m_step = 0; m_mode = m; }

};


#endif	//	_FADE_H_