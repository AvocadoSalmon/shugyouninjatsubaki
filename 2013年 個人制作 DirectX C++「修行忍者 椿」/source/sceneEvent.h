//*****************************************************************************************************************************
//
//		メインシーン
//
//*****************************************************************************************************************************
#ifndef _SCENE_EVENT_H_
#define _SCENE_EVENT_H_


#include	"Stage.h"
#include	"BaseObject.h"
#include	"Stage.h"
#include	"Player.h"
#include	"Camera.h"
#include	"Enemy.h"
#include	"Collision.h"
#include	"Fade.h"
#include	"SaveLoad.h"
#include	"Script.h"
#include	"Text.h"

/* マクロ定義 */
#define EVENT_LIGHT_DIRECTION	Vector3( 0,-1,1 )					//	ライト方向
#define EVENT_LIGHT_COLOR		0.75f, 0.6f, 0.6f					//	ライト色
#define EVENT_AMBIENT_COLOR		0x808080							//	アンビエント色

#define EVENT_PLAYER_SIZE		0.009f								//	プレイヤーサイズ
#define EVENT_INTIAL_POS		0.0f, 0.0f, -47.0f					//	プレイヤー初期座標

#define EVENT_INTIAL_CPOS		0.0f, 1.0f, -50.0f					//	カメラ初期座標

#define MES_X					580									//	イベント中の文字X座標
#define MES_Y					670									//	イベント中の文字Y座標
#define MES_SHADOW_COLOR		0xFF000000							//	イベント中の文字影の色
#define MES_COLOR				0xFFFFFFFF							//	イベント中の文字の色
	
#define NAME_MAX				128									//	ステージ名用のchar最大値
#define LOAD_NAME_MAX			256									//	読み込みステージ名用のchar最大値

#define EVENT_PARTICLE_MAX		1024								//	パーティクル最大数
#define EVENT_SKY_COLOR			Vector3( 0.6f, 0.6f, 0.6f )			//	空の色
#define EVENT_GROUND_COLOR		Vector3( 0.8f, 0.8f, 0.8f )			//	地面の色

#define EVENT_SCRIPTFILE		"script"							//	スクリプトファイル
#define EVENT_FONT_FILE			"DATA\\FontData"					//	フォントファイル

#define EVENT_SCENE_TIMER		60 * 2								//	シーン切り替えまでの時間
#define EVENT_FADEOUT_TIMER		10									//	フェードアウトまでの時間

class	sceneEvent : public Scene
{
private:
	iexView* view;
	c_StageManager* stage;
	EnemyManager* enemy_manager;
	c_Fade*		fade;
	iex2DObj* EnvTex;		//	環境マップ用テクスチャ
	iex2DObj* flare;		//	レンズフレア用テクスチャ
	iex2DObj* screen;		//	スクリーンフィルタ
	iex2DObj* HDR;			//	擬似ＨＤＲ
	iex2DObj* seaTexture;	//	海用テクスチャ
	iex2DObj* Text;			//	表示テキスト
	iex2DObj* Frame;		//	イベント枠
	Surface* pBackBuffer;	//	フレームバッファ
	c_Script  scr;			//	スクリプト
	char message[256];
	int count;
	c_String*	Font;		//	フォント
	int	m_ChangeTimer;		//	シーン変更までのカウンタ
	bool m_bChangeFlg;		//	シーン変更までのカウントフラグ
	bool m_bStop;			//	プレイヤー停止フラグ
	iex2DObj*	ShadowTex;	//	シャドウテクスチャ
	Surface*	ShadowZ;	//	影Zバッファ
	iex2DObj*	Depth;		//	被写界深度
	float m_FocusDist;		//	被写界深度焦点距離
	float m_FocusRange;		//	被写界深度焦点範囲
public:
	~sceneEvent();
	//	初期化
	bool Initialize();
	//	更新・描画
	void Update();	//	更新
	void Render();	//	描画

	void LenzFlare();		//	レンズフレア描画

	void RenderHDR();		//	HDR描画
	void InitShadow();		//	影初期化
	void RenderShadow();	//	影描画
	void RenderDepth();		//	被写界深度描画


	void ChageStage( char* name,Vector3& pos );
	void ChageStage( int n );

	/* スクリプト関連 */
	void SetMessage( char *msg );
	void SetCount( int s_count );
	void SetScriptScene( sceneEvent *s );
	void SetWalk();
	void SetMotion( int m );
	inline void SetStop( bool s ){ m_bStop = s; }
	inline void SetChange( bool f ){ m_bChangeFlg = f; }
	void SetCameraPos( Vector3& p );
	void SetCameraTarget( Vector3& t );
	void SetCameraTargetToPlayer();
	void ChangeCamera( int m );
	void ChangeFocusDist( float dist );
	void ChangeFocusRange( float range );

};




#endif	//	_SCENE_EVENT_H_

