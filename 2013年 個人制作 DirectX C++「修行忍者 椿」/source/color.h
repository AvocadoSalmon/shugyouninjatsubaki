#ifndef __COLOR_H__
#define __COLOR_H__

//	�J���[
#define RGBA(r,g,b,a)	((COLOR)(((a)<<24)|((r)<<16)|((g)<<8)|(b)))

#define RED			RGBA(255,0,0,255)
#define GREEN		RGBA(0,255,0,255)
#define BLUE		RGBA(0,0,255,255)
#define YELLOW		RGBA(255,255,0,255)
#define ORANGE		RGBA(255,128,0,255)
#define MAGENTA		RGBA(255,0,255,255)
#define CYAN		RGBA(0,255,255,255)
#define WHITE		RGBA(255,255,255,255)
#define GRAY		RGBA(128,128,128,255)
#define BLACK		RGBA(0,0,0,255)
#define SEMITRANS	RGBA(255,255,255,128)

#define GET_ALPHA(c)	((u8)((c) >> 24))
#define GET_RED(c)		((u8)(((c) >> 16) & 0xff))
#define GET_GREEN(c)	((u8)(((c) >>  8) & 0xff))
#define GET_BLUE(c)		((u8)((c) & 0xff))

//typedef D3DXCOLOR


#endif