#include	"iextreme.h"
#include	"system/system.h"

#include	"BaseObject.h"
#include	"Collision.h"

//**************************************************************************************//
//																						//
//																						//
//																						//
//**************************************************************************************//



//**************************************************************************************//
//																						//
//		コンストラクタ・デストラクタ													//
//																						//
//**************************************************************************************//
BaseOBJ::BaseOBJ()
{

}

BaseOBJ::BaseOBJ(char *name)
{
	angle	= 0.0f;
	Pos.x	= 0.0f;
	Pos.y	= 0.0f;
	Pos.z	= 0.0f;
	Move.x	= 0.0f;
	Move.y	= 0.0f;
	Move.z	= 0.0f;
	gravity	= 0.0f;
	scale	= OBJECT_SIZE_BASIC;
	size	= OBJECT_SIZE_COLLIDE;

	bAlive = true;
	obj = new iex3DObj( name );
}

BaseOBJ::~BaseOBJ()
{
	DEL( obj );
}

//**************************************************************************************//
//																						//
//		更新																			//
//																						//
//**************************************************************************************//
void BaseOBJ::Update()
{
	//	座標更新
	Pos	= collision->CheckMove( Pos,Move );

	gravity -= GRAVITY;	//	重力値
	Pos.y += gravity;	//	Y座標更新
	GroundCheck();		//	地面チェック

	//	情報更新
	obj->SetScale( scale );
	obj->SetPos( Pos );
	obj->SetAngle( angle );
	obj->Animation();	//	モーション更新
	obj->Update();

}

//**************************************************************************************//
//																						//
//		描画																			//
//																						//
//**************************************************************************************//
void BaseOBJ::Render()
{
	obj->Render( shader,"copy_fx2" );
}

void BaseOBJ::Render( char* techname )
{
	obj->Render( shader, techname );
}

//**************************************************************************************//
//																						//
//																						//
//																						//
//**************************************************************************************//

//--------------------------------------------------------------------------------
//	引数: モーション番号
//	機能: n番目のモーションをセットする
//--------------------------------------------------------------------------------
void BaseOBJ::SetMotion( int n )
{
	if( obj->GetMotion() != n ){
		obj->SetMotion( n );
	}
}

//--------------------------------------------------------------------------------
//	引数: なし
//	機能: 地面ならtrue,空中ならfalseを返す
//--------------------------------------------------------------------------------
bool BaseOBJ::GroundCheck()
{
	float height = collision->FloorHeight(Pos);

	if( Pos.y < height ){
		gravity = 0;
		Pos.y = height;
		return true;
	}else{
		return false;
	}
}

//--------------------------------------------------------------------------------
//	引数: 指定オブジェクト、影響先座標、終点ボーン番号、終点から影響させるボーン数
//	機能: 指定オブジェクトのボーンにIKを適用させる
//--------------------------------------------------------------------------------
void IK( iex3DObj* obj, Vector3* target, int sBone, int numBone )
{
	//	ターゲット座標をモデルローカル化
	Vector3	tpos;
	Matrix	mat;
	D3DXMatrixInverse( &mat, NULL, &obj->TransMatrix );
	tpos.x = target->x * mat._11  +  target->y * mat._21  +  target->z * mat._31  +  mat._41;
	tpos.y = target->x * mat._12  +  target->y * mat._22  +  target->z * mat._32  +  mat._42;
	tpos.z = target->x * mat._13  +  target->y * mat._23  +  target->z * mat._33  +  mat._43;

	//	終点の設定
	mat = *obj->GetBone(sBone);
	Vector3	epos = Vector3( mat._41, mat._42, mat._43 );
	
	//	もし近ければＩＫの必要なし
	if( (epos-tpos).LengthSq() < 5*5 ) return;


	sBone = obj->BoneParent[sBone];

	//	とりあえず１０回(ＩＫのクォリティ)
	for( int n=0 ; n<IK_QUOLITY ; n++ )
	{
		int		bone = sBone;

		for( int b=1 ; b<numBone ; b++ )
		{
			Vector3 bonePos = *obj->GetBonePos(bone);
			Vector3 work;
			//	親ボーン取得
			int pbone = obj->BoneParent[bone];

			//	親空間ローカル化行列(原点は自分の位置)
			mat = *obj->GetBone(pbone);
			D3DXMatrixInverse( &mat, NULL, &mat );
			mat._41 -= bonePos.x;
			mat._42 -= bonePos.y;
			mat._43 -= bonePos.z;

			//	ターゲット座標のローカル化
			work.x = tpos.x*mat._11 + tpos.y*mat._21 + tpos.z*mat._31 + mat._41;
			work.y = tpos.x*mat._12 + tpos.y*mat._22 + tpos.z*mat._32 + mat._42;
			work.z = tpos.x*mat._13 + tpos.y*mat._23 + tpos.z*mat._33 + mat._43;

			//	終点座標のローカル化
			Vector3	ework, ework2;
			ework2.x = epos.x*mat._11 + epos.y*mat._21 + epos.z*mat._31 + mat._41;
			ework2.y = epos.x*mat._12 + epos.y*mat._22 + epos.z*mat._32 + mat._42;
			ework2.z = epos.x*mat._13 + epos.y*mat._23 + epos.z*mat._33 + mat._43;
			ework = ework2;
			//	ベクトル単位化
			work.Normalize();
			ework.Normalize();

			//	外積による軸算出
			Vector3 axis;
			Vector3Cross( axis, ework, work );
			axis.Normalize();

			//	コサイン取得
			float c = (ework.x*work.x + ework.y*work.y + ework.z*work.z);
			float s2, c2;
			s2 = sqrtf( (1-c) / 2 );
			c2 = sqrtf( (1+c) / 2 );
			//	クオータニオン作成
			Quaternion q;
			q.x = axis.x * s2;
			q.y = axis.y * s2;
			q.z = axis.z * s2;
			q.w = c2;
			//	ボーンのポーズ更新
			*obj->GetPose(bone) *= q;
			//	終点更新(親空間ローカル)
			q.toMatrix(mat);
			work.x = ework2.x*mat._11 + ework2.y*mat._21 + ework2.z*mat._31 + bonePos.x;
			work.y = ework2.x*mat._12 + ework2.y*mat._22 + ework2.z*mat._32 + bonePos.y;
			work.z = ework2.x*mat._13 + ework2.y*mat._23 + ework2.z*mat._33 + bonePos.z;

			//	終点座標のワールド化
			mat = (*obj->GetBone(pbone));
			epos.x = work.x*mat._11 + work.y*mat._21 + work.z*mat._31 + mat._41;
			epos.y = work.x*mat._12 + work.y*mat._22 + work.z*mat._32 + mat._42;
			epos.z = work.x*mat._13 + work.y*mat._23 + work.z*mat._33 + mat._43;

			bone = pbone;
		}
		//	ボーン更新
		obj->UpdateBoneMatrix();
		
		//	終点チェック
		Vector3	w = tpos - epos;
		if( w.x*w.x + w.y*w.y + w.z*w.z < 0.1f*0.1f ) break;
	}
}

//--------------------------------------------------------------------------------
//	引数: 表示するオブジェクトの座標
//	機能: プレイヤーの頭上のスクリーン座標を取得
//--------------------------------------------------------------------------------
Vector3 BaseOBJ::Get2DCoord( Vector3 &pos )
{
	//	スクリーン座標変換
	Matrix mat = matView * matProjection;

	Vector3 p = pos + BASE_2D_BIAS;	//	プレイヤーの少し上
	float x = p.x * mat._11 + p.y * mat._21 + p.z * mat._31 + mat._41;
	float y = p.x * mat._12 + p.y * mat._22 + p.z * mat._32 + mat._42;
	float w = p.x * mat._14 + p.y * mat._24 + p.z * mat._34 + mat._44;

	//	画面の座標へ
	x /= w;
	y /= w;

	x = ( x+1 ) *  SCREEN_HALF_WIDTH  / 2;
	y = ( 1-y ) *  SCREEN_HALF_HEIGHT / 2;

	return Vector3( x, y, 0 );
}