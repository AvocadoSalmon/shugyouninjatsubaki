#ifndef _STAGE_H_
#define _STAGE_H_


#include	"Environment.h"

#define STAGE_SIZE						1.0f			//	ステージサイズ
//**************************************************************************************//
//																						//
//	c_StageManagerクラス																//
//																						//
//**************************************************************************************//
class c_StageManager
{
protected:
	iexMesh* m_lpMesh;
public:
	virtual void Load() = 0;
	virtual void Load( char *name ) = 0;

	virtual ~c_StageManager(){ DEL(m_lpMesh); }
	virtual void Update() = 0;
	virtual void Render( char* name ) = 0;
	virtual void Render() = 0;
};

//**************************************************************************************//
//																						//
//	c_Stage1クラス																		//
//																						//
//**************************************************************************************//
#define STAGE1_CAMERA_INITIAL_TARGET	Vector3( 0,3.0f,0 )				//	ステージ１カメラ初期ターゲット
#define STAGE1_CAMERA_INITIAL_POS		Vector3( 0.0f,3.2f,-10.0f )		//	ステージ１カメラ初期座標

class c_Stage1 : public c_StageManager
{
private:
	c_Sky* m_sky;
	iexMesh* m_sea;
	iexMesh* m_cloud;
public:
	c_Stage1();
	~c_Stage1();

	void Load();
	void Load( char *name );

	void Update();
	void Render( char* name );
	void Render();
};

//**************************************************************************************//
//																						//
//	c_Stage2クラス																		//
//																						//
//**************************************************************************************//
class c_Stage2 : public c_StageManager
{
private:
	c_Sky* m_sky;
public:
	c_Stage2();
	~c_Stage2();

	void Load();
	void Load( char *name );

	void Update();
	void Render( char* name );
	void Render();
};



#endif	//	_STAGE_H_