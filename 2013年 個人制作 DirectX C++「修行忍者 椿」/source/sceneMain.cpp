#include	"iextreme.h"
#include	"system/system.h"

#include	"ThreadObject.h"
#include	"NowLoad.h"

#include	"sceneMain.h"

#ifdef _DEBUG
#include "Lib_Debug.h"
using namespace Debug;
#endif


//*****************************************************************************************************************************
//
//
//
//*****************************************************************************************************************************

c_PLAYER* player;
c_Collision*	collision;


//*****************************************************************************************************************************
//
//		初期化
//
//*****************************************************************************************************************************
bool sceneMain::Initialize()
{
	//	フェードイン
	fade = new c_Fade();
	fade->SetMode( MODE_FADE_INIT );

	count = 0;	//	ウェイト時間初期化

	//	パーティクル読み込み
	iexParticle::Initialize( FILE_PATH_PARTICLE, MIAN_PARTICLE_MAX );
	//	ステージ読み込み
	stage = new c_Stage1();
	stage->Load();
	shader->SetValue( "SkyColor", MIAN_SKY_COLOR );
	shader->SetValue( "GroundColor", MAIN_GROUND_COLOR );
	//	ステージ判定モデル読み込み
	collision = new c_Collision();
	collision->Load( FILE_PATH_BG_STAGE01_COLLISION );
	//	カメラ初期設定
	CAMERA.SetMode( CAMERA_NORMAL );
	//	プレイヤー読み込み
	player = new c_PLAYER( FILE_PATH_PLAYER_MODEL );
	player->SetScale( MAIN_PLAYER_SIZE );	//	スケール設定
	//	レンズフレア読み込み
	flare = new iex2DObj( FILE_PATH_LENZ_FLARE );
	//	2D読み込み
	Text = new iex2DObj( FILE_PATH_MAIN_TEXT );
	//	スクリーンフィルタ
	screen = new iex2DObj( SCREEN_WIDTH, SCREEN_HEIGHT, IEX2D_RENDERTARGET );
	shader2D->SetValue( "contrast", FILTER_CONTRAST );
	shader2D->SetValue( "chroma", FILTER_CHROMA );
	shader2D->SetValue( "ScreenColor", FILTER_COLOR );
	//	敵管理クラスの実体生成
	enemy_manager = new EnemyManager();
	//	環境マップ設定
	EnvTex = new iex2DObj( FILE_PATH_ENVMAP );
	shader->SetValue("EnvMap", EnvTex);
	//	海テクスチャ設定
	seaTexture = new iex2DObj( FILE_PATH_NORMAL_SEA );
	shader->SetValue("SeaMap", seaTexture->lpTexture );
	//	HDRレンダーターゲット
	HDR = new iex2DObj( HDR_SIZE, HDR_SIZE, IEX2D_RENDERTARGET );
	//	被写界深度一時レンダリング用サーフェイス
	Depth = new iex2DObj( SCREEN_WIDTH,SCREEN_HEIGHT, IEX2D_FLOAT );
	//	シャドウマップ初期化
	InitShadow();
	//	文字列初期化
	Font = new c_String();
	Font->Init( FILE_PATH_FONTDATA );
	//	BGM読み込み
	m_bgm = IEX_PlayStreamSound( FILE_PATH_BGM_STAGE1);

#ifdef _DEBUG
	Debug_Init();	//	デバッグシステム初期化
#endif

	return true;
}

//-----------------------------------------------------
//	解放
//-----------------------------------------------------
sceneMain::~sceneMain()
{
	iexParticle::Release();
	DEL( enemy_manager );
	DEL( collision );
	DEL( flare );
	DEL( screen );
	pBackBuffer->Release();
	DEL( Text );
	DEL( stage );
	DEL( player );
	DEL( fade );
	DEL( EnvTex );
	DEL( seaTexture );
	DEL( HDR );
	DEL( Font );
	DEL( ShadowTex );
	ShadowZ->Release();
	IEX_StopStreamSound( m_bgm );	//	BGMを停止

#ifdef _DEBUG
	CDebug::Rele();		//	デバッグシステム解放
#endif
	

}

//------------------------------------
//	デバッグ用関数
//------------------------------------
Vector3 pos;
float scale;
void CallBack_Pos(void*){ player->SetPos( pos ); }				//	座標セット
void CallBack_Scale(void*){ player->SetScale( scale ); }		//	サイズセット

//	デバッグ機能初期化
void sceneMain::Debug_Init()
{
#ifdef _DEBUG
	//	値の代入
	pos   = player->GetPos();
	scale = player->GetScale();

	CDebug::NewGroup(0);
	CDebug::SetGroup(0);

	CDebug::Title( "PLAYER" );
	CDebug::Bracket( "座標調整",1 );
	CDebug::Bracket( "サイズ調整",2 );

	CDebug::NewGroup(1);
	CDebug::SetGroup(1);
	CDebug::Value( "PlayerX = %0.3f",&pos.x,DEBUG_PRM_INCR,-DEBUG_PRM_MIN,DEBUG_PRM_MAX );
	CDebug::ValueCall( CallBack_Pos );
	CDebug::Value( "PlayerY = %0.3f",&pos.y,DEBUG_PRM_INCR,-DEBUG_PRM_MIN,DEBUG_PRM_MAX );
	CDebug::ValueCall( CallBack_Pos );
	CDebug::Value( "PlayerZ = %0.3f",&pos.z,DEBUG_PRM_INCR,-DEBUG_PRM_MIN,DEBUG_PRM_MAX );
	CDebug::ValueCall( CallBack_Pos );

	CDebug::NewGroup(2);
	CDebug::SetGroup(2);
	CDebug::Value( "SCALE = %0.3f",&scale,DEBUG_SIZE_INCR,-DEBUG_PRM_MIN,DEBUG_PRM_MAX );
	CDebug::ValueCall( CallBack_Scale );
#endif
}

//*****************************************************************************************************************************
//
//		主処理
//
//*****************************************************************************************************************************

void	sceneMain::Update()
{
	//	光源・アンビエント設定
	iexLight::DirLight( shader, 0, &MAIN_LIGHT_DIRECTION, MAIN_LIGHT_COLOR );
	iexLight::SetAmbient( MAIN_AMBIENT_COLOR );

	fade->Update();									//	フェード更新
	stage->Update();								//	ステージ更新
	player->Update();								//	プレイヤー更新
	player->HitCheck(enemy_manager);				//	プレイヤー当たり判定
	enemy_manager->Update( player->GetPos()  );		//	全敵更新

	iexParticle::Update();							//	パーティクル更新

	if( enemy_manager->CheckClear() ){	//	敵全滅
		fade->FadeOut();				//	フェードアウト
		ChangeScene( MODE_ENDING );		//	エンディングモードへ
	}

	//	流れる水処理
	static float tv = 0;
	tv -= WATER_FLOW_SPEED;
	shader->SetValue("AdjustSea", tv );

//-------------------------------
//	デバッグモード
//-------------------------------
#ifdef _DEBUG
	if( CDebug::IsOpen( START_GROUP ) )
	{
		CDebug::Exec();
		
	}
	else if( KEY_Get( KEY_ENTER ) == FIRST_PRESS )
	{
		CDebug::Open( DEBUG_MENU_COORD );
	}

	//	セーブ＆ロード
	if( GetKeyState('1') < 0 )Save( FILE_TEXT_POS_SAVE,player );
	if( GetKeyState('2') < 0 )Load( FILE_TEXT_POS_SAVE,player );

	//	仮ステージ切り替え
	if( GetKeyState('3') < 0 )ChageStage( DEBUG_STAGE_GRID );		//	GRIDステージ
	if( GetKeyState('4') < 0 )ChageStage( DEBUG_STAGE_NAKATEN );	//	NAKATENステージ

	if( GetKeyState('5') < 0 )fade->FadeIn();
	if( GetKeyState('6') < 0 )fade->FadeOut();

#endif

}

//--------------------------------------------------------------------------------
//	引数: なし
//	機能: 擬似的にHDR効果を描画する
//--------------------------------------------------------------------------------
void sceneMain::RenderHDR()
{
	//	HDRバッファへ切り替え
	HDR->RenderTarget();
	CAMERA.SetViewPort( 0,0,HDR_SIZE,HDR_SIZE );
	CAMERA.SetProjection( PRM_PROJECTION_HDR );
	CAMERA.Begin();
	
	stage->Render( "specular" );
	player->Render( "specular" );
	player->RenderLocus();
	enemy_manager->Render( "specular" );
	iexParticle::Render( shader2D,"add" );

	//	ぼかし
	DWORD	colorHDR = ( PRM_HDR_INIT_ALPHA ) | COLOR_WHITE;
	for( int i=HDR_POLYGON_SIZE ; i<HDR_POLYGON_SIZE_MAX ; i+=HDR_POLYGON_SIZE )
	{
		HDR->Render(  i,0,HDR_SIZE,HDR_SIZE, 0,0,HDR_SIZE,HDR_SIZE, shader2D,"add", colorHDR );
		HDR->Render( -i,0,HDR_SIZE,HDR_SIZE, 0,0,HDR_SIZE,HDR_SIZE, shader2D,"add", colorHDR );
		HDR->Render( 0,i,HDR_SIZE,HDR_SIZE, 0,0,HDR_SIZE,HDR_SIZE,  shader2D,"add", colorHDR );
		HDR->Render( 0,-i,HDR_SIZE,HDR_SIZE, 0,0,HDR_SIZE,HDR_SIZE, shader2D,"add", colorHDR );

		HDR->Render(  i,0,HDR_SIZE,HDR_SIZE, 0,0,HDR_SIZE,HDR_SIZE, shader2D,"gauss", colorHDR );
		HDR->Render( -i,0,HDR_SIZE,HDR_SIZE, 0,0,HDR_SIZE,HDR_SIZE, shader2D,"gauss", colorHDR );
		HDR->Render( 0,i,HDR_SIZE,HDR_SIZE, 0,0,HDR_SIZE,HDR_SIZE,  shader2D,"gauss", colorHDR );
		HDR->Render( 0,-i,HDR_SIZE,HDR_SIZE, 0,0,HDR_SIZE,HDR_SIZE, shader2D,"gauss", colorHDR );

		//	だんだん透明に
		colorHDR -= PRM_HDR_DECR_ALPHA;
	}
	
}

//--------------------------------------------------------------------------------
//	引数: なし
//	機能: 被写界深度を描画する
//--------------------------------------------------------------------------------
void sceneMain::RenderDepth()
{
	Depth->RenderTarget();
	CAMERA.Begin();

	//	物体描画
	stage->Render( "depth" );
	player->EventRender( "depth" );

	shader2D->SetValue( "DepthTex",Depth->lpTexture );
}

//*****************************************************************************************************************************
//
//		描画関連
//
//*****************************************************************************************************************************
void	sceneMain::Render()
{
	//	フレームバッファのポインタ保存
	iexSystem::GetDevice()->GetRenderTarget( 0, &pBackBuffer );
	
	RenderHDR();														//	HDR
	RenderShadow();														//	シャドウマップ作成
	screen->RenderTarget();

	//	カメラ設定
	CAMERA.SetViewPort( PRM_VIEWPORT_N );
	CAMERA.SetProjection( PRM_PROJECTION_N );
	CAMERA.Begin();

	//	被写界深度設定
	Vector3 FocusDist = player->GetPos() - CAMERA.GetPos();
	shader2D->SetValue( "FocusDist" , FocusDist.Length() );
	shader2D->SetValue( "FocusRange" , DEFAULT_FOSUS_RANGE );

	stage->Render();													//	背景描画
	player->Render();													//	プレイヤー描画
	enemy_manager->Render( "full" );									//	全敵描画
	iexParticle::Render( shader2D,"Particle" );							//	パーティクル描画
	LenzFlare();														//	レンズフレア描画
	HDR->Render( 0,0,SCREEN_WIDTH,SCREEN_HEIGHT,0,0,HDR_SIZE,HDR_SIZE,shader2D,"add",HDR_ALPHA );	//	擬似ＨＤＲ描画

	//	画面に色調を変えて復元	
	screen->Render( 0,0,SCREEN_WIDTH,SCREEN_HEIGHT, 0,0,SCREEN_WIDTH,SCREEN_HEIGHT, shader2D, "Mastering" );
	//	フレームバッファへ切り替え
	iexSystem::GetDevice()->SetRenderTarget( 0,pBackBuffer );
	screen->Render( 0,0,SCREEN_WIDTH,SCREEN_HEIGHT, 0,0,SCREEN_WIDTH,SCREEN_HEIGHT, shader2D, "copy" );

	//	深度情報描画
	RenderDepth();
	//	スクリーンをぼかす
	screen->RenderTarget();

	screen->Render( DOF_COORD_1,SCREEN_WIDTH,SCREEN_HEIGHT,0,0,SCREEN_WIDTH,SCREEN_HEIGHT,shader2D,"copy",DOF_ALPHA_1 );
	screen->Render( DOF_COORD_2,SCREEN_WIDTH,SCREEN_HEIGHT,0,0,SCREEN_WIDTH,SCREEN_HEIGHT,shader2D,"copy",DOF_ALPHA_2 );
	screen->Render( DOF_COORD_3,SCREEN_WIDTH,SCREEN_HEIGHT,0,0,SCREEN_WIDTH,SCREEN_HEIGHT,shader2D,"copy",DOF_ALPHA_3 );
	screen->Render( DOF_COORD_4,SCREEN_WIDTH,SCREEN_HEIGHT,0,0,SCREEN_WIDTH,SCREEN_HEIGHT,shader2D,"copy",DOF_ALPHA_4 );
	screen->Render( DOF_COORD_5,SCREEN_WIDTH,SCREEN_HEIGHT,0,0,SCREEN_WIDTH,SCREEN_HEIGHT,shader2D,"copy",DOF_ALPHA_5 );
	//	フレームバッファへ切り替え
	iexSystem::GetDevice()->SetRenderTarget( 0,pBackBuffer );
	//	被写界深度
	screen->Render( 0,0,SCREEN_WIDTH,SCREEN_HEIGHT,0,0,SCREEN_WIDTH,SCREEN_HEIGHT,shader2D,"focus" );



	/* 2D描画 */
	StartText();									//	テキスト描画
	//Font->RenderStr( 100,60,"ＳＥＴ/ＰＯＷＥＲ",0xFF000000,0xFFFFFFFF );	//	文字列描画	
	fade->Render();									//	フェード描画
	

}

//*****************************************************************************************************************************
//
//		処理
//
//*****************************************************************************************************************************

//--------------------------------------------------------------------------------
//	引数: ファイル名、プレイヤーの座標
//	機能: ステージを切り替え、プレイヤーを指定座標へセットする
//--------------------------------------------------------------------------------
void sceneMain::ChageStage( char* name,Vector3& pos )
{
	stage->Load( name );
	player->SetPos( pos );
}

//--------------------------------------------------------------------------------
//	引数: ステージ切り替え番号
//	機能: データを読み込んでn番のステージに切り替える
//--------------------------------------------------------------------------------
void sceneMain::ChageStage( int n )
{
	char name[CHAR_NUM_MAX];
	Vector3 pos;

	//	読み込み
	char file[LOAD_NAME_MAX];
	sprintf( file,"DATA\\TXT\\%d.txt",n );
	FILE* fp = fopen( file,"rt" );
	fscanf( fp,"%s",name );
	fscanf( fp,"%f,%f,%f",&pos.x,&pos.y,&pos.z );
	fclose( fp );

	ChageStage( name,pos );
}

//--------------------------------------------------------------------------------
//	引数: なし
//	機能: レンズフレアを描画する
//--------------------------------------------------------------------------------
void sceneMain::LenzFlare()
{
	//	仮太陽位置
	Vector3 Sun( SUN_COORD );
	Matrix m = matView * matProjection;
	float SunX = Sun.x * m._11 + Sun.y * m._21 + Sun.z * m._31 + m._41;
	float SunY = Sun.x * m._12 + Sun.y * m._22 + Sun.z * m._32 + m._42;
	float z    = Sun.x * m._13 + Sun.y * m._23 + Sun.z * m._33 + m._43;
	float w    = Sun.x * m._14 + Sun.y * m._24 + Sun.z * m._34 + m._44;
	if( z < 0 )return;
	SunX = ( SunX/w + FLARE_BIAS ) * SCREEN_HALF_WIDTH;
	SunY = ( -SunY/w + FLARE_BIAS ) * SCREEN_HALF_HEIGHT;


	//	光源から画面中心へのベクトル
	float vx = SCREEN_HALF_WIDTH - SunX;
	float vy = SCREEN_HALF_HEIGHT - SunY;

	//	フレアの間隔
	float FlareRate[] = {
		FLARE_RATE
	};

	//	フレアのタイプ
	int flareType[] = {
		FLARE_PTN
	};

	//	フレアのサイズ
	int flareSize[] = {
		FLARE_SIZE
	};

	//	フレアの色
	DWORD flareColor[] = {
		FLARE_COLOR
	};

	for( int i=0; i < FLARE_MAX; i++ ){
		int fx = (int)( FlareRate[i] * vx + SunX );
		int fy = (int)( FlareRate[i] * vy + SunY );
		
		flare->Render( fx-flareSize[i],fy-flareSize[i],flareSize[i]*2,flareSize[i]*2,flareType[i]*128,0,128,128,RS_ADD,flareColor[i] );
	}

	
}

//--------------------------------------------------------------------------------
//	引数: なし
//	機能: ゲーム開始時のSTART表示
//--------------------------------------------------------------------------------
void sceneMain::StartText()
{
	static int wx = 0;
	static int wy = MAIN_TEXT_HEIGHT;
	static bool	ScaleFlag = false;
	static int	RenderTimer = MAIN_TIME_COUNT;

	if( !ScaleFlag ){
		wx += MAIN_TEXT_WIDTH / MAIN_TEXT_SIZE;
		if( wx >= MAIN_TEXT_WIDTH ){
			wx = MAIN_TEXT_WIDTH;
			RenderTimer--;
		}
		if( RenderTimer <= 0 )ScaleFlag = true;
	}
	else{
		wy -= MAIN_TEXT_HEIGHT / MAIN_TEXT_SIZE;
		if( wy <= 0 )wy = 0;
	}

	Text->Render( MAIN_TEXT_X, MAIN_TEXT_Y, wx, wy, 0, 0, MAIN_TEXT_WIDTH, MAIN_TEXT_HEIGHT, shader2D, "copy" );
}

//*****************************************************************************************************************************
//
//		シャドウマップ
//
//*****************************************************************************************************************************

//	シャドウ初期化
void sceneMain::InitShadow()
{
	ShadowTex = new iex2DObj( SHADOW_SIZE,SHADOW_SIZE, IEX2D_RENDERTARGET );
	iexSystem::GetDevice()->CreateDepthStencilSurface( SHADOW_SIZE, SHADOW_SIZE, D3DFMT_D16, D3DMULTISAMPLE_NONE, 0, FALSE, &ShadowZ, NULL );
}

//	シャドウマップ作成
void sceneMain::RenderShadow()
{
	LPDIRECT3DSURFACE9	org;
	iexSystem::GetDevice()->GetRenderTarget( 0, &org );

	//	レンダーターゲット設定
	ShadowTex->RenderTarget();
	//	Ｚバッファ設定
	Surface*	orgZ;
	iexSystem::GetDevice()->GetDepthStencilSurface( &orgZ );
	iexSystem::GetDevice()->SetDepthStencilSurface(ShadowZ);

	//	ライト方向
	Vector3 dir = MAIN_LIGHT_DIRECTION;
	dir.Normalize();

	//	シャドウ作成
	Vector3	target = player->GetPos();
	Vector3 pos = target - dir*SHADOW_DIST;
	Vector3 up( 0, 1, 0 );			//	カメラ上方向設定
	//	視点とライト位置へ
	D3DXMATRIX	ShadowMat, work;
	LookAtLH( ShadowMat, pos, target, up );
	D3DXMatrixOrthoLH( &work, SHADOW_PROJ_SIZE, SHADOW_PROJ_SIZE, SHADOW_PROJ_NEAR, SHADOW_PROJ_FAR );	//	平行投影行列(範囲100x100)
	ShadowMat *= work;

	shader->SetValue( "ShadowProjection",  &ShadowMat );

	D3DVIEWPORT9 vp = { 0,0,SHADOW_SIZE,SHADOW_SIZE, 0, 1.0f };
	iexSystem::GetDevice()->SetViewport( &vp );
	//	レンダリング
	iexSystem::GetDevice()->Clear( 0, NULL, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER, COLOR_F_WHITE, 1.0f, 0 );

	//	描画
	stage->Render( "ShadowBuf" );
	player->Render( "ShadowBuf" );

	shader->SetValue( "ShadowMap",ShadowTex->lpTexture );

	//	レンダーターゲット復元
	iexSystem::GetDevice()->SetRenderTarget( 0, org );
	iexSystem::GetDevice()->SetDepthStencilSurface(orgZ);
}