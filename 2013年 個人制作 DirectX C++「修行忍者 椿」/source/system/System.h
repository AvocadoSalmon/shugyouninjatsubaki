#ifndef _SYSTEM_H_
#define _SYSTEM_H_

#include	"Scene.h"

#include	"..\\system/Framework.h"
#include	"..\\Collision.h"
#include	"..\\Player.h"
#include	"..\\Fade.h"

//*****************************************************************************
//
//		ファイルパス
//
//*****************************************************************************

//	タイトル
#define FILE_PATH_BG_NAKATEN				"DATA\\BG\\nakaten\\nakaten.imo"
#define FILE_PATH_BG_NAKATEN_COLLISION		"DATA\\BG\\nakaten\\nakatenX.imo"
#define FILE_PATH_TITLE_FONT				"DATA\\Title.png"
#define FILE_PATH_TITLE_VIRTUAL				"DATA\\VirtualMap.png"
#define FILE_PATH_TITLE_FILM				"DATA\\title_film.png"
#define FILE_PATH_TITLE_BFILM				"DATA\\title_filmback.png"
#define FILE_PATH_TITLE_LASER				"DATA\\UV\\laser\\laser.IEM"
#define FILE_PATH_TITLE_LIGHTSAMP			"DATA\\light.png"
//	エンディング

//	ローディング
#define FILE_PATH_LOAD_BACK					"DATA\\Load.png"
//	背景	
#define FILE_PATH_BG_PRE					"DATA\\BG\\grid.imo"
#define FILE_PATH_BG_STAGE01				"DATA\\BG\\Temple\\Temple.imo"
#define FILE_PATH_BG_STAGE01_SEA			"DATA\\BG\\Temple\\Sea.imo"
#define FILE_PATH_BG_STAGE01_CLOUD			"DATA\\BG\\Temple\\cloudy.imo"
#define FILE_PATH_BG_STAGE01_COLLISION		"DATA\\BG\\Temple\\Temple_Collision.imo"
#define FILE_PATH_BG_STAGE02				"DATA\\BG\\nakaten.imo"
#define FILE_PATH_BG_STAGE02_COLLISION		"DATA\\BG\\nakatenX.imo"
//	パーティクル
#define FILE_PATH_PARTICLE					"DATA\\particle.png"
//	UVアニメーションモデル
#define FILE_PATH_EFFUV_APPEAR				"DATA\\UV\\lighting\\lighting.IEM"
#define FILE_PATH_EFFUV_LASER				"DATA\\UV\\laser\\laser.IEM"
//	プレイヤー関係
#define FILE_PATH_PLAYER_MODEL				"DATA\\CHR\\tsubaki\\tsubaki.IEM"
#define FILE_PATH_PLAYER_LOCUS				"DATA\\locus.png"
#define FILE_PATH_PLAYER_KATANA				"DATA\\WEAPON\\Katana.x"
#define FILE_PATH_PLAYER_KUNAI				"DATA\\WEAPON\\Kunai.x"
//	イベントモード
#define FILE_PATH_EVENT_FRAME				"DATA\\EventFrame.png"
//	メインモード
#define FILE_PATH_PLAYER_GAUGE				"DATA\\gauge.png"
#define FILE_PATH_MAIN_TEXT					"DATA\\MainText.png"
#define FILE_PATH_LENZ_FLARE				"DATA\\flare.png"
#define FILE_PATH_ENVMAP					"DATA\\Env.png"
#define FILE_PATH_NORMAL_SEA				"DATA\\Nsea.png"
//	敵関係
#define FILE_PATH_ENEMY01					"DATA\\CHR\\sordios\\sordios.IEM"
#define FILE_PATH_ENEMY02					"DATA\\CHR\\e01\\e01.IEM"
#define FILE_PATH_ENEMY03					"DATA\\CHR\\enemy\\enemy.IEM"
//	テキスト
#define FILE_TEXT_POS_SAVE					"DATA\\TXT\\PositionSave.txt"
//	表示物
#define FILE_PATH_MISSION					"DATA\\Mission.png"
#define FILE_PATH_FONTDATA					"DATA\\FontData"
//	環境関係
#define FILE_PATH_ENV_CLOUD					"DATA\\cloudX.png"
//	効果音
#define FILE_PATH_SE_SELECT					"DATA\\SE\\select.wav"
#define FILE_PATH_SE_DICISION				"DATA\\SE\\start.wav"
#define FILE_PATH_SE_LOAD					"DATA\\SE\\load_start.wav"
#define FILE_PATH_SE_RUN					"DATA\\SE\\run.wav"
#define FILE_PATH_SE_BLADE1					"DATA\\SE\\blade1.wav"
#define FILE_PATH_SE_BLADE2					"DATA\\SE\\blade2.wav"
#define FILE_PATH_SE_BLADE3					"DATA\\SE\\blade3.wav"
#define FILE_PATH_SE_CHARGE					"DATA\\SE\\charge.wav"
#define FILE_PATH_SE_RELEASE				"DATA\\SE\\release.wav"
#define FILE_PATH_SE_HIT1					"DATA\\SE\\hit1.wav"
#define FILE_PATH_SE_HIT2					"DATA\\SE\\hit2.wav"
#define FILE_PATH_SE_HIT3					"DATA\\SE\\hit3.wav"
//	BGM
#define FILE_PATH_BGM_TITLE					"DATA\\BGM\\title.ogg" 
#define FILE_PATH_BGM_STAGE1				"DATA\\BGM\\Battle.ogg" 
#define FILE_PATH_BGM_ENDING				"DATA\\BGM\\ending.ogg" 

//*****************************************************************************
//
//		ゲームシステム全般
//
//*****************************************************************************

/*　マクロ定義　*/
#define DEL(x)	if(x != NULL)delete x; x = NULL;				//	セーフデリート
#define EULER	PI/180.0f										//	オイラー角
#define EVERYDIRECTION	PI*2									//	全方位(360度)

#define SCREEN_WIDTH		1280								//	スクリーン幅
#define SCREEN_HALF_WIDTH	640									//	スクリーン幅の半分
#define SCREEN_HEIGHT		720									//	スクリーン高さ
#define SCREEN_HALF_HEIGHT	360									//	スクリーン高さの半分

#define	SHADOW_SIZE			1024								//	シャドウバッファサイズ
#define	SHADOW_PROJ_SIZE	10									//	影の投影サイズ
#define	SHADOW_PROJ_NEAR	1									//	影の投影近Z
#define	SHADOW_PROJ_FAR		80									//	影の投影遠Z
#define	SHADOW_DIST			20									//	ターゲットからの距離

#define FILTER_CONTRAST		1.3f								//	スクリーンフィルタコントラスト値
#define FILTER_CHROMA		0.8f								//	スクリーンフィルタコントラスト値
#define FILTER_COLOR		Vector3( 0.8f, 0.8f, 1.0f )			//	スクリーンフィルタコントラスト値

#define	DEFAULT_FOSUS_DIST	10.0f								//	被写界深度のデフォルト焦点距離
#define	DEFAULT_FOSUS_RANGE	100.0f								//	被写界深度のデフォルト焦点範囲

#define COLOR_WHITE			0xFFFFFF							//	白色
#define COLOR_RED			0xFF0000							//	赤色
#define COLOR_GREEN			0x00FF00							//	緑色
#define COLOR_BLUE			0x0000FF							//	青色
#define COLOR_F_WHITE		0xFFFFFFFF							//	アルファ最大白色
#define COLOR_F_RED			0xFFFF0000							//	アルファ最大赤色
#define COLOR_F_GREEN		0xFF00FF00							//	アルファ最大緑色
#define COLOR_F_BLUE		0xFF0000FF							//	アルファ最大青色

#define HOLD_KEY	1											//	キー押し続け状態
#define PULL_KEY	2											//	キー離した状態
#define PUSH_KEY	3											//	キー押した状態

#define PRM_PROJECTION_N	0.8f, 0.1f, 500.0f,1280.0f/720.0f	//	通常のプロジェクション用パラメータ
#define PRM_PROJECTION_HDR	0.8f,0.1f,300.0f,1280.0f/720.0f		//	疑似HDR用のプロジェクション用パラメータ
#define PRM_VIEWPORT_N		0,0,1280,720						//	通常のビューポート用パラメータ
#define PRM_VIEWPORT_HDR	0,0,512,512							//	疑似HDR用のビューポート用パラメータ

#define WATER_FLOW_SPEED		0.0008f							//	流れる水の速さ

#define HDR_POLYGON_SIZE		4								//	疑似HDRのポリゴンサイズ
#define HDR_POLYGON_SIZE_MAX	24								//	疑似HDRの最大ポリゴンサイズ
#define PRM_HDR_INIT_ALPHA		(8*6)<<24						//	疑似HDRの初期アルファ
#define PRM_HDR_DECR_ALPHA		8<<24							//	疑似HDRのアルファ減少値
#define HDR_SIZE				512								//	疑似HDRの描画サイズ
#define HDR_CREATED_SIZE		0,0,512,512						//	疑似HDRの生成サイズ(512X512)
#define HDR_ALPHA				0xBB808080						//	疑似HDRの描画アルファ

#define	DOF_COORD_1				 3  ,  0						//	被写界深度のポリゴン座標
#define	DOF_COORD_2				-8  ,  0						//	被写界深度のポリゴン座標
#define	DOF_COORD_3				 10 ,  0						//	被写界深度のポリゴン座標
#define	DOF_COORD_4				 0  ,  5						//	被写界深度のポリゴン座標
#define	DOF_COORD_5				 0  , -8						//	被写界深度のポリゴン座標

#define	DOF_ALPHA_1				0x80FFFFFF						//	被写界深度の透明度
#define	DOF_ALPHA_2				0x40FFFFFF						//	被写界深度の透明度
#define	DOF_ALPHA_3				0x20FFFFFF						//	被写界深度の透明度
#define	DOF_ALPHA_4				0x60FFFFFF						//	被写界深度の透明度
#define	DOF_ALPHA_5				0x30FFFFFF						//	被写界深度の透明度

#define LIGHT_DIRECTION			&Vector3( 0,-1,1 )				//	ライト方向
#define LIGHT_COLOR				0.9f, 0.8f, 0.8f				//	ライト色
#define AMBIENT_COLOR			0xFFFFFFFF						//	アンビエント色

#define DEBUG_PRM_INCR			0.1f							//	デバッグ用パラメータの増加値
#define DEBUG_SIZE_INCR			0.001f							//	デバッグ用サイズの増加値
#define DEBUG_PRM_MAX			100.0f							//	デバッグ用パラメータの最大値
#define DEBUG_PRM_MIN			-100.0f							//	デバッグ用パラメータの最小値
#define DEBUG_MENU_COORD		5, 5							//	デバッグ用メニューの座標
#define DEBUG_STAGE_GRID		0								//	デバッグ用ステージ１
#define DEBUG_STAGE_NAKATEN		1								//	デバッグ用ステージ２

#define CHAR_NUM_MAX			128								//	char型配列用のとりあえずの最大値
#define LOAD_NAME_MAX			256								//	読み込みステージ名用のchar最大値

#define FLARE_MAX				9								//	レンズフレアの最大数
#define SUN_COORD				500.0f,50.0f,0.0f				//	読み込みステージ名の最大値
#define FLARE_BIAS				1.0f							//	レンズフレアの幅の調整値
#define FLARE_RATE				0.0f, 0.25f, 0.5f, 0.8f,0.9f, 1.1f, 1.4f, 1.7f, 2.0f												//	各フレアの間隔
#define FLARE_PTN				1, 1, 2, 0, 2, 2, 0, 2 , 1																			//	各フレアの種類・パターン
#define FLARE_SIZE				50, 110, 40, 90, 30, 20, 160, 40, 70																//	各フレアのサイズ
#define FLARE_COLOR				0xFFFF8080,0xFF70FF80,0xFF308850,0xFFFFCCC0,0xFFC0CCC0,0xFFC0CC50,0xFFC0C090,0xFF808040,0xFFCCCCFF,	//	各フレアの色


//	シーン切り替え用定数
enum{
	MODE_TITLE,
	MODE_MAIN,
	MODE_EVENT,
	MODE_ENDING,
};


//*****************************************************************************
//		extern変数
//*****************************************************************************
extern Framework*	MainFrame;
extern iexShader* shader;
extern iexShader* shader2D;

//*****************************************************************************
//		関数
//*****************************************************************************
//------------------------------------------------------
//	システム初期化・解放
//------------------------------------------------------
void	SYSTEM_Initialize();
void	SYSTEM_Release();

void ChangeScene( int ModeNum );	//	シーンの切り替え


//*****************************************************************************
//		フォント
//*****************************************************************************
void	InitFont( void );
void	ReleaseFont( void );
void	DrawString( LPSTR str, int x, int y, DWORD color );



#endif	//	_SYSTEM_H_