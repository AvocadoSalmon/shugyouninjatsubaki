#include	"iextreme.h"
#include	"system.h"
#include	"Framework.h"

//*****************************************************************************************************************************
//
//		フレームワーク
//
//*****************************************************************************************************************************

//------------------------------------------------------
//		初期化・解放
//------------------------------------------------------
Framework::Framework( int FPSMode )
{
	this->FPSMode = FPSMode;

	scene = NULL;

	dwFrameTime = 0;
	dwGameFrame = 0;

	dwCurFrame = 0;
	dwRCurFrame = 0;
	dwFPS = 0;
	dwRenderFPS = 0;
}

Framework::~Framework()
{
	if( scene != NULL ) delete scene;
}


//*****************************************************************************************************************************
//		更新
//*****************************************************************************************************************************
bool Framework::Update()
{
	static 	DWORD	dwSec = 0;

	//	プロセス時間取得
	timeBeginPeriod(1);
	LARGE_INTEGER work;
	QueryPerformanceCounter(&work);	
	LARGE_INTEGER nFreq;
	QueryPerformanceFrequency(&nFreq);
	timeEndPeriod(1);
	//	現在のプロセス時間(100マイクロ秒単位)	
	DWORD	CurrentTime = (DWORD)(work.QuadPart/(nFreq.QuadPart/10000));
	if( dwSec == 0 ) dwSec = CurrentTime;
	//	フレーム制限
	if( CurrentTime < dwFrameTime+167 )
	{
		Sleep(0);
		return false;
	}
	//	経過時間
	DWORD dTime = CurrentTime - dwFrameTime;
	if( dTime > 2000 ) dwFrameTime = CurrentTime;

	//	スキップタイプ 
	switch( FPSMode ){
	case FPS_60:	bRender = TRUE;	break;
	case FPS_30:	if( dwGameFrame & 0x01 ) bRender=TRUE; else bRender=FALSE;
					break;
	case FPS_FLEX:	if( dTime > 167*2 ) bRender = FALSE; else bRender = TRUE;
					break;
	}

	//	フレーム時間更新
	if( GetKeyState(VK_LCONTROL) < 0 ) dwFrameTime += 3000;
	dwFrameTime += 167;

	//	秒間フレーム数保存
	if( dwSec < CurrentTime ){
		dwFPS       = dwCurFrame;
		dwRenderFPS = dwRCurFrame;
		dwCurFrame  = dwRCurFrame = 0;
		dwSec += 10000;
	}
	dwCurFrame ++;	//	フレーム数更新
	dwGameFrame++;	//	ゲームフレーム数更新

	//	更新処理
	KEY_SetInfo();
	if( scene != NULL ) scene->Update();

	return true;
}

//*****************************************************************************************************************************
//		描画
//*****************************************************************************************************************************
void Framework::Render()
{
	if( !bRender ) return;
	//	シーン開始
	iexSystem::BeginScene(); 
	//	シーン描画
	if( scene != NULL ) scene->Render();

	//	フレーム表示
#ifdef _DEBUG
	char	str[64];
	wsprintf( str, "FPS %03d / %03d\n", dwFPS, dwRenderFPS );
	IEX_DrawText( str, 1150,10,200,20, 0xFFFFFF00 );
#endif

	// シーン終了
	iexSystem::EndScene();

	dwRCurFrame ++;	//	描画フレーム数更新
}
