#include	"iextreme.h"
#include	"system/system.h"
#include	"Script.h"
//#include	"source/sceneMain.h"
#include	"sceneEvent.h"

//*****************************************************************************************************************************
//
//
//
//*****************************************************************************************************************************

int		ScriptSize = 0;
int		ScriptIP = 0;
char	*TextBuf = NULL;

//sceneMain* s_scene;
sceneEvent* s_scene;

//--------------------------------------------------------------------------------
//	引数: sceneMainクラスの実体
//	機能: スクリプトのセット
//--------------------------------------------------------------------------------
void sceneEvent::SetScriptScene( sceneEvent *s )
{
	s_scene = s;
}

//*****************************************************************************************************************************
//
//		スクリプト初期化
//
//*****************************************************************************************************************************
BOOL	c_Script::ReleaseScript()
{
	DEL( TextBuf );

	return TRUE;
}

/*	スクリプト読み込み	*/ 
BOOL	c_Script::LoadScript( LPSTR filename )
{
	HANDLE	hfile;
	DWORD	dum;
	char	fname[MAX_PATH];

	if( TextBuf ) delete [] TextBuf;

	wsprintf( fname, "DATA\\SCR\\%s.SCR", filename );
	/*	ファイルを開く	*/ 
	hfile = CreateFile( fname, GENERIC_READ, FILE_SHARE_READ, (LPSECURITY_ATTRIBUTES)NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, (HANDLE) NULL );
	if( hfile != INVALID_HANDLE_VALUE ){
		ScriptSize = GetFileSize( hfile, NULL );
		TextBuf = new char[ScriptSize];	
		ReadFile( hfile, TextBuf, ScriptSize, &dum, NULL );
		CloseHandle(hfile);
	}
	ScriptIP = 0;
	return TRUE;
}

BOOL	c_Script::LoadScript2( LPSTR filename )
{
	ScriptSize = 0;
	ScriptIP = 0;
	TextBuf = NULL;

	HANDLE	hfile;
	DWORD	dum;

	if( TextBuf ){ delete [] TextBuf, TextBuf = NULL; }

	/*	ファイルを開く	*/ 
	hfile = CreateFile( filename, GENERIC_READ, FILE_SHARE_READ, (LPSECURITY_ATTRIBUTES)NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, (HANDLE) NULL );
	if( hfile != INVALID_HANDLE_VALUE ){
		ScriptSize = GetFileSize( hfile, NULL );
		TextBuf = new char[ScriptSize];	
		ReadFile( hfile, TextBuf, ScriptSize, &dum, NULL );
		CloseHandle(hfile);
	}
	ScriptIP = 0;

	return TRUE;
}


BOOL	c_Script::SetScriptIP( int ip )
{
	ScriptIP = ip;
	return TRUE;
}

//*****************************************************************************************************************************
//
//		パラメータ取得
//
//*****************************************************************************************************************************

//
//		頭出し
//

BOOL	c_Script::SearchParamTop( void )
{
	for( ; ScriptIP<ScriptSize ; ScriptIP++ ){
		if( TextBuf[ScriptIP] == '\t' ) continue;
		if( TextBuf[ScriptIP] == ' ' ) continue;
		if( TextBuf[ScriptIP] == ',' ) continue;
		if( TextBuf[ScriptIP] == '\n' ){
			ScriptIP++;
		}
		return TRUE;
	}
	return FALSE;
}

//
//		文字列パラメータ取得
//

void	c_Script::GetParamS( LPSTR buf )
{
	char	temp, n;

	SearchParamTop();

	for( n=0 ; ; ScriptIP++, n++ ){
		temp = TextBuf[ScriptIP];
		if( temp == ','  ) break;
		if( temp == ' '  ) break;
		if( temp == '\t' ) break;
		if( temp == '}' ){ ScriptIP--; break; }
		if( temp == 0x0d || temp == 0x0a ){
			if( TextBuf[ScriptIP+1]==0x0d || TextBuf[ScriptIP+1]==0x0a ) ScriptIP++;
			break;
		}
		buf[n] = temp;
		if( IsDBCSLeadByte(temp) ){
			buf[n+1] = TextBuf[ScriptIP+1];
			n++;
			ScriptIP++;
		}
	}
	ScriptIP++;
	buf[n] = '\0';
}


//
//		数値パラメータ取得
//

int		c_Script::GetParamN( void )
{
	int	num;
	char	TempStr[16];

	SearchParamTop();
	GetParamS(TempStr);
	num = atoi( TempStr );
	return num;
}

float	c_Script::GetParamFloat( void )
{
	float	num;
	char	TempStr[16];

	SearchParamTop();
	GetParamS(TempStr);
	num = (float)atof( TempStr );
	return num;
}

//*****************************************************************************************************************************
//
//		デコード
//
//*****************************************************************************************************************************

//
//		頭出し
//

BOOL	c_Script::SearchTop( void )
{
	for( ; ScriptIP<ScriptSize ; ScriptIP++ ){
		/*	コメント飛ばし	*/ 
		if( (TextBuf[ScriptIP]=='/') && (TextBuf[ScriptIP+1]=='/') ){
			for( ; TextBuf[ScriptIP] != '\n' ; ScriptIP++ ){
				/*	２Byte文字ならもう一個		*/ 
				if( IsDBCSLeadByte(TextBuf[ScriptIP]) ) ScriptIP++;
			}
			continue;
		}
		if( TextBuf[ScriptIP] == ' ' ) continue;
		if( TextBuf[ScriptIP] == '\t' ) continue;
		if( TextBuf[ScriptIP] == '\n' ) continue;
		if( TextBuf[ScriptIP] == 0x0d ) continue;
		if( TextBuf[ScriptIP] == '}' ) continue;
		return TRUE;
	}

	return FALSE;
}


//--------------------------------------------------------------------------------
//	引数: なし
//	機能: メッセージのセット
//--------------------------------------------------------------------------------
void c_Script::SetMSG()
{
	char msg[256];
	GetParamS( msg );
	if(msg[0]=='!'){
		s_scene->SetMessage( NULL );
	}
	else s_scene->SetMessage( msg );
}


//--------------------------------------------------------------------------------
//	引数: なし
//	機能: 待ち時間処理
//--------------------------------------------------------------------------------
void c_Script::EventWait()
{
	int count = GetParamN();
	s_scene->SetCount( count );
}

//--------------------------------------------------------------------------------
//	引数: なし
//	機能: プレイヤーを歩かせる
//--------------------------------------------------------------------------------
void c_Script::EventWalk()
{
	s_scene->SetWalk();
}

//--------------------------------------------------------------------------------
//	引数: なし
//	機能: プレイヤーのモーションセット
//--------------------------------------------------------------------------------
void c_Script::EventMotion()
{
	int motion = GetParamN();
	s_scene->SetMotion( motion );
}

//--------------------------------------------------------------------------------
//	引数: なし
//	機能: ストップ処理
//--------------------------------------------------------------------------------
void c_Script::EventStop()
{
	int judge = GetParamN();
	s_scene->SetStop( (bool)judge );
}

//--------------------------------------------------------------------------------
//	引数: なし
//	機能: イベント終了処理
//--------------------------------------------------------------------------------
void c_Script::EventEnd()
{
	ScriptIP-=5;
}

//--------------------------------------------------------------------------------
//	引数: なし
//	機能: シーン移行処理
//--------------------------------------------------------------------------------
void c_Script::SceneShift()
{
	int judge = GetParamN();
	s_scene->SetChange( (bool)judge );
	
}

//--------------------------------------------------------------------------------
//	引数: なし
//	機能: カメラ座標セット
//--------------------------------------------------------------------------------
void c_Script::SetCameraPos()
{
	Vector3 pos;
	pos.x = GetParamFloat();
	pos.y = GetParamFloat();
	pos.z = GetParamFloat();
	s_scene->SetCameraPos( pos );
}

//--------------------------------------------------------------------------------
//	引数: なし
//	機能: カメラターゲットセット
//--------------------------------------------------------------------------------
void c_Script::SetCameraTarget()
{
	Vector3 pos;
	pos.x = GetParamFloat();
	pos.y = GetParamFloat();
	pos.z = GetParamFloat();
	s_scene->SetCameraTarget( pos );
}

//--------------------------------------------------------------------------------
//	引数: なし
//	機能: カメラターゲットをプレイヤーにセット
//--------------------------------------------------------------------------------
void c_Script::SetCameraTargetToPlayer()
{
	s_scene->SetCameraTargetToPlayer();
}

//--------------------------------------------------------------------------------
//	引数: なし
//	機能: カメラモード切替
//--------------------------------------------------------------------------------
void c_Script::SetCameraMode()
{
	int mode = GetParamN();	
	s_scene->ChangeCamera( mode );
}

//--------------------------------------------------------------------------------
//	引数: なし
//	機能: 焦点距離設定
//--------------------------------------------------------------------------------
void c_Script::SetFocusDist()
{
	float dist = GetParamFloat();	
	s_scene->ChangeFocusDist( dist );
}

//--------------------------------------------------------------------------------
//	引数: なし
//	機能: 焦点範囲設定
//--------------------------------------------------------------------------------
void c_Script::SetFocusRange()
{
	float range = GetParamFloat();	
	s_scene->ChangeFocusRange( range );
}

//*****************************************************************************************************************************
//
//		スクリプトデコード
//
//*****************************************************************************************************************************

BOOL	c_Script::DEC_Main( void )
{
	char	work[256];
	BOOL	ret = FALSE;

	for(;;){
		if( !SearchTop() ) return FALSE;
		GetParamS(work);
		//	コマンド
		if( lstrcmp( work, "END" ) == 0 ){ EventEnd(); return TRUE; }
		else if( lstrcmp( work, "MSG" ) == 0 ){ SetMSG(); return TRUE; }
		else if( lstrcmp( work, "WAIT" ) == 0 ){ EventWait(); return TRUE; }
		else if( lstrcmp( work, "WALK" ) == 0 ){ EventWalk(); return TRUE; }
		else if( lstrcmp( work, "MOTION" ) == 0 ){ EventMotion(); return TRUE; }
		else if( lstrcmp( work, "STOP" ) == 0 ){ EventStop(); return TRUE; }
		else if( lstrcmp( work, "SHIFT" ) == 0 ){ SceneShift(); return TRUE; }
		else if( lstrcmp( work, "CPOS" ) == 0 ){ SetCameraPos(); return TRUE; }
		else if( lstrcmp( work, "CTARGET" ) == 0 ){ SetCameraTarget(); return TRUE; }
		else if( lstrcmp( work, "CSETP" ) == 0 ){ SetCameraTargetToPlayer(); return TRUE; }
		else if( lstrcmp( work, "CMODE" ) == 0 ){ SetCameraMode(); return TRUE; }
		else if( lstrcmp( work, "FOCUSD" ) == 0 ){ SetFocusDist(); return TRUE; }
		else if( lstrcmp( work, "FOCUSR" ) == 0 ){ SetFocusRange(); return TRUE; }
	}
	return TRUE;
}


BOOL	c_Script::Decipherment( LPSTR Command )
{
	char	work[256];
	BOOL	ret = FALSE;

	for(;;){
		if( !SearchTop() ) return FALSE;
		GetParamS(work);

		//	終了
		if( lstrcmp( work, "END" ) == 0 ){ ScriptIP-=5;; return TRUE; }
		
		//	コマンド取得
		else if( lstrcmp( work, Command ) == 0 ){
			//	敵座標
			if( lstrcmp( Command, "EPOS" ) == 0 ){
				Pos.x = GetParamFloat();
				Pos.y = GetParamFloat();
				Pos.z = GetParamFloat();
				return TRUE;
			}
		}
	}

	return FALSE;
}

//----------------------------------------------------------------------------------------------------------
//		座標取得
//----------------------------------------------------------------------------------------------------------
Vector3	c_Script::GetPos()
{
	this->Decipherment( "EPOS" );


	return Pos;
}


