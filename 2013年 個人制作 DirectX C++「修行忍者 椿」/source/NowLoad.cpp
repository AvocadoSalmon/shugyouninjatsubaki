#include	"iextreme.h"
#include	"system/system.h"

#include	"nowLoad.h"
#include	"system/Framework.h"


cNowLoad::cNowLoad() : cThreadObject(){
	_obj = new iex2DObj( FILE_PATH_LOAD_BACK );
}

cNowLoad::~cNowLoad(){
	if( _obj != NULL ){
		delete _obj;
		_obj = NULL;
	}
}

void cNowLoad::Exec(){
	if(!CheckFrameRate())return;
}

void cNowLoad::Render(){
	if( _obj != NULL ){
		//_obj->Render( 412,648,456,48, 0,0,456,48 );
		_obj->Render(shader,"copy");
	}
}
