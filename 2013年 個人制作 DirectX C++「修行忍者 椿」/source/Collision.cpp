#include	"iextreme.h"
#include	"system/system.h"
#include	"Collision.h"
#include	"Player.h"
#include	"Enemy.h"

//**************************************************************************************//
//																						//
//																						//
//																						//
//**************************************************************************************//


//**************************************************************************************//
//																						//
//		コンストラクタ・デストラクタ													//
//																						//
//**************************************************************************************//
c_Collision::c_Collision()
{
	m_CollisionModel = NULL;
}

c_Collision::~c_Collision()
{
	DEL( m_CollisionModel );
}


//	壁判定モデル読み込み
void c_Collision::Load(char *name)
{
	if( m_CollisionModel != NULL )delete m_CollisionModel;
	m_CollisionModel = new iexMesh( name );
	m_CollisionModel->SetScale( COLLISION_MODEL_SIZE );
}


//**************************************************************************************//
//																						//
//		更新																			//
//																						//
//**************************************************************************************//


//**************************************************************************************//
//																						//
//																						//
//																						//
//**************************************************************************************//

//--------------------------------------------------------------------------------
//	引数: 判定するキャラの現在位置
//	機能: 地面にレイを撃って現在のY座標を返す
//--------------------------------------------------------------------------------
float c_Collision::FloorHeight( Vector3& pos )
{
	Vector3 p = pos + COLLISION_FLOOR_BIAS;	//	少し上
	Vector3	v( 0, -1, 0 );					//	レイ発射方向
	float	d = COLLISION_FLOOR_RAY_DIST;	//	レイピック判定距離
	Vector3	out;

	m_CollisionModel->RayPick( &out, &p, &v, &d );

	return out.y;
}

//--------------------------------------------------------------------------------
//	引数: 判定するキャラの現在位置,判定するキャラの向いている方向
//	機能: 壁とのキョリを返す
//--------------------------------------------------------------------------------
float c_Collision::Wall( Vector3& pos, Vector3& v )
{
	Vector3	out;
	float	d = COLLISION_FLOOR_RAY_DIST;	//	レイピック判定距離

	Vector3	p = pos + Vector3( 0 ,0.4f ,0 );

	if( m_CollisionModel->RayPick( &out, &p, &v, &d ) < 0 ) return COLLISION_ERROR_DIST;	//	エラーなら当たってない距離を返す

	Vector3 vec = out - pos;
	vec.y = 0;
	d = vec.Length();

	return d;
}

//--------------------------------------------------------------------------------
//	引数: 判定するキャラの現在位置,移動量
//	機能: キャラクタと壁の当たり判定をして新しい位置を返す
//--------------------------------------------------------------------------------
Vector3	c_Collision::CheckMove( Vector3& pos, Vector3& move )
{
	Vector3	p = pos;
	//	方向ベクトル補正
	Vector3	m = move;
	m.y = 0;
	float	md = move.Length();
	//	移動ベクトル正規化
	m.Normalize();

	//	壁判定
	Vector3 v = m;
	float d = Wall( p, v );
	if( d >=0 && d < md ){
		//	法線正規化
		v.Normalize();
		float cc = (m.x*v.x + m.z*v.z);

		d = (md-d) * 1.05f;		//	1.05 = 調整値
		m = move - v*cc*d;
		md = m.Length();
		//	進行方向再チェック
		v = m;
		v.Normalize();

		p = pos;
		d = Wall( p, v );
		if( d < .0f || d > md ){
			//	移動
			p += m;
		}
	} else {
		p += move;
	}
	return p;
}

//--------------------------------------------------------------------------------
//	引数: 判定するキャラクタA(固定), 判定するキャラクタB(移動)
//	機能: キャラクタ同士の位置判定をして新しい位置を返す
//--------------------------------------------------------------------------------
Vector3 c_Collision::CheckPos(BaseOBJ *actorA, BaseOBJ *actorB)
{
	return CheckPos( actorA->GetPos(),actorA->GetSize(),actorB );
}

//--------------------------------------------------------------------------------
//	引数: 判定するキャラクタA(固定)の位置,判定サイズ, 判定するキャラクタB(移動)
//	機能: キャラクタ同士の位置判定をして新しい位置を返す
//--------------------------------------------------------------------------------
Vector3 c_Collision::CheckPos(Vector3 &PosA, float size, BaseOBJ *actorB)
{
	Vector3 v = actorB->GetPos() - PosA;
	float	d = v.Length();
	v.Normalize();

	if( d < size + actorB->GetSize() ){
		return PosA + v * ( size + actorB->GetSize() );
	}
	
	return actorB->GetPos();
}