//*****************************************************************************************************************************
//
//		メインシーン
//
//*****************************************************************************************************************************
#ifndef _SCENE_MAIN_H_
#define _SCENE_MAIN_H_


#include	"Stage.h"
#include	"BaseObject.h"
#include	"Stage.h"
#include	"Player.h"
#include	"Camera.h"
#include	"Enemy.h"
#include	"Collision.h"
#include	"Fade.h"
#include	"SaveLoad.h"
#include	"Script.h"
#include	"Text.h"


/* マクロ定義 */
#define WATER_FLOW_SPEED		0.0008f								//	流れる水の速さ
#define MAIN_PLAYER_SIZE		0.009f								//	プレイヤーサイズ

#define MAIN_LIGHT_DIRECTION	Vector3( 0,-1,1 )					//	ライト方向
#define MAIN_LIGHT_COLOR		0.75f, 0.6f, 0.6f					//	ライト色
#define MAIN_AMBIENT_COLOR		0x808080							//	アンビエント色

#define MIAN_PARTICLE_MAX		1024								//	パーティクル最大数
#define MIAN_SKY_COLOR			Vector3( 0.6f, 0.6f, 0.6f )			//	空の色
#define MAIN_GROUND_COLOR		Vector3( 0.8f, 0.8f, 0.8f )			//	地面の色

#define	MAIN_TEXT_X			250										//	ゲーム開始時表示画像のX座標
#define	MAIN_TEXT_Y			300										//	ゲーム開始時表示画像のY座標
#define	MAIN_TEXT_SIZE		32										//	ゲーム開始時表示画像のサイズ
#define	MAIN_TEXT_WIDTH		768										//	ゲーム開始時表示画像の幅
#define	MAIN_TEXT_HEIGHT	128										//	ゲーム開始時表示画像の高さ
#define	MAIN_TIME_COUNT		120										//	ゲーム開始時表示画像の表示時間(フレーム)

class	sceneMain : public Scene
{
private:
	iexView*	view;
	c_StageManager*	stage;
	EnemyManager* enemy_manager;
	c_Fade*		fade;
	iex2DObj *EnvTex;		//	環境マップ用テクスチャ
	iex2DObj *flare;		//	レンズフレア用テクスチャ
	iex2DObj *screen;		//	スクリーンフィルタ
	Surface* pBackBuffer;	//	フレームバッファ
	iex2DObj *Text;			//	表示テキスト
	c_Script  scr;			//	スクリプト
	char message[256];
	int count;
	iex2DObj*	HDR;		//	擬似ＨＤＲ
	iex2DObj*	seaTexture;	//	海用テクスチャ
	c_String*	Font;		//	フォント
	LPDSSTREAM m_bgm;		//	BGM
	iex2DObj*	ShadowTex;	//	シャドウテクスチャ
	Surface*	ShadowZ;	//	影Zバッファ
	iex2DObj*	Depth;		//	被写界深度
public:
	~sceneMain();
	//	初期化
	bool Initialize();
	//	更新・描画
	void Update();	//	更新
	void Render();	//	描画

	void LenzFlare();
	void StartText();

	void RenderHDR();		//	HDR描画
	void InitShadow();		//	影初期化
	void RenderShadow();	//	影描画
	void RenderDepth();		//	被写界深度描画


	void ChageStage( char* name,Vector3& pos );
	void ChageStage( int n );

	void SetMessage( char *msg );
	void SetCount( int s_count );

	//	デバッグ用
	void Debug_Init();

};


#endif	//	_SCENE_MAIN_H_

