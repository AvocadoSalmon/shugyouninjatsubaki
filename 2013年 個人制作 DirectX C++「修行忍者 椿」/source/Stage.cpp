#include	"iextreme.h"
#include	"system/system.h"

#include	"Stage.h"
#include	"sceneMain.h"

//**************************************************************************************//
//																						//
//	c_Stage1クラス																		//
//																						//
//**************************************************************************************//

//**************************************************************************************																						//
//		初期化																																								//
//**************************************************************************************
void c_Stage1::Load()
{
	//	背景読み込み
	if( !m_lpMesh )m_lpMesh = new iexMesh( FILE_PATH_BG_STAGE01 ); 
	//	海読み込み
	if( !m_sea )m_sea = new iexMesh( FILE_PATH_BG_STAGE01_SEA );
	//	雲読み込み
	if( !m_cloud )m_cloud = new iexMesh( FILE_PATH_BG_STAGE01_CLOUD );	
	//	空読み込み
	if( !m_sky )m_sky = new c_Sky();

}

void c_Stage1::Load( char *name )
{
	//	背景読み込み
	if( !m_lpMesh )m_lpMesh = new iexMesh( name ); 
	//	海読み込み
	if( !m_sea )m_sea = new iexMesh( FILE_PATH_BG_STAGE01_SEA );
	//	雲読み込み
	if( !m_cloud )m_cloud = new iexMesh( FILE_PATH_BG_STAGE01_CLOUD );	
	//	空読み込み
	if( !m_sky )m_sky = new c_Sky();
}

c_Stage1::c_Stage1()
{
	m_lpMesh	= NULL;
	m_sea		= NULL;
	m_cloud		= NULL;
	m_sky		= NULL;

	CAMERA.SetTarget( STAGE1_CAMERA_INITIAL_TARGET );	//	プレイヤーのやや上
	CAMERA.SetPos( STAGE1_CAMERA_INITIAL_POS );

}

c_Stage1::~c_Stage1()
{
	DEL( m_lpMesh );
	DEL( m_sea );
	DEL( m_cloud );
	DEL( m_sky );
}

//**************************************************************************************																						//
//		更新																																								//
//**************************************************************************************
void c_Stage1::Update()
{
	//	スケール設定
	m_cloud->SetScale( STAGE_SIZE );
	m_sea->SetScale( STAGE_SIZE );
	m_lpMesh->SetScale( STAGE_SIZE );

	//	カメラ注視点はプレイヤーの少し先
	const float dist = 1.0f;							//	カメラを離す距離
	Vector3 vec = player->GetPos() - CAMERA.GetPos();	//	カメラからプレイヤーへのベクトル
	vec.Normalize();									//	正規化
	Vector3 target( player->GetPos().x + vec.x*dist,player->GetPos().y + 1.5f,player->GetPos().z + vec.z*dist );
	CAMERA.SetTarget( target );							//	カメラターゲット設定
	CAMERA.Update( player );
}

//**************************************************************************************																						//
//		描画																																								//
//**************************************************************************************
void c_Stage1::Render( char* name )
{
	m_lpMesh->Update();
	m_lpMesh->Render( shader,name );

	m_sea->Update();
	m_sea->Render( shader,name );
}

void c_Stage1::Render()
{	
	m_cloud->Render( shader,"copyNL" );		//	空描画
	m_sky->Render();						//	雲描画
	m_sea->Render( shader,"sea"  );			//	海描画

	m_lpMesh->Render( shader,"full_s" );	//	背景描画
	
}

//**************************************************************************************																						//
//		処理																																								//
//**************************************************************************************



//**************************************************************************************//
//																						//
//	c_Stage2クラス																		//
//																						//
//**************************************************************************************//

//**************************************************************************************																						//
//		初期化																																								//
//**************************************************************************************
void c_Stage2::Load()
{
	//m_lpMesh = new iexMesh( filename ); 

	//	空
	m_sky = new c_Sky();

}

void c_Stage2::Load( char *name )
{
	m_lpMesh = new iexMesh( name ); 

	//	空
	m_sky = new c_Sky();
}

c_Stage2::c_Stage2()
{
	//	空
	m_sky = NULL;
}

c_Stage2::~c_Stage2()
{
	DEL( m_lpMesh );
	DEL( m_sky );
}

//**************************************************************************************																						//
//		更新																																								//
//**************************************************************************************
void c_Stage2::Update()
{
	//	スケール設定
	m_lpMesh->SetScale( STAGE_SIZE );
}

//**************************************************************************************																						//
//		描画																																								//
//**************************************************************************************
void c_Stage2::Render( char* name )
{
	m_lpMesh->Update();
	m_lpMesh->Render( shader,name );

	m_sky->Render();	//	雲描画
}

void c_Stage2::Render()
{

}

//**************************************************************************************																						//
//		処理																																								//
//**************************************************************************************