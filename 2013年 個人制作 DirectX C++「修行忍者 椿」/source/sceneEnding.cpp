#include	"iextreme.h"
#include	"system/system.h"

#include	"sceneEnding.h"

//**************************************************************************************//
//																						//
//																						//
//																						//
//**************************************************************************************//



//**************************************************************************************//
//																						//
//		初期化																			//
//																						//
//**************************************************************************************//
bool sceneEnding::Initialize()
{

	//	背景画像読み込み
	m_ending = new iex2DObj( FILE_PATH_TITLE_FONT );

	//	2D画像読み込み
	lpClear = new iex2DObj( FILE_PATH_TITLE_FONT );

	m_count = ENDING_COUNT;

	fade = new c_Fade();
	fade->SetMode( MODE_FADE_INIT );

	//	BGM読み込み
	m_bgm = IEX_PlayStreamSound( FILE_PATH_BGM_ENDING );

	return true;
}

//-----------------------------------------------------
//	解放
//-----------------------------------------------------
sceneEnding::~sceneEnding()
{
	DEL( m_ending );
	DEL( fade );
	DEL( lpClear );
	IEX_StopStreamSound( m_bgm );	//	BGMを停止
}

//**************************************************************************************//
//																						//
//		更新																			//
//																						//
//**************************************************************************************//
void sceneEnding::Update()
{
	//	平行光・アンビエント設定
	iexLight::DirLight( shader, NULL, LIGHT_DIRECTION, LIGHT_COLOR );
	iexLight::SetAmbient( ENDING_AMBIENT_COLOR );

	m_count--;

	fade->Update();

	if( m_count <= 0 ){
		m_count = 0;
		fade->SetMode( MODE_FADE_ACT );		//	フェードアウト
		ChangeScene( MODE_TITLE );			//	タイトルへ
	}
}

//**************************************************************************************//
//																						//
//		描画																			//
//																						//
//**************************************************************************************//
void sceneEnding::Render()
{	
	//	各描画
	m_ending->Render( ENDING_BG_PARAM );							//	背景画像

	
	lpClear->Render( ENDING_CLEAR_PARAM );							//	CLEAR画像

	fade->Render();
}


//**************************************************************************************//
//																						//
//		処理																			//
//																						//
//**************************************************************************************//