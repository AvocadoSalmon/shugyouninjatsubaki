//**************************************************************************************************
//																									
//		基本シェーダー		
//
//**************************************************************************************************

//------------------------------------------------------
//		環境関連
//------------------------------------------------------
float4x4 TransMatrix;	//	変換行列
float4x4 matView;		//	変換行列
float4x4 Projection;	//	変換行列

float3	ViewPos;

//------------------------------------------------------
//		テクスチャサンプラー	
//------------------------------------------------------
texture Texture;	//	デカールテクスチャ
sampler DecaleSamp = sampler_state
{
    Texture = <Texture>;
    MinFilter = LINEAR;
    MagFilter = LINEAR;
    MipFilter = NONE;

    AddressU = Wrap;
    AddressV = Wrap;
};

texture NormalMap;	//	法線マップテクスチャ
sampler NormalSamp = sampler_state
{
    Texture = <NormalMap>;
    MinFilter = LINEAR;
    MagFilter = LINEAR;
    MipFilter = NONE;

    AddressU = Wrap;
    AddressV = Wrap;
};

texture SpecularMap;	//	スペキュラマップテクスチャ
sampler SpecularSamp = sampler_state
{
    Texture = <SpecularMap>;
    MinFilter = LINEAR;
    MagFilter = LINEAR;
    MipFilter = NONE;

    AddressU = Wrap;
    AddressV = Wrap;
};

texture HeightMap;		//	高さマップテクスチャ
sampler HeightSamp = sampler_state
{
    Texture = <HeightMap>;
    MinFilter = LINEAR;
    MagFilter = LINEAR;
    MipFilter = NONE;

    AddressU = Wrap;
    AddressV = Wrap;
};

//**************************************************************************************************
//		頂点フォーマット
//**************************************************************************************************
struct VS_BASIC
{
    float4 Pos    : POSITION;
    float4 Color  : COLOR0;
    float2 Tex	  : TEXCOORD0;
};

struct VS_OUTPUT
{
    float4 Pos		: POSITION;
    float4 Color	: COLOR0;
    float2 Tex		: TEXCOORD0;
    float3 Normal	: TEXCOORD1;

    float4 Ambient	: COLOR1;
	float4 light	: COLOR2;
	float3 vLight	: TEXCOORD2;
	float3 vE		: TEXCOORD3;
	float  specular	: TEXCOORD4;
};

struct VS_INPUTL
{
    float4 Pos    : POSITION;
    float3 Normal : NORMAL;
    float4 Color  : COLOR0;
    float2 Tex	  : TEXCOORD0;
};

struct VS_OUTPUT_FOG
{
    float4 Pos		: POSITION;
    float4 Color	: COLOR0;
    float2 Tex		: TEXCOORD0;

	float  Fog		: TEXCOORD1;
	
    float4 GlobalPos    : TEXCOORD2;	//	グローバル座標
    float3 GlobalNormal : TEXCOORD3;	//	グローバル法線
    
    float4 light	: COLOR1;
	float3 vLight	: TEXCOORD4;
	float3 vE		: TEXCOORD5;
};

//**************************************************************************************************
//
//		ライティング
//
//**************************************************************************************************

//**************************************************************************************************
//	半球ライティング
//**************************************************************************************************
float3 SkyColor = { 0.48f, 0.5f, 0.5f };
float3 GroundColor = { 0.1f, 0.1f, 0.1f };

inline float4 HemiLight( float3 normal )
{
	float4 color;
	float rate = (normal.y*0.5f) + 0.5f;
	color.rgb = SkyColor * rate;
	color.rgb += GroundColor * (1-rate);
	color.a = 1.0f;

	return color;
}

//**************************************************************************************************
//	平行光
//**************************************************************************************************
float3 LightDir = { 1.0f, -1.0f, 0.0f };
float3 DirLightColor = { 0.6f, 0.6f, 0.6f };

inline float3 DirLight( float3 dir, float3 normal )
{
	float3 light;
	float rate = max( 0.0f, dot( -dir, normal ) );
	light = DirLightColor * rate;
	
	return light;
}

//**************************************************************************************************
//	スペキュラ
//**************************************************************************************************
inline float Specular(float3 pos, float3 normal)
{
    float   sp;

    float3	H = normalize(ViewPos - pos);
    H = normalize(H - LightDir);
    
	sp = dot(normal, H);
	sp = max( 0, sp );
	sp = pow(sp, 30);

    return sp;
}

//------------------------------------------------------
//		フォグ関連
//------------------------------------------------------
float	FogNear	= 80.0f;
float	FogFar  = 3000.0f;
float3	FogColor = { 0.1f, 0.1f, 0.1f };

//**************************************************************************************************
//
//		基本シェーダー
//
//**************************************************************************************************

//------------------------------------------------------
//	頂点シェーダー
//------------------------------------------------------
VS_BASIC VS_Basic( VS_INPUTL In )
{
	VS_BASIC Out = (VS_BASIC)0;

	float3 P = mul(In.Pos, TransMatrix);

	float3x3	mat = TransMatrix;
	float3 N = mul(In.Normal, mat);
	N = normalize(N);

	Out.Pos   = mul(In.Pos, Projection);
	Out.Color.rgb = DirLight( LightDir, N ) + HemiLight( N );
	Out.Color.a = 1.0f;
	Out.Tex   = In.Tex;

	return Out;
}

//------------------------------------------------------
//	頂点カラー付シェーダー
//------------------------------------------------------
VS_BASIC VS_Basic2( VS_INPUTL In )
{
    VS_BASIC Out = (VS_BASIC)0;
	
	float3 P = mul( In.Pos, TransMatrix );

	float3x3	mat = TransMatrix;
	float3 N = mul(In.Normal, mat);
	N = normalize(N);

	Out.Pos   = mul(In.Pos, Projection);
	Out.Color.rgb = (DirLight( LightDir, N ) + HemiLight( N ));
	Out.Color.a = In.Color.a;
	Out.Tex   = In.Tex;

    return Out;
}


//------------------------------------------------------
//	頂点シェーダー(平行光なし)
//------------------------------------------------------
VS_BASIC VS_Basic3( VS_INPUTL In )
{
	VS_BASIC Out = (VS_BASIC)0;

	float3 P = mul(In.Pos, TransMatrix);

	float3x3	mat = TransMatrix;
	float3 N = mul(In.Normal, mat);
	N = normalize(N);

	Out.Pos   = mul(In.Pos, Projection);
	Out.Color.rgb = 1;
	Out.Color.a = 1.0f;
	Out.Tex   = In.Tex;

	return Out;
}

//------------------------------------------------------
//	頂点カラー付シェーダー(UVアニメ)
//------------------------------------------------------
float SU,SV,UValpha = 1.0f;

VS_BASIC VS_UVanime( VS_INPUTL In )
{
    VS_BASIC Out = (VS_BASIC)0;
	
	float3 P = mul(In.Pos,    TransMatrix);			//	頂点座標を変換
	float3x3 m = TransMatrix;						//	3×3のベクトル変換行列
	float3 N = mul(In.Normal, m);					//	ポリゴンの法線を変換
	N = normalize(N);								//	正規化

	Out.Pos   = mul(In.Pos, Projection);							//	頂点座標を投影変換
	Out.Color.rgb = DirLight( LightDir, N ) + HemiLight( N );		//	平行光合成+半球ライティング
	Out.Color.a = In.Color.a * UValpha;								//	アルファ値(頂点カラー)
	Out.Tex   = In.Tex;												//	テクスチャ情報を出力	
	Out.Tex.x	+=	SU;												//	U方向にアニメーション
	Out.Tex.y	+=	SV;												//	V方向にアニメーション
	
    return Out;
}

//------------------------------------------------------
//	ピクセルシェーダー	
//------------------------------------------------------
float4 PS_Basic( VS_BASIC In) : COLOR
{   
	float4	OUT;
	//	ピクセル色決定
	OUT = In.Color * tex2D( DecaleSamp, In.Tex );

	return OUT;
}

//------------------------------------------------------
//	テクニック
//------------------------------------------------------
technique copy
{
    pass P0
    {
		AlphaBlendEnable = true;
		BlendOp          = Add;
		SrcBlend         = SrcAlpha;
		DestBlend        = InvSrcAlpha;
		ZWriteEnable     = true;

		VertexShader = compile vs_3_0 VS_Basic();
		PixelShader  = compile ps_3_0 PS_Basic();
    }
}

technique copyNL
{
    pass P0
    {
		AlphaBlendEnable = true;
		BlendOp          = Add;
		SrcBlend         = SrcAlpha;
		DestBlend        = InvSrcAlpha;
		ZWriteEnable     = true;

		VertexShader = compile vs_3_0 VS_Basic3();
		PixelShader  = compile ps_3_0 PS_Basic();
    }
}

technique lcopy
{
    pass P0
    {
		AlphaBlendEnable = true;
		BlendOp          = Add;
		SrcBlend         = SrcAlpha;
		DestBlend        = InvSrcAlpha;
		ZWriteEnable     = true;

		VertexShader = compile vs_2_0 VS_Basic2();
		PixelShader  = compile ps_2_0 PS_Basic();
    }
}

technique add
{
    pass P0
    {
		AlphaBlendEnable = true;
		BlendOp          = Add;
		SrcBlend         = SrcAlpha;
		DestBlend        = One;
		ZWriteEnable     = true;

		VertexShader = compile vs_3_0 VS_Basic();
		PixelShader  = compile ps_3_0 PS_Basic();
    }
}

technique add_uv
{
    pass P0
    {
		AlphaBlendEnable = true;
		BlendOp          = Add;
		SrcBlend         = SrcAlpha;
		DestBlend        = One;
		ZWriteEnable     = false;

		VertexShader = compile vs_2_0 VS_UVanime();
		PixelShader  = compile ps_2_0 PS_Basic();
    }
}

//**************************************************************************************************
//
//		フルエフェクト
//
//**************************************************************************************************
//------------------------------------------------------
//	頂点シェーダ
//------------------------------------------------------
VS_OUTPUT VS_FullFX( VS_INPUTL In )
{
    VS_OUTPUT Out = (VS_OUTPUT)0;
	
	float4 P = mul(In.Pos,    TransMatrix);

	float3x3	mat = TransMatrix;
	float3 N = mul(In.Normal, mat);
	N = normalize(N);
	Out.Normal = N;
	Out.Ambient.rgb = HemiLight(N);

	Out.Pos   = mul(In.Pos, Projection);
	Out.Color = 1.0f;
	Out.Tex   = In.Tex;

	//	ライトベクトル補正
	float3	vx;
	float3	vy = { 0.0f, -1, 0.01f };

	vx = cross( N, vy );
	vx = normalize( vx );
	
	vy = cross( vx, N );
	vy = normalize( vy );

	//	ライトベクトル補正
	Out.vLight.x = dot(vx, LightDir);
	Out.vLight.y = dot(vy, LightDir);
	Out.vLight.z = dot(N, LightDir);
	Out.vLight = normalize( Out.vLight );

	// 視線ベクトル補正
	float3 E = (P - ViewPos);   // 視線ベクトル
	Out.vE.x = -dot(vx, E);
	Out.vE.y = dot(vy, E);
	Out.vE.z = dot(N, E);
	Out.vE = normalize( Out.vE );

	Out.light = 0;//PointLight( P, N );

	//	フォグ計算
	Out.Ambient.a = (FogFar-Out.Pos.z) / (FogFar-FogNear);
	Out.Ambient.a = saturate(Out.Ambient.a);

	return Out;
}

//------------------------------------------------------
//	頂点カラー付シェーダ
//------------------------------------------------------
VS_OUTPUT VS_FullFX2( VS_INPUTL In )
{
	VS_OUTPUT Out = (VS_OUTPUT)0;

	float4 P = mul(In.Pos, TransMatrix);

	float3x3	mat = TransMatrix;
	float3 N = mul(In.Normal, mat);
	N = normalize(N);
	Out.Normal = N;

	Out.Ambient.rgb = HemiLight(N);

	Out.Pos   = mul(In.Pos, Projection);
	Out.Color = In.Color;
	Out.Tex   = In.Tex;

	//	ライトベクトル補正
	float3	vx;
	float3	vy = { 0, 1, 0.01f };

	vx = cross( N, vy );
	vx = normalize( vx );

	vy = cross( N, vx );
	vy = normalize( vy );

	//	ライトベクトル補正
	Out.vLight.x = dot(vx, LightDir);
	Out.vLight.y = dot(vy, LightDir);
	Out.vLight.z = dot(N, LightDir);

	// 視線ベクトル補正
	float3 E = (P - ViewPos);   // 視線ベクトル
	Out.vE.x = dot(vx, E);
	Out.vE.y = dot(vy, E);
	Out.vE.z = dot(N, E);

	Out.light = 0;//PointLight( P, N );

	//	フォグ計算
	Out.Ambient.a = (FogFar-Out.Pos.z) / (FogFar-FogNear);
	Out.Ambient.a = saturate(Out.Ambient.a);

	return Out;
}

//------------------------------------------------------
//		ピクセルシェーダー	
//------------------------------------------------------
float4 PS_FullFX( VS_OUTPUT In) : COLOR
{   
	float4	OUT;
	float2 Tex = In.Tex;

	//	パララックスディスプレースメント
	float h = tex2D( HeightSamp, Tex ).r-0.5f;// 高さマップのサンプリング
	float3 E = normalize(In.vE);
	In.vLight = normalize(In.vLight);
	
	Tex -= 0.05f * h * E.xy;
	float3 N = tex2D( NormalSamp, Tex ).xyz * 2.0f - 1.0f;

	//	ライト計算
	float3 light = DirLight(In.vLight, N);
	//	ピクセル色決定
	OUT = In.Color * tex2D( DecaleSamp, Tex );
	OUT.rgb = (OUT.rgb*(light+In.Ambient.rgb));


	//	スペキュラ
	float3  R = normalize( reflect( E, N ) );
	OUT.rgb += pow( max( 0, dot(R, -In.vLight) ), 10 ) * tex2D( SpecularSamp, Tex );
	//	フォグ採用
	OUT.rgb = (OUT.rgb * In.Ambient.a) + (FogColor * (1-In.Ambient.a));

	return OUT;
}

//------------------------------------------------------
//		ピクセルシェーダー	
//------------------------------------------------------
float4 PS_FullFX2( VS_OUTPUT In) : COLOR
{   
	float4	OUT;
	float2 Tex = In.Tex;

	//	パララックスディスプレースメント
	float h = tex2D( HeightSamp, Tex ).r-0.5f;// 高さマップのサンプリング
	float3 E = normalize(In.vE);
	In.vLight = normalize(In.vLight);
	
	float3 N = tex2D( NormalSamp, Tex ).xyz * 2.0f - 1.0f;

	//	ライト計算
	float3 light = DirLight(In.vLight, N);
	//	ピクセル色決定
	OUT = In.Color * tex2D( DecaleSamp, Tex );
	OUT.rgb = (OUT.rgb*(light+In.Ambient.rgb));


	//	スペキュラ
	float3  R = normalize( reflect( E, N ) );
	OUT.rgb += pow( max( 0, dot(R, -In.vLight) ), 10 ) * tex2D( SpecularSamp, Tex );
	//	フォグ採用
	OUT.rgb = (OUT.rgb * In.Ambient.a) + (FogColor * (1-In.Ambient.a));

	return OUT;
}

//------------------------------------------------------
//		合成なし	
//------------------------------------------------------
technique copy_fx
{
    pass P0
    {
		AlphaBlendEnable = true;
		BlendOp          = Add;
		SrcBlend         = SrcAlpha;
		DestBlend        = InvSrcAlpha;
		ZWriteEnable     = true;

		VertexShader = compile vs_3_0 VS_FullFX();
		PixelShader  = compile ps_3_0 PS_FullFX();
    }
}

technique lcopy_fx
{
    pass P0
    {
		AlphaBlendEnable = true;
		BlendOp          = Add;
		SrcBlend         = SrcAlpha;
		DestBlend        = InvSrcAlpha;
		ZWriteEnable     = true;
		VertexShader = compile vs_3_0 VS_FullFX2();
		PixelShader  = compile ps_3_0 PS_FullFX();
    }
}

technique copy_fx2
{
    pass P0
    {
		AlphaBlendEnable = true;
		BlendOp          = Add;
		SrcBlend         = SrcAlpha;
		DestBlend        = InvSrcAlpha;
		ZWriteEnable     = true;

		VertexShader = compile vs_3_0 VS_FullFX();
		PixelShader  = compile ps_3_0 PS_FullFX2();
    }
}

technique lcopy_fx2
{
    pass P0
    {
		AlphaBlendEnable = true;
		BlendOp          = Add;
		SrcBlend         = SrcAlpha;
		DestBlend        = InvSrcAlpha;
		ZWriteEnable     = true;
		VertexShader = compile vs_3_0 VS_FullFX2();
		PixelShader  = compile ps_3_0 PS_FullFX2();
    }
}

//**************************************************************************************************
//
//		環境マップ
//
//**************************************************************************************************

float EnvParam = 0.5f;	//	合成の割合

//**************************************************************************************************
//	環境マップ用
//**************************************************************************************************
texture EnvMap;		//	環境テクスチャ
sampler EnvSamp = sampler_state
{
    Texture = <EnvMap>;
    MinFilter = LINEAR;
    MagFilter = LINEAR;
    MipFilter = NONE;

    AddressU = Wrap;
    AddressV = Wrap;
};

inline float4 Environment( float3 normal )
{
	float4	color;
	
	float2	uv;
	uv.x = normal.x*0.5f + 0.5f;
	uv.y = -normal.y*0.5f + 0.5f;

	color = tex2D( EnvSamp, uv );
	return color;
}

//------------------------------------------------------
//		頂点シェーダー	
//------------------------------------------------------
 VS_OUTPUT_FOG VS_Lighting2( VS_INPUTL In )
{
   VS_OUTPUT_FOG Out = (VS_OUTPUT_FOG)0;

    Out.Pos = mul(In.Pos, Projection);
	Out.Tex = In.Tex;
	Out.Color.a = 1;

	//	法線変換	
	float3x3 mat = TransMatrix;
	float3 N = mul( In.Normal, mat );
	N = normalize(N);	

	//	半球ライティング適用
	Out.Color.rgb = HemiLight(N);
	//	平行光適用
	Out.Color.rgb += DirLight( LightDir,N );

	//	グローバルポジション&法線
	Out.GlobalPos = mul(In.Pos, TransMatrix );
	float3x3 m = (float3x3)TransMatrix;
	Out.GlobalNormal = mul(In.Normal, m );	
	
	//	フォグ
	Out.Fog = ( FogFar - Out.Pos.z ) / ( FogFar-FogNear );
	Out.Fog = saturate( Out.Fog );
	
    return Out;
}

//------------------------------------------------------
//		環境マップ用ピクセルシェーダー	
//------------------------------------------------------
float4 PS_Env( VS_OUTPUT_FOG In) : COLOR
{   
	float4	OUT;

	//	ピクセル色決定
	OUT = In.Color * tex2D( DecaleSamp, In.Tex );

	In.GlobalNormal = normalize(In.GlobalNormal);
	
	float4 Env = Environment( In.GlobalNormal );
	OUT = OUT * ( 1 - EnvParam ) + Env * EnvParam;
	
	//	スペキュラ
	OUT.rgb += Specular( In.GlobalPos, In.GlobalNormal );

	//	フォグ採用
	OUT.rgb = (OUT.rgb * In.Fog) + (FogColor * (1-In.Fog));
	
	return OUT;
}

//------------------------------------------------------
//		環境マッピング
//------------------------------------------------------
technique environment
{
    pass P0
    {
		AlphaBlendEnable = true;
		BlendOp          = Add;
		SrcBlend         = SrcAlpha;
		DestBlend        = InvSrcAlpha;
		CullMode         = CCW;
		ZEnable          = true;
		ZWriteEnable     = true;

		VertexShader = compile vs_3_0 VS_Lighting2();
		PixelShader  = compile ps_3_0 PS_Env();
    }
}

//------------------------------------------------------
//		海ピクセルシェーダー	
//------------------------------------------------------
float AdjustSea;	//	流量
texture SeaMap;	//	海法線テクスチャ
sampler SeaSamp = sampler_state
{
	Texture = <SeaMap>;
	MinFilter = LINEAR;
	MagFilter = LINEAR;
	MipFilter = NONE;
	
	AddressU = Wrap;
	AddressV = Wrap;
};

float4 PS_Sea( VS_OUTPUT In) : COLOR
{   
	float4	OUT;
	float2 Tex = In.Tex;

	//	パララックスディスプレースメント
	float h = tex2D( HeightSamp, Tex ).r-0.5f;// 高さマップのサンプリング
	float3 E = normalize(In.vE);
	In.vLight = normalize(In.vLight);
	
	float2 NormalUV = Tex;
	NormalUV.y += AdjustSea;
	float3 N = tex2D( NormalSamp, NormalUV ).xyz * 2.0f - 1.0f;

	//	ライト計算
	In.vLight   = normalize( In.vLight );
	float3 light = DirLight(In.vLight, N);
	
	//	視線反射ベクトル
	float3 R = reflect( -E,N );
	//	スペキュラマップ取得
	float4 sp_tex = tex2D( SpecularSamp,Tex );
	
	//	環境マップ
	float3 env = ( 1 - sp_tex.a ) * Environment( R );
	
	//	スペキュラ
	//OUT.rgb += pow( max( 0,dot(R,In.vLight) ), 30 ) * sp_tex;
	
	//	ピクセル色決定
	OUT = In.Color * tex2D( DecaleSamp, Tex );
	OUT.rgb = (OUT.rgb*(light+In.Ambient.rgb));
	
	//------------------------------------------------------
	//	海処理
	//------------------------------------------------------
	float3 OUT2;
	//	海法線取得(UVを変えて2回合成)
	float2 SeaUV = Tex;
	SeaUV.y += AdjustSea;
	float3 Sea = tex2D( SeaSamp,Tex ).xyz * 2.0f - 1.0f;
	SeaUV.y += AdjustSea;
	SeaUV.x = -SeaUV.x;
	Sea += tex2D( SeaSamp,SeaUV ).xyz * 2.0f - 1.0f;
	Sea = normalize( Sea );
	
	//	海法線→元テクスチャの揺らぎ
	OUT2 = In.Color * tex2D( DecaleSamp,Tex - Sea.xy * 0.04f );
	
	//	反射ベクトルの計算
	float3 R2 = reflect( -E,N+Sea );
	R2 = normalize( R2 );
	
	//	海環境マップの合成
	OUT2 *= Environment( R2 );
	float rate = DirLight( In.vLight,Sea );
	
	//	元画像との合成
	rate = pow( rate,3 ) + 0.5f;
	OUT.rgb = lerp( OUT.rgb,OUT2,rate );
	
	//	スペキュラ
	OUT.rgb += pow( max( 0,dot(R,In.vLight) ), 50 ) * sp_tex;
	
	//	フォグ採用
	OUT.rgb = (OUT.rgb * In.Ambient.a) + (FogColor * (1-In.Ambient.a));

	return OUT;
}

//------------------------------------------------------
//		海シェーダ
//------------------------------------------------------
technique sea
{
    pass P0
    {
		AlphaBlendEnable = true;
		BlendOp          = Add;
		SrcBlend         = SrcAlpha;
		DestBlend        = InvSrcAlpha;
		ZWriteEnable     = true;

		VertexShader = compile vs_3_0 VS_FullFX2();
		PixelShader  = compile ps_3_0 PS_Sea();
    }
}


//********************************************************************
//
//		トゥーンシェーディング		
//
//********************************************************************

//------------------------------------------------------
//		ライトマップ
//------------------------------------------------------
texture LightMap;
sampler LightSamp = sampler_state
{
    Texture = <LightMap>;
    MinFilter = LINEAR;
    MagFilter = LINEAR;
    MipFilter = NONE;

    AddressU = Clamp;
    AddressV = Clamp;
};


struct VS_OUTPUT_TOON
{
    float4 Pos		: POSITION;
    float4 Color	: COLOR0;
    float2 Tex		: TEXCOORD0;
    
    float Level		: TEXCOORD1;
    
};

float OutlineSize = 0.2f;

//------------------------------------------------------
//		アウトライン頂点シェーダー	
//------------------------------------------------------
VS_OUTPUT VS_Outline( VS_INPUTL In )
{
	VS_OUTPUT Out = (VS_OUTPUT)0;
	
	//	法線方向に拡大
	In.Normal = normalize( In.Normal );
	In.Pos.xyz += In.Normal *  OutlineSize;
	
	Out.Pos = mul( In.Pos,Projection );
	Out.Tex = In.Tex;
	
	float3 color = { 0.0f,0.0f,0.0f };
	Out.Color.rgb = color;
	Out.Color.a = 1;
	
	return Out;
};

//------------------------------------------------------
//		アウトラインピクセルシェーダー	
//------------------------------------------------------
float4 PS_Outline( VS_OUTPUT In ) : COLOR
{
	float4 OUT;
	OUT = In.Color;
	return OUT;
}

//------------------------------------------------------
//		トゥーン頂点シェーダー	
//------------------------------------------------------
float ToonLevel = 0.5f;
float ToonShadow = 0.4f;

VS_OUTPUT_TOON VS_Toon( VS_INPUTL In )
{
	VS_OUTPUT_TOON Out = (VS_OUTPUT_TOON)0;
	
	Out.Pos = mul( In.Pos,Projection );
	Out.Tex = In.Tex;
	Out.Color = 1;
	
	float3x3 mat = TransMatrix;
	float3 N = mul( In.Normal,mat );
	N = normalize(N);
	
	float3 v = normalize( LightDir );
	Out.Level = dot(v,-N) * 0.5f + 0.5f;
	
	return Out;
};

//------------------------------------------------------
//		薄膜頂点シェーダー	
//------------------------------------------------------
VS_OUTPUT_TOON VS_Film( VS_INPUTL In )
{
	VS_OUTPUT_TOON Out = (VS_OUTPUT_TOON)0;
	
	Out.Pos = mul( In.Pos,Projection );
	Out.Tex = In.Tex;
	Out.Color = In.Color;
	
	float3x3 mat = TransMatrix;
	float3 N = mul( In.Normal,mat );
	N = normalize(N);
	
	float3 vE = normalize( ViewPos - In.Pos );
	float value = max( 0, dot( vE,N ) );
	
	Out.Level = 1-value;
	
	return Out;
};

//------------------------------------------------------
//		トゥーンピクセルシェーダー	
//------------------------------------------------------
float4 PS_Toon( VS_OUTPUT_TOON In ) : COLOR
{
	float4 OUT;
	OUT = tex2D( DecaleSamp,In.Tex );
	
	if( In.Level < ToonLevel )OUT.rgb *= ToonShadow;
	
	return OUT;
}

//------------------------------------------------------
//		ライトマップピクセルシェーダー	
//------------------------------------------------------
float4 PS_LightMap( VS_OUTPUT_TOON In ) : COLOR
{
	float4 OUT;
	OUT = tex2D( DecaleSamp,In.Tex );
	
	float2 UV = { In.Level ,0 };
	float3 level = tex2D( LightSamp,UV );
	OUT.rgb *= level;
	OUT.rgb += pow( level,4 );
	
	return OUT;
}

//------------------------------------------------------
//		薄膜ピクセルシェーダー	
//------------------------------------------------------
float4 PS_FilmMap( VS_OUTPUT_TOON In ) : COLOR
{
	float4 OUT;
	
	float2 UV = { In.Level ,0 };
	OUT = tex2D( LightSamp,UV );
	
	return OUT;
}

//------------------------------------------------------
//		トゥーン用テクニック
//------------------------------------------------------
technique ToonLine
{
    pass P0
    {
		AlphaBlendEnable = true;
		BlendOp          = Add;
		SrcBlend         = SrcAlpha;
		DestBlend        = InvSrcAlpha;
		CullMode         = CW;
		ZEnable          = true;

		VertexShader = compile vs_3_0 VS_Outline();
		PixelShader  = compile ps_3_0 PS_Outline();
    }
}

technique toon
{
    pass P0
    {
		AlphaBlendEnable = true;
		BlendOp          = Add;
		SrcBlend         = SrcAlpha;
		DestBlend        = InvSrcAlpha;
		CullMode         = CW;
		ZEnable          = true;

		VertexShader = compile vs_3_0 VS_Outline();
		PixelShader  = compile ps_3_0 PS_Outline();
    }
    
    pass Toon
    {
		CullMode		 = CCW;
		ZEnable          = true;
		
		VertexShader = compile vs_3_0 VS_Toon();
		PixelShader  = compile ps_3_0 PS_Toon();
    }
}

//------------------------------------------------------
//		ライトマップ用テクニック
//------------------------------------------------------
technique lightmap
{ 
    pass P0
    {
		CullMode		 = CCW;
		AlphaBlendEnable = true;
		BlendOp          = Add;
		SrcBlend         = SrcAlpha;
		DestBlend        = InvSrcAlpha;
		ZEnable          = true;
		ZWriteEnable     = true;
		
		VertexShader = compile vs_3_0 VS_Toon();
		PixelShader  = compile ps_3_0 PS_LightMap();
    }
}

//------------------------------------------------------
//		薄膜用テクニック
//------------------------------------------------------
technique film
{   
    pass P0
    {
    	AlphaBlendEnable = true;
		BlendOp          = Add;
		SrcBlend         = SrcAlpha;
		DestBlend        = InvSrcAlpha;
		CullMode         = CCW;
		ZEnable          = true;
		ZWriteEnable     = true;
		
		
		VertexShader = compile vs_3_0 VS_Film();
		PixelShader  = compile ps_3_0 PS_FilmMap();
    }
}

//************************************************************************************************//
//				疑似ＨＤＲ																		  //
//************************************************************************************************//

//	頂点フォーマット
struct VS_HDR
{
    float4 Pos		: POSITION;
    float3 wPos		: TEXCOORD2;
    float3 Normal	: TEXCOORD1;
    float2 Tex		: TEXCOORD0;
};

//------------------------------------------------------
//	スペキュラー描画
//------------------------------------------------------
float HDRSpecular(float3 pos,float3 normal)
{
	float	sp;
	
	float3 H = normalize( ViewPos - pos );
	H = normalize( H - LightDir );
	
	sp = dot( normal,H );
	//sp = max( 0,sp );
	sp = min( 1.0,sp );
	sp = pow( sp,30 );
	
	return sp;
}

//**************************************************************************************************
//
//		スペキュラー描画
//
//**************************************************************************************************
//------------------------------------------------------
//	頂点シェーダー
//------------------------------------------------------
VS_HDR VS_Specular( VS_INPUTL In )
{
	VS_HDR Out = (VS_HDR)0;

	Out.Pos = mul(In.Pos,Projection);
	Out.Tex = In.Tex;
	
	//	スペキュラ計算用座標
	Out.wPos = mul( In.Pos,TransMatrix );
	//	スペキュラ計算用法線
	float3x3 mat = TransMatrix;
	float3 N = mul(In.Normal,mat);
	Out.Normal = normalize( N );

	return Out;
}

//------------------------------------------------------
//	ピクセルシェーダー	
//------------------------------------------------------
float4 PS_Specular( VS_HDR In) : COLOR
{   
	float4	OUT;

	//	スペキュラ計算
	float s  = HDRSpecular( In.wPos,In.Normal );
	float4 sp = tex2D( SpecularSamp,In.Tex ) * s;
	//	ピクセル色決定
	OUT.rgb = sp;
	OUT.a = 1;
	
	return OUT;
}

//------------------------------------------------------
//	テクニック
//------------------------------------------------------
technique specular
{
    pass P0
    {
		AlphaBlendEnable = true;			
		BlendOp          = Add;				
		SrcBlend         = SrcAlpha;		
		DestBlend        = InvSrcAlpha;		
		ZWriteEnable     = true;			

		VertexShader = compile vs_3_0 VS_Specular();
		PixelShader  = compile ps_3_0 PS_Specular();
    }
}

//**************************************************************************************************
//
//		雨シェーダー
//
//**************************************************************************************************
float AdjustRain ;	//	雨の流れる速さ
float RainVol;		//	雨の量
//------------------------------------------------------
//	頂点シェーダー
//------------------------------------------------------
VS_BASIC VS_Rain( VS_INPUTL In )
{
	VS_BASIC Out = (VS_BASIC)0;
	Out.Pos   = mul(In.Pos, Projection);
	Out.Color = 1;

	Out.Tex   = In.Tex;
	Out.Tex.y -= AdjustRain;
	Out.Tex.x += RainVol;
	return Out;
}

//------------------------------------------------------
//	テクニック
//------------------------------------------------------
technique rain
{
    pass P0
    {
		AlphaBlendEnable = true;
		BlendOp          = Add;
		SrcBlend         = SrcAlpha;
		DestBlend        = InvSrcAlpha;
		ZWriteEnable     = false;

		VertexShader = compile vs_3_0 VS_Rain();
		PixelShader  = compile ps_3_0 PS_Basic();
    }
}

//**************************************************************************************************
//
//		点光源付きフルエフェクト
//
//**************************************************************************************************

struct V_FULL
{
    float4 Pos		: POSITION;
    float4 Color	: COLOR0;
    float2 Tex		: TEXCOORD0;

    float4 Ambient	: COLOR1;
	float4 light	: COLOR2;
	float3 vLight	: TEXCOORD1;
	float3 vE		: TEXCOORD2;

	float3 plight	: TEXCOORD3;
};

float3	plight_color = { 1,0,0 };
float3	plight_pos = { 0,0,0 };
float	plight_range = 1.0f;

//------------------------------------------------------
//	頂点シェーダ
//------------------------------------------------------
V_FULL VS_Full( VS_INPUTL In )
{
	V_FULL Out = (V_FULL)0;

	Out.Pos   = mul(In.Pos, Projection);
	Out.Color = 1;//In.Color;
	Out.Tex   = In.Tex;

	float4 P = mul(In.Pos, TransMatrix);
	float3x3	mat = TransMatrix;
	float3 N = mul(In.Normal, mat);
	N = normalize(N);

	// 半球ライティング
	Out.Ambient.rgb = HemiLight(N);

	//	頂点ローカル座標系算出
	float3	vx;
	float3	vy = { 0, 1, 0.001f };
	vx = cross( vy, N );
	vx = normalize( vx );
	vy = cross( vx, N  );
	vy = normalize( vy );

	//	ライトベクトル補正
	Out.vLight.x = dot(vx, LightDir);
	Out.vLight.y = dot(vy, LightDir);
	Out.vLight.z = dot(N, LightDir);

	// 視線ベクトル補正
	float3 E = P - ViewPos;   // 視線ベクトル
	Out.vE.x = dot(vx, E);
	Out.vE.y = dot(vy, E);
	Out.vE.z = dot(N, E);

	//	点光源ベクトル補正
	float3 pvec = (P - plight_pos) / plight_range;
	//	ライトベクトル補正
	Out.plight.x = dot(vx, pvec);
	Out.plight.y = dot(vy, pvec);
	Out.plight.z = dot(N, pvec);

	return Out;
}

//------------------------------------------------------
//		ピクセルシェーダー	
//------------------------------------------------------
float4 PS_Full( V_FULL In) : COLOR
{   
	float4	OUT;
	float2 Tex = In.Tex;

	//	パララックスディスプレースメント
	float h = tex2D( HeightSamp, Tex ).r-0.5f;
	float3 E = normalize(In.vE);
	Tex.x += 0.04f * h * E.x;
	Tex.y -= 0.04f * h * E.y;

	//	法線取得
	float3 N = tex2D( NormalSamp, Tex ).xyz * 2.0f - 1.0f;

	//	ライト計算
	In.vLight   = normalize( In.vLight );
	float3 light = DirLight(In.vLight, N);

	//	点光源
	float plight = max( 0, 1-length(In.plight) );
	In.plight = normalize(In.plight);
	plight *= max( 0, dot( -In.plight, N ) );
	
	light += plight_color * plight;

	//	視線反射ベクトル
	float3 R = reflect( -E, N );
	//	スペキュラマップ取得
	float4 sp_tex = tex2D( SpecularSamp, Tex );

	//	環境マップ
	float3 env = Environment(R) * (1-sp_tex.a);

	//	ピクセル色決定
	OUT = In.Color * tex2D( DecaleSamp, Tex );
	OUT.rgb = (OUT.rgb * (light + In.Ambient.rgb) );
	OUT.rgb = OUT.rgb * env + OUT.rgb;

	//	スペキュラ
	OUT.rgb += pow( max( 0, dot(R, In.vLight) ), 10 ) * sp_tex;
	


	return OUT;
}

//------------------------------------------------------
//		合成なし	
//------------------------------------------------------
technique full
{
    pass P0
    {
		AlphaBlendEnable = true;
		BlendOp          = Add;
		SrcBlend         = SrcAlpha;
		DestBlend        = InvSrcAlpha;
		ZWriteEnable     = true;

		VertexShader = compile vs_3_0 VS_Full();
		PixelShader  = compile ps_3_0 PS_Full();
    }
}

//*****************************************************************************************************************************
//
//		シャドウマップ作成
//
//*****************************************************************************************************************************

float4x4 ShadowProjection;

texture ShadowMap;
sampler ShadowSamp = sampler_state
{
	Texture = <ShadowMap>;
	MinFilter = POINT;
	MagFilter = POINT;
	MipFilter = NONE;
	
	BorderColor = 0xFFFFFFFF;
	AddressU = BORDER;
	AddressV = BORDER;
};

struct VS_SHADOW
{
	float4 Pos		:	POSITION;
	float4 Color	:	TEXCOORD0;
};

//------------------------------------------------------
//		頂点シェーダー
//------------------------------------------------------
VS_SHADOW VS_ShadowBuf( float4 Pos : POSITION )
{
	VS_SHADOW Out;
	
	//	座標変換
	float4x4 mat = mul( TransMatrix,ShadowProjection );
	Out.Pos = mul( Pos,mat );
	Out.Color = Out.Pos.z;
	
	return Out;
}

//------------------------------------------------------
//		ピクセルシェーダー
//------------------------------------------------------
float4 PS_ShadowBuf( VS_SHADOW In ) : COLOR
{
	return In.Color;
}

//------------------------------------------------------
//		テクニック
//------------------------------------------------------
technique ShadowBuf
{
    pass Pass0
    {
		AlphaBlendEnable = false;
		ZWriteEnable     = true;
		CullMode		 = none;
		
		VertexShader = compile vs_2_0 VS_ShadowBuf();
		PixelShader  = compile ps_2_0 PS_ShadowBuf();
    }
}

//*****************************************************************************************************************************
//
//		シャドウマップ採用
//
//*****************************************************************************************************************************

struct V_FULL_S
{
    float4 Pos		: POSITION;
    float4 Color	: COLOR0;
    float2 Tex		: TEXCOORD0;
    float4 Ambient	: COLOR1;
	float4 light	: COLOR2;
	float3 vLight	: TEXCOORD1;
	float3 vE		: TEXCOORD2;
	float3 plight	: TEXCOORD3;
	
	float3 vShadow	: TEXCOORD4;

};

float AdjustValue = -0.004f;
float Shadow = 0.4f;

inline float3 GetShadowTex( float3 Pos )
{
	float3 Tex;
	
	float4 ppp;
	ppp.xyz = Pos;	
	ppp.w = 1;
	Tex = mul( ppp,ShadowProjection );
	
	Tex.y = -Tex.y;
	Tex.xy = 0.5f * Tex.xy + 0.5f;
	
	return Tex;
}

inline float GetShadow( float3 Tex )
{
	float d = tex2D( ShadowSamp,Tex.xy ).r;
	float l = ( d < Tex.z + AdjustValue ) ? Shadow : 1;
	
	return l;
}

//------------------------------------------------------
//		頂点シェーダー
//------------------------------------------------------
V_FULL_S VS_FullFX_S( VS_INPUTL In )
{
	V_FULL_S Out = (V_FULL_S)0;

	Out.Pos   = mul(In.Pos, Projection);
	Out.Color = 1;//In.Color;
	Out.Tex   = In.Tex;

	float4 P = mul(In.Pos, TransMatrix);
	float3x3	mat = TransMatrix;
	float3 N = mul(In.Normal, mat);
	N = normalize(N);

	// 半球ライティング
	Out.Ambient.rgb = HemiLight(N);

	//	頂点ローカル座標系算出
	float3	vx;
	float3	vy = { 0, 1, 0.001f };
	vx = cross( vy, N );
	vx = normalize( vx );
	vy = cross( vx, N  );
	vy = normalize( vy );

	//	ライトベクトル補正
	Out.vLight.x = dot(vx, LightDir);
	Out.vLight.y = dot(vy, LightDir);
	Out.vLight.z = dot(N, LightDir);

	// 視線ベクトル補正
	float3 E = P - ViewPos;   // 視線ベクトル
	Out.vE.x = dot(vx, E);
	Out.vE.y = dot(vy, E);
	Out.vE.z = dot(N, E);

	//	点光源ベクトル補正
	float3 pvec = (P - plight_pos) / plight_range;	

	//	ライトベクトル補正
	Out.plight.x = dot(vx, pvec);
	Out.plight.y = dot(vy, pvec);
	Out.plight.z = dot(N, pvec);	


	//	シャドウマップ
	Out.vShadow = GetShadowTex( P );

	return Out;
}

//------------------------------------------------------
//		ピクセルシェーダー
//------------------------------------------------------
float4 PS_FullFX_S( V_FULL_S In) : COLOR
{   
	float4	OUT;
	float2 Tex = In.Tex;

	//	パララックスディスプレースメント
	float h = tex2D( HeightSamp, Tex ).r-0.5f;
	float3 E = normalize(In.vE);
	/*
	Tex.x += 0.04f * h * E.x;
	Tex.y -= 0.04f * h * E.y;
	*/

	//	法線取得
	float3 N = tex2D( NormalSamp, Tex ).xyz * 2.0f - 1.0f;

	//	ライト計算
	In.vLight   = normalize( In.vLight );
	float3 light = DirLight(In.vLight, N);

	//	点光源
	float plight = max( 0, 1-length(In.plight) );
	In.plight = normalize(In.plight);
	plight *= max( 0, dot( -In.plight, N ) );
	
	light += plight_color * plight;

	//	視線反射ベクトル
	float3 R = reflect( -E, N );
	//	スペキュラマップ取得
	float4 sp_tex = tex2D( SpecularSamp, Tex );

	//	環境マップ
	float3 env = Environment(R) * (1-sp_tex.a);

	//	ピクセル色決定
	OUT = In.Color * tex2D( DecaleSamp, Tex );
	OUT.rgb = (OUT.rgb * (light + In.Ambient.rgb) );
	OUT.rgb = OUT.rgb * env + OUT.rgb;

	//	スペキュラ
	OUT.rgb += pow( max( 0, dot(R, In.vLight) ), 10 ) * sp_tex;

	//	シャドウマップ適用
	OUT.rgb *= GetShadow( In.vShadow ); 

	return OUT;
}

//------------------------------------------------------
//		テクニック
//------------------------------------------------------
technique full_s
{
    pass P0
    {
		AlphaBlendEnable = true;
		BlendOp          = Add;
		SrcBlend         = SrcAlpha;
		DestBlend        = InvSrcAlpha;
		ZWriteEnable     = true;
		CullMode		 = ccw;
		VertexShader = compile vs_3_0 VS_FullFX_S();
		PixelShader  = compile ps_3_0 PS_FullFX_S();
    }
}


//*****************************************************************************************************************************
//
//		深度描画シェーダー
//
//*****************************************************************************************************************************
struct VS_DEPTH
{
	float4 Pos		: POSITION;
	float  Depth	: TEXCOORD0;
};

//-------------------------------------------
//	頂点シェーダー
//-------------------------------------------
VS_DEPTH VS_Depth( float4 Pos : POSITION )
{
	VS_DEPTH Out;
	
	Out.Pos = mul( Pos,Projection );
	Out.Depth = Out.Pos.z;	//	深度
	
	return Out;
}

//-------------------------------------------
//	ピクセルシェーダー
//-------------------------------------------
float4 PS_Depth( VS_DEPTH In ) : COLOR
{
	float4 OUT;
	OUT.rgb = In.Depth;	//	深度を色として描画
	OUT.a = 1;
	return OUT;
}

//-------------------------------------------
//	テクニック
//-------------------------------------------
technique depth
{
	pass P0
	{
		AlphaBlendEnable = true;
		BlendOp          = Add;
		SrcBlend         = SrcAlpha;
		DestBlend        = InvSrcAlpha;
		ZWriteEnable     = true;

		VertexShader = compile vs_3_0 VS_Depth();
		PixelShader  = compile ps_3_0 PS_Depth();		
	}
}

//**************************************************************************************************
//
//		反射マップ作成
//
//**************************************************************************************************

/*

//------------------------------------------------------
//	頂点シェーダ
//------------------------------------------------------
V_FULL VS_Reflect( VS_INPUT In )
{
	V_FULL Out = (V_FULL)0;

	In.Pos.y = -In.Pos.y;

	Out.Pos   = mul(In.Pos, Projection);
	Out.Color = 1;//In.Color;
	Out.Tex   = In.Tex;

	float4 P = mul(In.Pos, TransMatrix);
	float3x3	mat = TransMatrix;
	float3 N = mul(In.Normal, mat);
	N = normalize(N);

	// 半球ライティング
	Out.Ambient.rgb = HemiLight(N);

	//	頂点ローカル座標系算出
	float3	vx;
	float3	vy = { 0, 1, 0.001f };
	vx = cross( vy, N );
	vx = normalize( vx );
	vy = cross( vx, N  );
	vy = normalize( vy );

	//	ライトベクトル補正
	Out.vLight.x = dot(vx, LightDir);
	Out.vLight.y = dot(vy, LightDir);
	Out.vLight.z = dot(N, LightDir);

	// 視線ベクトル補正
	float3 E = P - ViewPos;   // 視線ベクトル
	Out.vE.x = dot(vx, E);
	Out.vE.y = dot(vy, E);
	Out.vE.z = dot(N, E);

	//	点光源ベクトル補正
	float3 pvec = (P - plight_pos) / plight_range;
	//	ライトベクトル補正
	Out.plight.x = dot(vx, pvec);
	Out.plight.y = dot(vy, pvec);
	Out.plight.z = dot(N, pvec);


	return Out;
}

//------------------------------------------------------
//		テクニック	
//------------------------------------------------------
technique Reflect
{
    pass P0
    {
		AlphaBlendEnable = true;
		BlendOp          = Add;
		SrcBlend         = SrcAlpha;
		DestBlend        = InvSrcAlpha;
		ZWriteEnable     = true;
		CullMode         = cw;

		VertexShader = compile vs_3_0 VS_Reflect();
		PixelShader  = compile ps_3_0 PS_Full();
    }
}

//**************************************************************************************************
//
//		反射マップ付描画
//
//**************************************************************************************************

//------------------------------------------------------
//	反射マップ
//------------------------------------------------------
texture RefMap;		//	反射マップテクスチャ
sampler RefSamp = sampler_state
{
    Texture = <RefMap>;
    MinFilter = LINEAR;
    MagFilter = LINEAR;
    MipFilter = NONE;

    AddressU = Wrap;
    AddressV = Wrap;
};

struct V_FULL_SR
{
    float4 Pos		: POSITION;
    float4 Color	: COLOR0;
    float2 Tex		: TEXCOORD0;

    float4 Ambient	: COLOR1;
	float4 light	: COLOR2;
	float3 vLight	: TEXCOORD1;
	float3 vE		: TEXCOORD2;

	float3 plight	: TEXCOORD3;

	float3 vShadow : TEXCOORD4;
    float4 wPos		: TEXCOORD5;
};

//------------------------------------------------------
//	反射付頂点シェーダ
//------------------------------------------------------
V_FULL_SR VS_FullFX_SR( VS_INPUT In )
{
	V_FULL_SR Out = (V_FULL_SR)0;

	Out.Pos   = mul(In.Pos, Projection);
	Out.Color = 1;
	Out.Tex   = In.Tex;

	Out.wPos  = Out.Pos; 

	float4 P = mul(In.Pos, TransMatrix);
	float3x3	mat = TransMatrix;
	float3 N = mul(In.Normal, mat);
	N = normalize(N);

	// 半球ライティング
	Out.Ambient.rgb = HemiLight(N);

	//	頂点ローカル座標系算出
	float3	vx;
	float3	vy = { 0, 1, 0.001f };
	vx = cross( vy, N );
	vx = normalize( vx );
	vy = cross( vx, N  );
	vy = normalize( vy );

	//	ライトベクトル補正
	Out.vLight.x = dot(vx, LightDir);
	Out.vLight.y = dot(vy, LightDir);
	Out.vLight.z = dot(N, LightDir);

	// 視線ベクトル補正
	float3 E = P - ViewPos;   // 視線ベクトル
	Out.vE.x = dot(vx, E);
	Out.vE.y = dot(vy, E);
	Out.vE.z = dot(N, E);

	//	点光源ベクトル補正
	float3 pvec = (P - plight_pos) / plight_range;
	//	ライトベクトル補正
	Out.plight.x = dot(vx, pvec);
	Out.plight.y = dot(vy, pvec);
	Out.plight.z = dot(N, pvec);

	//	シャドウバッファ
	Out.vShadow = GetShadowTex(P);

	return Out;
}

//------------------------------------------------------
//		ピクセルシェーダー	
//------------------------------------------------------
float4 PS_FullFX_SR( V_FULL_SR In) : COLOR
{   
	float4	OUT;
	float2 Tex = In.Tex;

	//	パララックスディスプレースメント
	float h = tex2D( HeightSamp, Tex ).r-0.5f;
	float3 E = normalize(In.vE);
	Tex.x += 0.04f * h * E.x;
	Tex.y -= 0.04f * h * E.y;

	//	法線取得
	float3 N = tex2D( NormalSamp, Tex ).xyz * 2.0f - 1.0f;

	//	ライト計算
	In.vLight   = normalize( In.vLight );
	float3 light = DirLight(In.vLight, N);

	//	点光源
	float plight = max( 0, 1-length(In.plight) );
	In.plight = normalize(In.plight);
	plight *= max( 0, dot( -In.plight, N ) );
	
	light += plight_color * plight;

	//	視線反射ベクトル
	float3 R = reflect( -E, N );
	//	スペキュラマップ取得
	float4 sp_tex = tex2D( SpecularSamp, Tex );

	//	環境マップ
	float3 env = Environment(R) * (1-sp_tex.a);

	//	ピクセル色決定
	OUT = In.Color * tex2D( DecaleSamp, Tex );
	OUT.rgb = (OUT.rgb * (light + In.Ambient.rgb) );
	OUT.rgb = OUT.rgb * env + OUT.rgb;

	//	スペキュラ
	OUT.rgb += pow( max( 0, dot(R, In.vLight) ), 10 ) * sp_tex;

	//	シャドウマップ
	OUT.rgb *= GetShadow( In.vShadow );


	float2 ref = ( In.wPos.xy/In.wPos.w )*0.5f + 0.5f;
	ref.y = -ref.y;
	float3 Env = tex2D( RefSamp,ref ) * ( OUT.rgb + 0.4f );
	
	OUT.rgb = lerp( Env,OUT.rgb,0.3f );






	return OUT;
}

//------------------------------------------------------
//	
//------------------------------------------------------
technique full_sr
{
    pass P0
    {
		AlphaBlendEnable = true;
		BlendOp          = Add;
		SrcBlend         = SrcAlpha;
		DestBlend        = InvSrcAlpha;
		ZWriteEnable     = true;

		VertexShader = compile vs_3_0 VS_FullFX_SR();
		PixelShader  = compile ps_3_0 PS_FullFX_SR();
    }
}

*/